'use strict'
const app = require('app')
const BrowserWindow = require('browser-window')

let mainWindow = null

app.on('window-all-closed', () => {
  if (process.platform != 'darwin') { app.quit() }
})

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    'auto-hide-menu-bar': true, width: 1000, height: 600 })
  // mainWindow.maximize()
  mainWindow.loadURL(`file://${__dirname}/index.html`)
  // mainWindow.openDevTools()
  mainWindow.on('closed', () => { mainWindow = null })
})
