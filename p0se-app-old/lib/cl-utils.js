'use strict'
const addon = require('bindings')('addon')

exports.getPlatforms = function getPlatforms () {
  return addon.getPlatforms()
}
