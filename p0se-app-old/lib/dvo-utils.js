'use strict'
const resolve = require('path').resolve
const addon = require('bindings')('addon')
const THREE = require('three')

exports.readImageSync = function (path, alpha, beta, convertBGR2RGB) {
  if (alpha === undefined) { alpha = 1 }
  if (beta === undefined) { beta = 0 }
  if (convertBGR2RGB === undefined) { convertBGR2RGB = true }

  const res = addon.readImageSync(resolve(path), alpha, beta, convertBGR2RGB)

  return { width: res[0], height: res[1], numChannels: res[2],
      data: new Float32Array(res[3], 0, res[0]*res[1]*res[2]) }
}

exports.createGeometry = function (rgbImage, depthImage, intrinsics) {
  assertImage('rgbImage', rgbImage, 3)
  assertImage('depthImage', depthImage, 1)

  if (rgbImage.data.length !== depthImage.data.length*3) {
    throw new Error('Color and depth image must have the same dimension')
  }

  const ret = addon.createGeometry(rgbImage.data.buffer, depthImage.data.buffer,
                                   rgbImage.width, rgbImage.height,
                                   intrinsics.elements.buffer)

  const colors = new Float32Array(ret[0], 0, ret[0].byteLength/4)
  const positions = new Float32Array(ret[1], 0, ret[1].byteLength/4)

  const geometry = new THREE.BufferGeometry()
  geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3))
  geometry.addAttribute('color', new THREE.BufferAttribute(colors, 3))

  return geometry
}

function assertImage (name, image, numChannels) {
  if (!image ||
      typeof image.width !== 'number' ||
      typeof image.height !== 'number' ||
      typeof image.numChannels !== 'number'||
      !image.data instanceof Float32Array) {
    throw new Error(`${name} must be of form { width: Number, height: Number,` +
                    ' numChannels: Number, data: Float32Array }')
  }

  if (image.width * image.height * image.numChannels !== image.data.length) {
    throw new Error(`${name} data and description do not match`)
  }

  if (numChannels !== undefined && image.numChannels !== numChannels) {
    throw new Error(`${name} is expected to have ${numChannels} channel(s)`)
  }
}
