#include <node.h>

#include "dvo-js-bindings/dvo-js-bindings.h"
#include "cl-js-bindings/cl-js-bindings.h"
#include "p0se-js-bindings/p0se-js-bindings.h"

namespace {

void Init(v8::Local<v8::Object> exports) {
  cl_js_bindings::Init(exports);
  dvo_js_bindings::Init(exports);
  p0se_js_bindings::Init(exports);
}

NODE_MODULE(addon, Init)

} // namepace
