#include "cl-js-bindings/cl-js-bindings.h"

#include <vector>
#include <string>
#include <CL/cl.hpp>

#include "cl-js-bindings/platform.h"
#include "cl-js-bindings/device.h"

namespace cl_js_bindings {

void GetPlatforms(const v8::FunctionCallbackInfo<v8::Value>& info) {
  v8::Isolate* isolate = info.GetIsolate();

  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);

  auto length = static_cast<int>(platforms.size());
  auto platformObjects = v8::Array::New(isolate, length);
  for (int i = 0; i < length; ++i) {
    auto platformObject = CreatePlatformObject(isolate, platforms[i]);
    platformObjects->Set(i, platformObject);
  }

  info.GetReturnValue().Set(platformObjects);
}

void Init(v8::Local<v8::Object> exports) {
  v8::Isolate* isolate = v8::Isolate::GetCurrent();

  NODE_SET_METHOD(exports, "getPlatforms", GetPlatforms);
}

}  // namespace cl_js_bindings
