#ifndef CL_JS_BIDNINGS_H_
#define CL_JS_BIDNINGS_H_

#include <node.h>

namespace cl_js_bindings {

void Init(v8::Local<v8::Object> exports);

}  // namespace cl_js_bindings

#endif // CL_JS_BIDNINGS_H_
