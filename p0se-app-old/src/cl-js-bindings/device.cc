#include "cl-js-bindings/device.h"

#include <string>

#include "cl-js-bindings/weak-callbacks.h"

namespace cl_js_bindings {

template <cl_device_info device_info>
void GetInfoString(v8::Local<v8::String> property,
             const v8::PropertyCallbackInfo<v8::Value>& info) {
  v8::Isolate* isolate = info.GetIsolate();

  cl::Device* device = static_cast<cl::Device*>(
    info.Holder()->GetInternalField(0).As<v8::External>()->Value());

  std::string ret = device->getInfo<device_info>();
  info.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, ret.c_str()));
}

v8::Local<v8::Object> CreateDeviceObject(v8::Isolate* isolate,
                                         const cl::Device& device) {
  auto tpl = v8::ObjectTemplate::New(isolate);
  tpl->SetInternalFieldCount(1);

  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "extensions"),
                   GetInfoString<CL_DEVICE_EXTENSIONS>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "name"),
                   GetInfoString<CL_DEVICE_NAME>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "openCLCVersion"),
                   GetInfoString<CL_DEVICE_OPENCL_C_VERSION>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "profile"),
                   GetInfoString<CL_DEVICE_PROFILE>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "vendor"),
                   GetInfoString<CL_DEVICE_VENDOR>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "version"),
                   GetInfoString<CL_DEVICE_VERSION>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "driverVersion"),
                   GetInfoString<CL_DRIVER_VERSION>);

  auto device_object = tpl->NewInstance();

  auto ptr = new cl::Device(device);
  device_object->SetInternalField(0, v8::External::New(isolate, ptr));
  v8::Persistent<v8::Object>(isolate, device_object).SetWeak(
    ptr, WeakCallbackDelete<cl::Device>, v8::WeakCallbackType::kParameter);

  return device_object;
}

} // namespace cl_js_bindings
