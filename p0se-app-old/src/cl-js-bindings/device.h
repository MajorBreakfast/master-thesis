#ifndef CL_JS_BIDNINGS_DEVICE_H_
#define CL_JS_BIDNINGS_DEVICE_H_

#include <v8.h>
#include <CL/cl.hpp>

namespace cl_js_bindings {

v8::Local<v8::Object> CreateDeviceObject(v8::Isolate* isolate,
                                         const cl::Device& device);

} // namespace cl_js_bindings

#endif // CL_JS_BIDNINGS_DEVICE_H_
