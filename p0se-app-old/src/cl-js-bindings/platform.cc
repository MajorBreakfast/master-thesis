#include "cl-js-bindings/platform.h"

#include <string>
#include <vector>

#include "cl-js-bindings/device.h"
#include "cl-js-bindings/weak-callbacks.h"

namespace cl_js_bindings {

template <cl_platform_info platform_info>
void GetInfo(v8::Local<v8::String> property,
             const v8::PropertyCallbackInfo<v8::Value>& info) {
  v8::Isolate* isolate = info.GetIsolate();

  cl::Platform* platform = static_cast<cl::Platform*>(
    info.Holder()->GetInternalField(0).As<v8::External>()->Value());

  std::string ret = platform->getInfo<platform_info>();
  info.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, ret.c_str()));
}

void GetDevices(v8::Local<v8::String> property,
                const v8::PropertyCallbackInfo<v8::Value>& info) {
  v8::Isolate* isolate = info.GetIsolate();

  cl::Platform* platform = static_cast<cl::Platform*>(
    info.Holder()->GetInternalField(0).As<v8::External>()->Value());

  std::vector<cl::Device> devices;
  platform->getDevices(CL_DEVICE_TYPE_ALL, &devices);

  auto length = static_cast<int>(devices.size());
  auto deviceObjects = v8::Array::New(isolate, length);
  for (int i = 0; i < length; ++i) {
    auto deviceObject = CreateDeviceObject(isolate, devices[i]);
    deviceObjects->Set(i, deviceObject);
  }

  info.GetReturnValue().Set(deviceObjects);
}

v8::Local<v8::Object> CreatePlatformObject(v8::Isolate* isolate,
                                           const cl::Platform& platform) {
  auto tpl = v8::ObjectTemplate::New(isolate);
  tpl->SetInternalFieldCount(1);

  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "extensions"),
                  GetInfo<CL_PLATFORM_EXTENSIONS>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "name"),
                   GetInfo<CL_PLATFORM_NAME>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "profile"),
                   GetInfo<CL_PLATFORM_PROFILE>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "vendor"),
                  GetInfo<CL_PLATFORM_VENDOR>);
  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "version"),
                   GetInfo<CL_PLATFORM_VERSION>);

  tpl->SetAccessor(v8::String::NewFromUtf8(isolate, "devices"), GetDevices);

  auto platform_object = tpl->NewInstance();

  auto ptr = new cl::Platform(platform);
  platform_object->SetInternalField(0, v8::External::New(isolate, ptr));
  v8::Persistent<v8::Object>(isolate, platform_object).SetWeak(
    ptr, WeakCallbackDelete<cl::Platform>, v8::WeakCallbackType::kParameter);

  return platform_object;
}

} // namespace cl_js_bindings
