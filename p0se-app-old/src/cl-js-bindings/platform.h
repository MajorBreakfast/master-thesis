#ifndef CL_JS_BIDNINGS_PLATFORM_H_
#define CL_JS_BIDNINGS_PLATFORM_H_

#include <v8.h>
#include <CL/cl.hpp>

namespace cl_js_bindings {

v8::Local<v8::Object> CreatePlatformObject(v8::Isolate* isolate,
                                           const cl::Platform& platform);

} // namespace cl_js_bindings

#endif // CL_JS_BIDNINGS_PLATFORM_H_
