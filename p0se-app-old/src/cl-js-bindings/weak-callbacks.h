#ifndef CL_JS_BINDINGS_WEAK_CALLBACKS_H_
#define CL_JS_BINDINGS_WEAK_CALLBACKS_H_

namespace cl_js_bindings {

// Callback for SetWeak() that calls the destructor of the paramter
template <typename T>
void WeakCallbackDelete(const v8::WeakCallbackInfo<T>& info) {
  delete info.GetParameter();
}

} // namespace cl_js_bindings

#endif // CL_JS_BINDINGS_WEAK_CALLBACKS_H_
