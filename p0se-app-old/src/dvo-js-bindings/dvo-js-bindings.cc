#include "dvo-js-bindings/dvo-js-bindings.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace dvo_js_bindings {

void ReadImageSync(const v8::FunctionCallbackInfo<v8::Value>& info) {
  v8::Isolate* isolate = info.GetIsolate();

  // Load image using OpenCV (Slow, takes 4-6ms for 640x480 image)
  std::string path(*v8::String::Utf8Value(info[0]));
  cv::Mat matIn = cv::imread(path,
                           CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);;
  double alpha = info[1]->NumberValue();
  double beta = info[2]->NumberValue();
  cv::Mat mat; matIn.convertTo(mat, CV_32FC(matIn.channels()), alpha, beta);

  // BGR to RGB conversion
  bool convertBGR2RGB = info[3]->BooleanValue();
  if (convertBGR2RGB && mat.type() == CV_32FC3) {
    cv::Mat mat2;
    cv::cvtColor(mat, mat2, CV_BGR2RGB);
    mat = mat2;
  }

  // Copy data into ArrayBuffer
  size_t num_bytes = mat.total() * mat.elemSize();
  auto array_buffer = v8::ArrayBuffer::New(isolate, num_bytes);
  memcpy(array_buffer->GetContents().Data(), mat.data, num_bytes);

  // Return array [widht, height, numChannels, data]
  auto ret = v8::Array::New(isolate, 4);
  ret->Set(0, v8::Number::New(isolate, mat.cols)); // width
  ret->Set(1, v8::Number::New(isolate, mat.rows)); // height
  ret->Set(2, v8::Number::New(isolate, mat.channels())); // numChannels
  ret->Set(3, array_buffer); // data
  info.GetReturnValue().Set(ret);
}

inline void multiplyMatrixVector3(const float* A, const float* x, float* b) {
  b[0] = A[0]*x[0] + A[3]*x[1] + A[6]*x[2];
  b[1] = A[1]*x[0] + A[4]*x[1] + A[7]*x[2];
  b[2] = A[2]*x[0] + A[5]*x[1] + A[8]*x[2];
}

inline int invertMatrix3(const float* M, float* Inv) {
  Inv[0] =  M[8]*M[4] - M[5]*M[7];
  Inv[1] = -M[8]*M[1] + M[2]*M[7];
  Inv[2] =  M[5]*M[1] - M[2]*M[4];
  Inv[3] = -M[8]*M[3] + M[5]*M[6];
  Inv[4] =  M[8]*M[0] - M[2]*M[6];
  Inv[5] = -M[5]*M[0] + M[2]*M[3];
  Inv[6] =  M[7]*M[3] - M[4]*M[6];
  Inv[7] = -M[7]*M[0] + M[1]*M[6];
  Inv[8] =  M[4]*M[0] - M[1]*M[3];

  float det = M[0]*Inv[0] + M[1]*Inv[3] + M[2]*Inv[6];

  if (det == 0) { return -1; }

  float a = 1.0f / det;
  Inv[0] *= a; Inv[3] *= a; Inv[6] *= a;
  Inv[1] *= a; Inv[4] *= a; Inv[7] *= a;
  Inv[2] *= a; Inv[5] *= a; Inv[8] *= a;

  return 0;
}

void CreateGeometry(const v8::FunctionCallbackInfo<v8::Value>& info) {
  v8::Isolate* isolate = info.GetIsolate();

  float* rgb = static_cast<float*>(info[0].As<v8::ArrayBuffer>()
                                   ->GetContents().Data());
  float* depth = static_cast<float*>(info[1].As<v8::ArrayBuffer>()
                                     ->GetContents().Data());
  int w = info[2]->Int32Value();
  int h = info[3]->Int32Value();
  float* intrinsics = static_cast<float*>(info[4].As<v8::ArrayBuffer>()
                                          ->GetContents().Data());

  float invIntrinsics[9];
  if (invertMatrix3(intrinsics, invIntrinsics) < 0) {
    const char* msg = "Intrinsic matrix is non-invertible";
    auto string = v8::String::NewFromUtf8(isolate, msg);
    isolate->ThrowException(v8::Exception::Error(string));
  }

  int num_valid = 0;
  for (int i = 0; i < w*h; ++i) if (depth[i] > 0) ++num_valid;

  auto colors_buffer = v8::ArrayBuffer::New(isolate, 3*num_valid*sizeof(float));
  auto positions_buffer = v8::ArrayBuffer::New(isolate,
                                               3*num_valid*sizeof(float));
  float* colors = static_cast<float*>(colors_buffer->GetContents().Data());
  float* positions = static_cast<float*>(positions_buffer
                                         ->GetContents().Data());

  int offset = 0;
  for (int y = 0; y < h; ++y) {
    for (int x = 0; x < w; ++x) {
      int pos = x + y*w;
      float d = depth[pos];
      if (d > 0) {
        float v[] = {(w-1-x)*d, (h-1-y)*d, d};
        multiplyMatrixVector3(invIntrinsics, v, positions + offset);
        memcpy(colors + offset, rgb + 3*(x + y*w), 3 * sizeof(float));
        offset += 3;
      }
    }
  }

  auto ret = v8::Array::New(isolate, 2);
  ret->Set(0, colors_buffer);
  ret->Set(1, positions_buffer);
  info.GetReturnValue().Set(ret);
}

void Init(v8::Local<v8::Object> exports) {
  v8::Isolate* isolate = v8::Isolate::GetCurrent();

  NODE_SET_METHOD(exports, "readImageSync", ReadImageSync);
  NODE_SET_METHOD(exports, "createGeometry", CreateGeometry);
}

}  // namespace dvo_js_bindings
