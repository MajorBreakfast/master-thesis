#ifndef DVO_JS_BIDNINGS_H_
#define DVO_JS_BIDNINGS_H_

#include <node.h>

namespace dvo_js_bindings {

void Init(v8::Local<v8::Object> exports);

}  // namespace dvo_js_bindings

#endif // DVO_JS_BIDNINGS_H_
