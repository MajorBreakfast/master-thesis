#ifndef P0SE_JS_BIDNINGS_H_
#define P0SE_JS_BIDNINGS_H_

#include <node.h>

namespace p0se_js_bindings {

void Init(v8::Local<v8::Object> exports);

}  // namespace p0se_js_bindings

#endif // P0SE_JS_BIDNINGS_H_
