# API

## P0seContainer

### Constructor
```
const p0seContainer = new P0seContainer({
  platformIndex: 1,
  deviceIndex: 0
})
```

## P0seImage

### `new P0seImage(p0seContainer, width, height, dataType)`
- `width` and `height` are integer values
- `dataType` can be either `'uchar3'`, `'short'` or `'float'`

### `writeSync(image)`
- `image` is an object of the form `{ width, height, dataType, buffer }`
- chainable (returns `this`)

### `readSync([image])`
- `image` is an optional object of the form `{ width, height, dataType, buffer }`. If no object is provided then a new one is created and returned
- chainable (returns `this`). If no image is passed in as an argument, then it returns the newly created image instead.
