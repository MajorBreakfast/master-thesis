const { app, BrowserWindow, ipcMain } = require('electron')
require('electron-debug')() // Adds a keyboard shortcut for opening dev tools
const path = require('path')
const url = require('url')

function main () {
  let mainBrowserWindow
  let otherBrowserWindows = new Set()

  function createMainBrowserWindow () {
    mainBrowserWindow = new BrowserWindow({
      width: 800,
      height: 600,
      title: 'p0se App'
    })

    mainBrowserWindow.loadURL(url.format({
      pathname: path.resolve(__dirname, '..', 'render-process', 'index.html'),
      protocol: 'file:',
      slashes: true
    }))

    mainBrowserWindow.setMenu(null)
    mainBrowserWindow.webContents.openDevTools()

    mainBrowserWindow.on('closed', () => { mainBrowserWindow = null })
  }

  function createAlignmentInspectorBrowserWindow () {
    browserWindow = new BrowserWindow({
      width: 1000,
      height: 600,
      title: 'p0se App - Alignment Inspector'
    })

    browserWindow.loadURL(url.format({
      pathname: path.resolve(__dirname, '..', 'render-process',
                             'alignment-inspector-index.html'),
      protocol: 'file:',
      slashes: true
    }))

    browserWindow.setMenu(null)
    browserWindow.webContents.openDevTools()

    otherBrowserWindows.add(browserWindow)
  }

  app.on('ready', () => {
    //createAlignmentInspectorBrowserWindow()
    createMainBrowserWindow()
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') { app.quit() }
  })

  app.on('activate', () => {
    // macOS behavior: Recreate a new window
    if (mainBrowserWindow === null) { createMainBrowserWindow() }
  })

  ipcMain.on('open-alignment-inspector', () => {
    createAlignmentInspectorBrowserWindow()
  })
}

module.exports = main
