#include <node.h>

#include "decode-png.h"
#include "create-geometry.h"
#include "p0se-js/native-container.h"
#include "p0se-js/native-device-buffer.h"
#include "p0se-js/native-aligner/native-aligner.h"

namespace addon {

void Init(v8::Local<v8::Object> exports) {
  NODE_SET_METHOD(exports, "decodePNG", DecodePng);
  NODE_SET_METHOD(exports, "createGeometry", CreateGeometry);

  // p0se-js
  p0se_js::NativeContainer::Init(exports);
  p0se_js::NativeDeviceBuffer::Init(exports);
  p0se_js::NativeAligner::Init(exports);
}

NODE_MODULE(addon, Init)

} // End namepace addon
