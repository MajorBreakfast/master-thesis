#include "decode-png.h"

#include <cinttypes>
#include <uv.h>
#include <node_buffer.h>

namespace addon {

// Row major
inline void multiplyMatrixVector3(const float* A, const float* x, float* b) {
  b[0] = A[0]*x[0] + A[1]*x[1] + A[2]*x[2];
  b[1] = A[3]*x[0] + A[4]*x[1] + A[5]*x[2];
  b[2] = A[6]*x[0] + A[7]*x[1] + A[8]*x[2];
}

float* GetData(v8::Local<v8::Float32Array> f32a) {
  return reinterpret_cast<float*>(f32a->Buffer()->GetContents().Data()) +
         f32a->ByteOffset();
}

void CreateGeometry(const v8::FunctionCallbackInfo<v8::Value>& info) {
  v8::Isolate* isolate = info.GetIsolate();

  uint8_t* rgb = reinterpret_cast<uint8_t*>(node::Buffer::Data(info[0]));
  uint16_t* depth = reinterpret_cast<uint16_t*>(node::Buffer::Data(info[1]));
  int32_t width = info[2]->Int32Value();
  int32_t height = info[3]->Int32Value();
  int32_t num_pixels = width * height;
  float* inv_intrinsics = GetData(info[4].As<v8::Float32Array>());
  float depth_scale_factor = static_cast<float>(info[5]->NumberValue());

  int32_t num_valid_pixels = 0;
  for (int i = 0; i < num_pixels; ++i) if (depth[i] > 0) { ++num_valid_pixels; }

  auto positions_ab =
      v8::ArrayBuffer::New(isolate, 3 * num_valid_pixels * sizeof(float));
  float* positions = static_cast<float*>(positions_ab->GetContents().Data());

  auto colors_ab =
      v8::ArrayBuffer::New(isolate, 3 * num_valid_pixels * sizeof(float));
  float* colors = static_cast<float*>(colors_ab->GetContents().Data());

  float color_scale_factor = 1.0f / 255.0f;
  int offset = 0;
  for (int32_t y = 0; y < height; ++y) {
    for (int32_t x = 0; x < width; ++x) {
      int32_t pos = x + y * width;
      float d = depth[pos] * depth_scale_factor;
      if (d > 0.0f) {
        float v[] = {x * d, y * d, d};
        multiplyMatrixVector3(inv_intrinsics, v, positions + offset * 3);
        uint8_t* rgbColor = rgb + 3 * pos;
        colors[3 * offset    ] = rgbColor[0] * color_scale_factor;
        colors[3 * offset + 1] = rgbColor[1] * color_scale_factor;
        colors[3 * offset + 2] = rgbColor[2] * color_scale_factor;
        offset += 1;
      }
    }
  }

  auto ret = v8::Array::New(isolate, 3);
  ret->Set(0, v8::Number::New(isolate, num_valid_pixels));
  ret->Set(1, positions_ab);
  ret->Set(2, colors_ab);
  info.GetReturnValue().Set(ret);
}

} // End namespace addon
