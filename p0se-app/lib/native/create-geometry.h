#pragma once

#include <node.h>

namespace addon {

void CreateGeometry(const v8::FunctionCallbackInfo<v8::Value>& info);

} // End namespace addon
