#include "decode-png.h"

#include <cinttypes>
#include <memory>
#include <algorithm>
#include <map>
#include <functional>
#include <uv.h>
#include <node_buffer.h>
#include <stdexcept>
#include <string>
#include "utils/throw-error.h"

//#define PNG_DEBUG 3
#include <png.h>

namespace addon {

using v8::Exception;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::HandleScope;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Undefined;
using v8::Value;

namespace decode_png_internal {

const std::map<int, std::string> kBitDepthMap = {
	{ 8, "uchar" },
	{ 16, "ushort" }
};

const std::map<int, std::string> kColorTypeMap = {
	{ PNG_COLOR_TYPE_GRAY, "" },
	{ PNG_COLOR_TYPE_RGB, "3" },
	{ PNG_COLOR_TYPE_RGB_ALPHA, "4" },
	{ PNG_COLOR_TYPE_GRAY_ALPHA, "2" }
};

struct PngStuff {
  PngStuff() {
    png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png) { throw std::runtime_error("libpng initialization error"); }
    info = png_create_info_struct(png);
    end_info = png_create_info_struct(png);
  }
  ~PngStuff() {
    png_destroy_read_struct(&png, &info, &end_info);
  }
  png_structp png;
  png_infop info;
  png_infop end_info;
};

struct WorkData {
  Isolate* isolate;
  Persistent<Function> callback;
  char* input_buffer;
  size_t input_length;
  size_t input_read_pos;
  std::unique_ptr<PngStuff> png_stuff;
  int32_t width, height, bit_depth, color_type;
	Persistent<Object> output_node_buffer;
	char* output_buffer;
	size_t output_length;
  std::unique_ptr<std::exception> exception;
};

inline bool IsBigEndian() { // From OpenCV
  return (((const int*)"\0\x1\x2\x3\x4\x5\x6\x7")[0] & 255) != 0;
}

void ReadFn(png_structp png, png_bytep output, png_size_t length) {
	auto work_data = reinterpret_cast<WorkData*>(png_get_io_ptr(png));
  if (work_data->input_read_pos + length > work_data->input_length) {
    png_error(png, "Input buffer ended unexpectedly");
  } else {
    length = std::min(length,
                      work_data->input_length - work_data->input_read_pos);
		memcpy(output, work_data->input_buffer + work_data->input_read_pos, length);
	  work_data->input_read_pos += length;
	}
}

void WarningFn(png_structp png_ptr, png_const_charp message) {}

void ErrorFn(png_structp png_ptr, png_const_charp message) {
  throw std::runtime_error(message);
}

void AsyncDecodePngInfo(uv_work_t* work) {
	WorkData* work_data = reinterpret_cast<WorkData*>(work->data);
  try {
		png_bytep input_buffer =
		    reinterpret_cast<png_bytep>(work_data->input_buffer);

    // Check for valid png signature
    if (work_data->input_length < 8 || png_sig_cmp(input_buffer, 0, 8) != 0) {
  		throw std::runtime_error("Invalid PNG file");
    }

    // png initialization
    work_data->png_stuff = std::unique_ptr<PngStuff>(new PngStuff());

    // libpng error handling
    png_set_error_fn(work_data->png_stuff->png, 0, ErrorFn, WarningFn);
    if (setjmp(png_jmpbuf(work_data->png_stuff->png))) {
  		throw std::runtime_error("libpng error");
  	}

    // Use custom read function
    work_data->input_read_pos = 0;
    png_set_read_fn(work_data->png_stuff->png, work_data, &ReadFn);

		// Read info
		png_read_info(work_data->png_stuff->png, work_data->png_stuff->info);

		// Extract info
		int32_t num_rows = png_get_image_height(work_data->png_stuff->png,
		                                        work_data->png_stuff->info);
	  size_t num_row_bytes = png_get_rowbytes(work_data->png_stuff->png,
		                                        work_data->png_stuff->info);

    work_data->width = png_get_image_width(work_data->png_stuff->png,
		                                       work_data->png_stuff->info);
    work_data->height = png_get_image_height(work_data->png_stuff->png,
		                                         work_data->png_stuff->info);
    work_data->bit_depth = png_get_bit_depth(work_data->png_stuff->png,
		                                         work_data->png_stuff->info);
    work_data->color_type = png_get_color_type(work_data->png_stuff->png,
		                                           work_data->png_stuff->info);
		work_data->output_length = num_row_bytes * num_rows;
  } catch (const std::exception& exception) {
    work_data->exception.reset(new std::exception(exception));
	}
}

void AsyncDecodePngPixels(uv_work_t* work);
void AfterDecodingPngPixels(uv_work_t* decode_pixels_work);

void AfterDecodingPngInfo(uv_work_t* decode_info_work) {
	WorkData* work_data = reinterpret_cast<WorkData*>(decode_info_work->data);
  Isolate* isolate = work_data->isolate;

  HandleScope scope(isolate);

	if (!work_data->exception.get()) {
		// Allocate buffer
		Local<Object> output_node_buffer =
		    node::Buffer::New(isolate, work_data->output_length).ToLocalChecked();
		work_data->output_node_buffer.Reset(isolate, output_node_buffer);
		work_data->output_buffer = node::Buffer::Data(output_node_buffer);
	}

	auto decode_pixels_work = new uv_work_t;
	decode_pixels_work->data = work_data;
	uv_queue_work(uv_default_loop(),
								decode_pixels_work,
								decode_png_internal::AsyncDecodePngPixels,
								(uv_after_work_cb)decode_png_internal::AfterDecodingPngPixels);

  // Cleanup
  delete decode_info_work;
}

void AsyncDecodePngPixels(uv_work_t* work) {
  auto work_data = reinterpret_cast<WorkData*>(work->data);

	if (work_data->exception.get()) { return; }

  try {
    // libpng error handling
    png_set_error_fn(work_data->png_stuff->png, 0, ErrorFn, WarningFn);
    if (setjmp(png_jmpbuf(work_data->png_stuff->png))) {
  		throw std::runtime_error("libpng error");
  	}

		// PNGs store 16 bit images as big endian
		if (!IsBigEndian()) { png_set_swap(work_data->png_stuff->png); }

		int32_t num_rows = png_get_image_height(work_data->png_stuff->png,
		                                        work_data->png_stuff->info);
	  size_t num_row_bytes = png_get_rowbytes(work_data->png_stuff->png,
		                                        work_data->png_stuff->info);

		png_bytep output_buffer =
		    reinterpret_cast<png_bytep>(work_data->output_buffer);
		std::vector<png_bytep> rows(num_rows);
	  for (int y = 0; y < num_rows; ++y) {
			rows[y] = &output_buffer[num_row_bytes * y];
		}
	  png_read_image(work_data->png_stuff->png, rows.data());
  } catch (const std::exception& exception) {
    work_data->exception.reset(new std::exception(exception));
	}
}

void AfterDecodingPngPixels(uv_work_t* decode_pixels_work) {
	auto work_data = reinterpret_cast<WorkData*>(decode_pixels_work->data);
  Isolate* isolate = work_data->isolate;

	try {
		HandleScope scope(isolate);

	  Local<Value> error = Undefined(isolate);
	  Local<Value> value = Undefined(isolate);

	  if (work_data->exception.get()) {
	    auto message = work_data->exception->what();
	    error = Exception::Error(String::NewFromUtf8(isolate, message));
	  } else {
			std::string data_type =
				kBitDepthMap.at(work_data->bit_depth) +
				kColorTypeMap.at(work_data->color_type);
			auto buffer = Local<Object>::New(isolate, work_data->output_node_buffer);

	    auto png_obj = Object::New(isolate);
	    png_obj->Set(String::NewFromUtf8(isolate, "width"),
	                 Number::New(isolate, work_data->width));
	    png_obj->Set(String::NewFromUtf8(isolate, "height"),
	                 Number::New(isolate, work_data->height));
		  png_obj->Set(String::NewFromUtf8(isolate, "dataType"),
	                 String::NewFromUtf8(isolate, data_type.c_str()));
		  png_obj->Set(String::NewFromUtf8(isolate, "buffer"), buffer);

	    value = png_obj;
	  }

	  auto callback = Local<Function>::New(isolate, work_data->callback);
	  Local<Value> argv[2] = { error, value };
	  callback->Call(isolate->GetCurrentContext()->Global(), 2, argv);

	  // Cleanup
	  work_data->callback.Reset();
		work_data->output_node_buffer.Reset();
	  delete work_data;
	  delete decode_pixels_work;
	} catch (const std::exception& ex) {
		addon::ThrowError(isolate, ex);
	}
}

} // End namespace decode_png_internal

void DecodePng(const FunctionCallbackInfo<Value>& info) {
	// Approach:
	// - First, read png info (async)
	// - Then, allocated a buffer of the correct size (main thread)
	// - Finally write into the buffer (async)
  Isolate* isolate = info.GetIsolate();
  HandleScope scope(isolate);

  auto work_data = new decode_png_internal::WorkData;
  work_data->isolate = isolate;
  work_data->callback.Reset(isolate, info[1].As<Function>());
  work_data->input_buffer = node::Buffer::Data(info[0]->ToObject());
  work_data->input_length = node::Buffer::Length(info[0]->ToObject());

  auto decode_info_work = new uv_work_t;
  decode_info_work->data = work_data;
  uv_queue_work(uv_default_loop(),
                decode_info_work,
                decode_png_internal::AsyncDecodePngInfo,
                (uv_after_work_cb)decode_png_internal::AfterDecodingPngInfo);
}

} // End namespace addon
