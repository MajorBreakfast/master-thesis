#include <string>
#include "p0se-js/native-aligner/native-aligner.h"
#include "../../utils/throw-error.h"

namespace p0se_js {

using v8::Array;
using v8::Context;
using v8::Exception;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::HandleScope;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Template;
using v8::Value;

struct NativeAlignerConstructorWorkData {
  v8::Isolate* isolate;
  std::unique_ptr<std::exception> exception;
  NativeContainer* native_container;
  int32_t width;
  int32_t height;
  int32_t num_levels;
  std::string mode;
  NativeAligner* native_aligner;
  v8::Persistent<v8::Object> self;
};

void NativeAligner::Constructor(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  HandleScope scope(isolate);

  if (!args.IsConstructCall()) {
    addon::ThrowTypeError(isolate,
      "Class constructor NativeAligner cannot be invoked without 'new'");
    return;
  }

  auto work_data = new NativeAlignerConstructorWorkData;
  work_data->isolate = isolate;
  work_data->native_container =
      ObjectWrap::Unwrap<NativeContainer>(args[0].As<Object>());
  work_data->width = args[1]->Int32Value();
  work_data->height = args[2]->Int32Value();
  work_data->num_levels = args[3]->Int32Value();
  work_data->mode = *String::Utf8Value(args[4]->ToString());
  work_data->self.Reset(isolate, args.This());

  auto work = new uv_work_t;
  work->data = work_data;
  uv_queue_work(uv_default_loop(),
                work,
                (uv_work_cb)NativeAligner::ConstructorAsync,
                (uv_after_work_cb)NativeAligner::ConstructorAfterAsync);
}

void NativeAligner::ConstructorAsync(uv_work_t* work) {
  auto work_data =
      reinterpret_cast<NativeAlignerConstructorWorkData*>(work->data);

	try {
    work_data->native_aligner = new NativeAligner(
      work_data->native_container,
      p0se::Aligner(work_data->native_container->GetContext(),
                    work_data->width,
                    work_data->height,
                    work_data->num_levels,
                    work_data->mode)
    );
  } catch (const std::exception& exception) {
    work_data->exception.reset(new std::exception(exception));
	}
}

void NativeAligner::ConstructorAfterAsync(uv_work_t* work) {
	auto work_data =
      reinterpret_cast<NativeAlignerConstructorWorkData*>(work->data);
  Isolate* isolate = work_data->isolate;
  HandleScope scope(isolate);

  auto self = Local<Object>::New(isolate, work_data->self);
  work_data->self.Reset();

  Local<Value> error = Undefined(isolate);

  if (work_data->exception.get()) {
    auto message = work_data->exception->what();
    error = Exception::Error(String::NewFromUtf8(isolate, message));
  } else {
    work_data->native_aligner->Wrap(self);
  }

  delete work_data;
  delete work;

  Local<Value> argv[1] = { error };
  self->Get(String::NewFromUtf8(isolate, "_constructedCallback")).As<Function>()
      ->Call(self, 1, argv);
}

}  // End of namespace p0se_js
