#include "p0se/images/device-image.h"
#include "p0se/logger/logger.h"
#include "p0se-js/native-aligner/native-aligner.h"
#include "p0se-js/native-device-buffer.h"
#include "../../utils/throw-error.h"
#include <node_buffer.h>
#include <map>
#include <string>

namespace p0se_js {

using v8::Array;
using v8::ArrayBuffer;
using v8::Context;
using v8::Exception;
using v8::Float32Array;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::HandleScope;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Template;
using v8::Value;

struct NativeAlignerRunWorkData {
  v8::Isolate* isolate;
  std::unique_ptr<std::exception> exception;
  NativeAligner* native_aligner;
  int32_t width;
  int32_t height;
  cl::Buffer pre_depth_device_buffer;
  cl::Buffer pre_rgb_device_buffer;
  cl::Buffer cur_depth_device_buffer;
  cl::Buffer cur_rgb_device_buffer;
  Eigen::Matrix3f camera_matrix;
  float depth_scale_factor;
  int32_t num_max_iterations_per_level;
  bool allow_early_break;
  Sophus::SE3f initial_motion;
  bool log;
  p0se::Aligner::Stats stats;
  std::unique_ptr<p0se::Logger> logger;
  Sophus::SE3f motion;
  Persistent<Function> callback;
};

void NativeAligner::Run(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  HandleScope scope(isolate);

  auto work_data = new NativeAlignerRunWorkData;
  work_data->isolate = isolate;
  work_data->native_aligner = ObjectWrap::Unwrap<NativeAligner>(args.This());
  work_data->width = args[0]->Int32Value();
  work_data->height = args[1]->Int32Value();
  work_data->pre_depth_device_buffer =
      ObjectWrap::Unwrap<NativeDeviceBuffer>(args[2].As<Object>())->GetBuffer();
  work_data->pre_rgb_device_buffer =
      ObjectWrap::Unwrap<NativeDeviceBuffer>(args[3].As<Object>())->GetBuffer();
  work_data->cur_depth_device_buffer =
      ObjectWrap::Unwrap<NativeDeviceBuffer>(args[4].As<Object>())->GetBuffer();
  work_data->cur_rgb_device_buffer =
      ObjectWrap::Unwrap<NativeDeviceBuffer>(args[5].As<Object>())->GetBuffer();
  args[6].As<Float32Array>()->CopyContents(work_data->camera_matrix.data(),
                                           9 * sizeof(float));
  work_data->depth_scale_factor = static_cast<float>(args[7]->NumberValue());
  work_data->num_max_iterations_per_level = args[8]->Int32Value();
  work_data->allow_early_break = args[9]->BooleanValue();
  float rot[4];
  args[10].As<Float32Array>()->CopyContents(rot, 4 * sizeof(float));
  Eigen::Quaternion<float> rotation(rot);
  Eigen::Vector3f translation;
  args[11].As<Float32Array>()->CopyContents(translation.data(),
                                            3 * sizeof(float));
  work_data->initial_motion = Sophus::SE3f(rotation, translation);
  work_data->log = args[12]->BooleanValue();
  work_data->callback.Reset(isolate, args[13].As<Function>());

  auto work = new uv_work_t;
  work->data = work_data;
  uv_queue_work(uv_default_loop(),
                work,
                (uv_work_cb)NativeAligner::RunAsync,
                (uv_after_work_cb)NativeAligner::RunAfterAsync);
}

void NativeAligner::RunAsync(uv_work_t* work) {
  auto work_data = reinterpret_cast<NativeAlignerRunWorkData*>(work->data);

	try {
    if (work_data->log) {
      work_data->logger.reset(new p0se::Logger());
    }

    work_data->native_aligner->aligner_.Run(
        // ToDo: For parallelism, the container should allow
        //       multiple command queues.
        work_data->native_aligner->native_container_->GetCommandQueue(),
        p0se::DeviceImage(work_data->width,
                          work_data->height,
                          p0se::Image::USHORT,
                          work_data->pre_depth_device_buffer),
        p0se::DeviceImage(work_data->width,
                          work_data->height,
                          p0se::Image::UCHAR3,
                          work_data->pre_rgb_device_buffer),
        p0se::DeviceImage(work_data->width,
                          work_data->height,
                          p0se::Image::USHORT,
                          work_data->cur_depth_device_buffer),
        p0se::DeviceImage(work_data->width,
                          work_data->height,
                          p0se::Image::UCHAR3,
                          work_data->cur_rgb_device_buffer),
        work_data->camera_matrix,
        work_data->depth_scale_factor,
        work_data->num_max_iterations_per_level,
        work_data->allow_early_break,
        work_data->initial_motion,
        &work_data->motion,
        &work_data->stats,
        work_data->logger.get());
  } catch (const std::exception& exception) {
    work_data->exception.reset(new std::exception(exception));
	}
}

void NativeAligner::RunAfterAsync(uv_work_t* work) {
	auto work_data = reinterpret_cast<NativeAlignerRunWorkData*>(work->data);
  Isolate* isolate = work_data->isolate;
  HandleScope scope(isolate);

  auto callback = Local<Function>::New(isolate, work_data->callback);
  work_data->callback.Reset();

  Local<Value> error = Undefined(isolate);
  Local<Value> value = Undefined(isolate);

  if (work_data->exception.get()) {
    auto message = work_data->exception->what();
    error = Exception::Error(String::NewFromUtf8(isolate, message));
  }

	try {
    Local<Object> result = Object::New(isolate);

    Local<Object> motion_obj = Object::New(isolate);
    result->Set(String::NewFromUtf8(isolate, "motion"), motion_obj);

    // motion.translation
    Local<ArrayBuffer> translation_ab =
        ArrayBuffer::New(isolate, 3 * sizeof(float));
    memcpy(translation_ab->GetContents().Data(),
           work_data->motion.translation().data(),
           3 * sizeof(float));
    motion_obj->Set(String::NewFromUtf8(isolate, "translation"),
                    translation_ab);

    // motion.rotation
    Eigen::Quaternion<float> q = work_data->motion.so3().unit_quaternion();
    Local<ArrayBuffer> rotation_ab =
        ArrayBuffer::New(isolate, 4 * sizeof(float));
    float quat_data[] = { q.x(), q.y(), q.z(), q.w() };
    memcpy(rotation_ab->GetContents().Data(),
           quat_data,
           4 * sizeof(float));
    motion_obj->Set(String::NewFromUtf8(isolate, "rotation"), rotation_ab);

    // Stats
    Local<Object> stats_obj = Object::New(isolate);
    result->Set(String::NewFromUtf8(isolate, "stats"), stats_obj);
    stats_obj->Set(String::NewFromUtf8(isolate, "duration"),
                   Number::New(isolate, work_data->stats.duration));
    Local<Object> level_stats_arr =
        Array::New(isolate, work_data->stats.level_stats.size());
    stats_obj->Set(String::NewFromUtf8(isolate, "levelStats"), level_stats_arr);

    for (int32_t i = 0; i < work_data->stats.level_stats.size(); ++i) {
      Local<Object> level_stats_obj = Object::New(isolate);
      level_stats_obj->Set(String::NewFromUtf8(isolate, "duration"),
                  Number::New(isolate, work_data->stats.level_stats[i].duration));
      level_stats_obj->Set(String::NewFromUtf8(isolate, "numIterations"),
                  Number::New(isolate, work_data->stats.level_stats[i].num_iterations));
      level_stats_arr->Set(i, level_stats_obj);
    }

    if (work_data->log) {
      auto& log_messages = work_data->logger->GetLogMessages();

      Local<Array> log_messages_array =
          Array::New(isolate, static_cast<int32_t>(log_messages.size()));
      result->Set(
          String::NewFromUtf8(isolate, "logMessages"),
          log_messages_array);

      for (int32_t i = 0; i < log_messages.size(); ++i) {
        Local<Object> log_message_obj = Object::New(isolate);
        if (auto m = dynamic_cast<p0se::StringLogMessage*>(log_messages[i].get())) {
          log_message_obj->Set(
              String::NewFromUtf8(isolate, "type"),
              String::NewFromUtf8(isolate, "string"));
          log_message_obj->Set(
              String::NewFromUtf8(isolate, "string"),
              String::NewFromUtf8(isolate, m->GetString().c_str()));
        }
        if (auto m = dynamic_cast<p0se::ImageLogMessage*>(log_messages[i].get())) {

          log_message_obj->Set(
              String::NewFromUtf8(isolate, "type"),
              String::NewFromUtf8(isolate, "image"));

          log_message_obj->Set(
              String::NewFromUtf8(isolate, "title"),
              String::NewFromUtf8(isolate, m->GetTitle().c_str()));

          Local<Object> buffer =
      		    node::Buffer::New(isolate, m->GetHostImage().GetNumBytes()).ToLocalChecked();
          memcpy(node::Buffer::Data(buffer),
                 m->GetHostImage().GetBuffer(),
                 m->GetHostImage().GetNumBytes());

          Local<Object> image_obj = Object::New(isolate);
          image_obj->Set(String::NewFromUtf8(isolate, "width"),
                         Number::New(isolate, m->GetHostImage().GetWidth()));
          image_obj->Set(String::NewFromUtf8(isolate, "height"),
                         Number::New(isolate, m->GetHostImage().GetHeight()));
          image_obj->Set(String::NewFromUtf8(isolate, "dataType"),
                         String::NewFromUtf8(isolate,
                             m->GetHostImage().GetDataTypeAsString().c_str()));
          image_obj->Set(String::NewFromUtf8(isolate, "buffer"), buffer);

          log_message_obj->Set(String::NewFromUtf8(isolate, "image"), image_obj);
        }
        if (auto m = dynamic_cast<p0se::HistogramLogMessage*>(log_messages[i].get())) {
          log_message_obj->Set(
              String::NewFromUtf8(isolate, "type"),
              String::NewFromUtf8(isolate, "histogram"));
          log_message_obj->Set(
              String::NewFromUtf8(isolate, "title"),
              String::NewFromUtf8(isolate, m->GetTitle().c_str()));

          Local<Array> values_array =
              Array::New(isolate, m->GetHistogram().GetSize());
          log_message_obj->Set(
              String::NewFromUtf8(isolate, "values"),
              values_array);

          Local<Array> value_arr = Array::New(isolate, 2);
          values_array->Set(0, value_arr);
          value_arr->Set(0, String::NewFromUtf8(isolate, "x"));
          value_arr->Set(1, String::NewFromUtf8(isolate, "y"));

          for (int i = 0; i < m->GetHistogram().GetSize(); ++i) {
            float x = m->GetHistogram().GetMinValue() + i * m->GetHistogram().GetBucketWidth();
            int32_t y = m->GetHistogram()[i];

            /*Local<Object> value_obj = Object::New(isolate);
            values_array->Set(i, value_obj);

            value_obj->Set(
                String::NewFromUtf8(isolate, "x"),
                Number::New(isolate, x));
            value_obj->Set(
                String::NewFromUtf8(isolate, "y"),
                Number::New(isolate, y));*/

            Local<Array> value_arr = Array::New(isolate, 2);
            values_array->Set(i + 1, value_arr);

            value_arr->Set(0, Number::New(isolate, x));
            value_arr->Set(1, Number::New(isolate, y));
          }
        }
        log_messages_array->Set(i, log_message_obj);
      }
    } // End of logging code
    value = result;
	} catch (const std::exception& exception) {
    error = Exception::Error(String::NewFromUtf8(isolate, exception.what()));
    value = Undefined(isolate);
	}

  delete work_data;
  delete work;
  Local<Value> argv[2] = { error, value };
  callback->Call(isolate->GetCurrentContext()->Global(), 2, argv);
}

}  // End of namespace p0se_js
