#include "p0se-js/native-aligner/native-aligner.h"
#include "../../utils/throw-error.h"

namespace p0se_js {

using v8::Array;
using v8::Context;
using v8::Exception;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::HandleScope;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Template;
using v8::Value;

void NativeAligner::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, Constructor);
  tpl->SetClassName(String::NewFromUtf8(isolate, "NativeAligner"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype methods
  NODE_SET_PROTOTYPE_METHOD(tpl, "_getBuildLog", GetBuildLog);
  NODE_SET_PROTOTYPE_METHOD(tpl, "run", Run);

  Local<Function> fn = tpl->GetFunction();

  exports->Set(String::NewFromUtf8(isolate, "NativeAligner"), fn);
}

void NativeAligner::GetBuildLog(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  NativeAligner* native_aligner = ObjectWrap::Unwrap<NativeAligner>(args.This());
  const std::string& log = native_aligner->aligner_.GetBuildLog();
  args.GetReturnValue().Set(String::NewFromUtf8(isolate, log.c_str()));
}

}  // End of namespace p0se_js
