#pragma once

#include <node.h>
#include <node_object_wrap.h>
#include <uv.h>
#include <CL/cl2.hpp>
#include <cinttypes>
#include "p0se/aligner/aligner.h"
#include "p0se-js/native-container.h"

namespace p0se_js {

class NativeAligner : public node::ObjectWrap {
 public:
  static void Init(v8::Local<v8::Object> exports);

 private:
  NativeAligner::NativeAligner(NativeContainer* native_container, p0se::Aligner&& aligner)
    : native_container_(native_container),
      aligner_(aligner) {}

  NativeContainer* native_container_;
  p0se::Aligner aligner_;

  static void Constructor(const v8::FunctionCallbackInfo<v8::Value>& args);
  static void ConstructorAsync(uv_work_t* work);
  static void ConstructorAfterAsync(uv_work_t* work);

  static void Run(const v8::FunctionCallbackInfo<v8::Value>& args);
  static void RunAsync(uv_work_t* work);
  static void RunAfterAsync(uv_work_t* work);

  static void GetBuildLog(const v8::FunctionCallbackInfo<v8::Value>& args);
};

}  // End of namespace p0se_js
