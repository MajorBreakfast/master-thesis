#include "p0se-js/native-container.h"
#include <stdexcept>
#include "../utils/throw-error.h"

namespace p0se_js {

using v8::Array;
using v8::Context;
using v8::Exception;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Template;
using v8::Value;

NativeContainer::NativeContainer(int32_t platform_index, int32_t device_index) {
  // Platform
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
  if (platform_index >= platforms.size()) {
    throw std::runtime_error("Invalid platform index");
  }
  platform_ = platforms[platform_index];

  // Device
  std::vector<cl::Device> devices;
  platform_.getDevices(CL_DEVICE_TYPE_ALL, &devices);
  if (device_index >= devices.size()) {
    throw std::runtime_error("Invalid device index");
  }
  device_ = devices[device_index];

  // Context and CommandQueue
  context_ = cl::Context(device_);
  command_queue_ = cl::CommandQueue(context_, device_);
  out_of_order_command_queue_ = cl::CommandQueue(context_, device_, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);
}

NativeContainer::~NativeContainer() {
}

void NativeContainer::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, Constructor);
  tpl->SetClassName(String::NewFromUtf8(isolate, "NativeContainer"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  Local<Function> fn = tpl->GetFunction();

  // Static methods
  NODE_SET_METHOD(fn.As<Object>(), "getPlatforms", GetPlatforms);

  exports->Set(String::NewFromUtf8(isolate, "NativeContainer"), fn);
}

void NativeContainer::Constructor(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  if (!args.IsConstructCall()) {
    addon::ThrowTypeError(isolate,
      "Class constructor NativeContainer cannot be invoked without 'new'");
    return;
  }

  if (!args[0]->IsObject()) {
    addon::ThrowError(isolate, "Expected options object");
    return;
  }
  Local<Object> options = args[0].As<Object>();

  int32_t platform_index = options->Get(String::NewFromUtf8(isolate, "platformIndex"))
                                  ->Int32Value();
  int32_t device_index = options->Get(String::NewFromUtf8(isolate, "deviceIndex"))
                                ->Int32Value();

  NativeContainer* obj = new NativeContainer(platform_index, device_index);
  obj->Wrap(args.This());
  args.GetReturnValue().Set(args.This());
}

// Static method
void NativeContainer::GetPlatforms(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);

  int32_t num_platforms = static_cast<int32_t>(platforms.size());
  Local<Array> platformObjects = Array::New(isolate, num_platforms);
  for (int32_t i = 0; i < num_platforms; ++i) {
    cl::Platform& platform = platforms[i];

    std::vector<cl::Device> devices;
    platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

    int32_t num_devices = static_cast<int32_t>(devices.size());
    Local<Array> deviceObjects = Array::New(isolate, num_devices);

    for (int32_t j = 0; j < num_devices; ++j) {
      cl::Device device = devices[j];

      Local<Object> deviceObject = Object::New(isolate);

      deviceObject->Set(String::NewFromUtf8(isolate, "name"),
                        String::NewFromUtf8(isolate, device.getInfo<CL_DEVICE_NAME>().c_str()));
      deviceObject->Set(String::NewFromUtf8(isolate, "version"),
                        String::NewFromUtf8(isolate, device.getInfo<CL_DEVICE_VERSION>().c_str()));

      deviceObjects->Set(j, deviceObject);
    }

    Local<Object> platformObject = Object::New(isolate);

    platformObject->Set(String::NewFromUtf8(isolate, "name"),
                        String::NewFromUtf8(isolate, platform.getInfo<CL_PLATFORM_NAME>().c_str()));
    platformObject->Set(String::NewFromUtf8(isolate, "vendor"),
                        String::NewFromUtf8(isolate, platform.getInfo<CL_PLATFORM_VENDOR>().c_str()));
    platformObject->Set(String::NewFromUtf8(isolate, "version"),
                        String::NewFromUtf8(isolate, platform.getInfo<CL_PLATFORM_VERSION>().c_str()));
    platformObject->Set(String::NewFromUtf8(isolate, "profile"),
                        String::NewFromUtf8(isolate, platform.getInfo<CL_PLATFORM_PROFILE>().c_str()));
    platformObject->Set(String::NewFromUtf8(isolate, "devices"), deviceObjects);

    platformObjects->Set(i, platformObject);
  }

  args.GetReturnValue().Set(platformObjects);
}


}  // End of namespace p0se_js
