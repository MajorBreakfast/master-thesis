#pragma once

#include <node.h>
#include <node_object_wrap.h>
#include <CL/cl2.hpp>
#include <cinttypes>

namespace p0se_js {

class NativeContainer : public node::ObjectWrap {
 public:
  static void Init(v8::Local<v8::Object> exports);

  const cl::Platform& GetPlatform() const { return platform_; }
  const cl::Device& GetDevice() const { return device_; }
  const cl::Context& GetContext() const { return context_; }
  const cl::CommandQueue& GetCommandQueue() const { return command_queue_; }
  const cl::CommandQueue& GetOutOfOrderCommandQueue() const {
      return out_of_order_command_queue_; }

 private:
  explicit NativeContainer(int32_t platform_index, int32_t device_index);
  ~NativeContainer();

  // Methods
  static void Constructor(const v8::FunctionCallbackInfo<v8::Value>& args);

  // Static methods
  static void GetPlatforms(const v8::FunctionCallbackInfo<v8::Value>& args);

  cl::Platform platform_;
  cl::Device device_;
  cl::Context context_;
  cl::CommandQueue command_queue_;
  cl::CommandQueue out_of_order_command_queue_;
};

}  // End of namespace p0se_js
