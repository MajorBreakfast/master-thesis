#include "p0se-js/native-device-buffer.h"
#include <stdexcept>
#include <map>
#include <string>
#include <exception>
#include <stdexcept>
#include <node_buffer.h>
#include "../utils/throw-error.h"

namespace p0se_js {

using v8::Array;
using v8::Context;
using v8::Exception;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Template;
using v8::Value;

NativeDeviceBuffer::NativeDeviceBuffer(NativeContainer* native_container,
                                       int32_t length)
  : native_container_(native_container) {
  buffer_ = cl::Buffer(native_container->GetContext(),
                       CL_MEM_READ_WRITE,
                       length);
}

NativeDeviceBuffer::~NativeDeviceBuffer() {
}

void NativeDeviceBuffer::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, Constructor);
  tpl->SetClassName(String::NewFromUtf8(isolate, "NativeDeviceBuffer"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype methods
  NODE_SET_PROTOTYPE_METHOD(tpl, "_getLength", GetLength);
  NODE_SET_PROTOTYPE_METHOD(tpl, "readSync", ReadSync);
  NODE_SET_PROTOTYPE_METHOD(tpl, "writeSync", WriteSync);

  Local<Function> fn = tpl->GetFunction();

  exports->Set(String::NewFromUtf8(isolate, "NativeDeviceBuffer"), fn);
}

void NativeDeviceBuffer::Constructor(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  try {
    if (!args.IsConstructCall()) {
      addon::ThrowTypeError(isolate,
        "Class constructor NativeDeviceBuffer cannot be invoked without 'new'");
      return;
    }

    NativeContainer* native_container =
        ObjectWrap::Unwrap<NativeContainer>(args[0].As<Object>());
    int32_t length = args[1]->Int32Value();

    NativeDeviceBuffer* native_device_buffer =
        new NativeDeviceBuffer(native_container, length);
    native_device_buffer->Wrap(args.This());

    args.GetReturnValue().Set(args.This());
  } catch (const std::exception& ex) {
    addon::ThrowError(isolate, ex);
  }
}

void NativeDeviceBuffer::GetLength(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  try {
    NativeDeviceBuffer* native_device_buffer =
        ObjectWrap::Unwrap<NativeDeviceBuffer>(args.This());

    size_t size = native_device_buffer->buffer_.getInfo<CL_MEM_SIZE>();
    args.GetReturnValue().Set(Number::New(isolate, static_cast<double>(size)));
  } catch (const std::exception& ex) {
    addon::ThrowError(isolate, ex);
  }
}

void NativeDeviceBuffer::ReadSync(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  try {
    NativeDeviceBuffer* native_device_buffer =
        ObjectWrap::Unwrap<NativeDeviceBuffer>(args.This());

    char* host_buffer = node::Buffer::Data(args[0]->ToObject());
    size_t size = node::Buffer::Length(args[0]->ToObject());

    cl::CommandQueue command_queue =
        native_device_buffer->native_container_->GetOutOfOrderCommandQueue();
    cl::Buffer buffer = native_device_buffer->buffer_;

    command_queue.enqueueReadBuffer(buffer, true, 0, size, host_buffer);
  } catch (const std::exception& ex) {
    addon::ThrowError(isolate, ex);
  }
}

void NativeDeviceBuffer::WriteSync(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  try {
    NativeDeviceBuffer* native_device_buffer =
        ObjectWrap::Unwrap<NativeDeviceBuffer>(args.This());

    char* host_buffer = node::Buffer::Data(args[0]->ToObject());
    size_t size = node::Buffer::Length(args[0]->ToObject());

    cl::CommandQueue command_queue =
        native_device_buffer->native_container_->GetOutOfOrderCommandQueue();
    cl::Buffer buffer = native_device_buffer->buffer_;

    command_queue.enqueueWriteBuffer(buffer, true, 0, size, host_buffer);
  } catch (const std::exception& ex) {
    addon::ThrowError(isolate, ex);
  }
}

}  // End of namespace p0se_js
