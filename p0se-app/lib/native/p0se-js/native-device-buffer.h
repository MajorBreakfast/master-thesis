#pragma once

#include <node.h>
#include <node_object_wrap.h>
#include <CL/cl2.hpp>
#include <cinttypes>
#include "p0se-js/native-container.h"

namespace p0se_js {

class NativeDeviceBuffer : public node::ObjectWrap {
 public:
  static void Init(v8::Local<v8::Object> exports);

  const cl::Buffer& GetBuffer() const { return buffer_; }
  cl::Buffer& GetBuffer() { return buffer_; }

 private:
  explicit NativeDeviceBuffer(NativeContainer* native_container,
                        int32_t length);
  ~NativeDeviceBuffer();

  static void Constructor(const v8::FunctionCallbackInfo<v8::Value>& args);
  static void GetLength(const v8::FunctionCallbackInfo<v8::Value>& args);
  static void ReadSync(const v8::FunctionCallbackInfo<v8::Value>& args);
  static void WriteSync(const v8::FunctionCallbackInfo<v8::Value>& args);

  cl::Buffer buffer_;
  NativeContainer* native_container_;
};

}  // End of namespace p0se_js
