#include <exception>
#include <node.h>
#include <string>

namespace addon {

inline void ThrowError(v8::Isolate* isolate, const std::exception& exception) {
  isolate->ThrowException(v8::Exception::Error(
    v8::String::NewFromUtf8(isolate, exception.what())
  ));
}

inline void ThrowError(v8::Isolate* isolate, const std::string& string) {
  isolate->ThrowException(v8::Exception::Error(
    v8::String::NewFromUtf8(isolate, string.c_str())
  ));
}

inline void ThrowTypeError(v8::Isolate* isolate, const std::exception& exception) {
  isolate->ThrowException(v8::Exception::Error(
    v8::String::NewFromUtf8(isolate, exception.what())
  ));
}

inline void ThrowTypeError(v8::Isolate* isolate, const std::string& string) {
  isolate->ThrowException(v8::Exception::Error(
    v8::String::NewFromUtf8(isolate, string.c_str())
  ));
}

} // End namepace addon
