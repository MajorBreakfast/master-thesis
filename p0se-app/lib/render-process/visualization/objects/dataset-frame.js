'use strict'
const addon = require('bindings')('addon')
const THREE = require('three')
const { mat3 } = require('gl-matrix')
const RGBDPoints = require('./rgbd-points.js')
const Point = require('./point.js')

class DatasetFrameObject extends THREE.Object3D {
  constructor (datasetFrame, rgbImage, depthImage, pose) {
    super()

    this.position.fromArray(pose.translation)
    this.quaternion.fromArray(pose.rotation)

    const rgbdPoints = RGBDPoints.create(
      rgbImage,
      depthImage,
      datasetFrame.dataset.depthScaleFactor,
      datasetFrame.dataset.inverseCameraMatrix
    )

    this.add(rgbdPoints)

    this.add(new Point())
  }
}

module.exports = DatasetFrameObject
