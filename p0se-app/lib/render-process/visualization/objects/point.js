'use strict'
const THREE = require('three')

class Point extends THREE.Points {
  constructor (options = {}) {
    const geometry = new THREE.Geometry()
    geometry.vertices.push(new THREE.Vector3(0, 0, 0 ))

    const textureLoader = new THREE.TextureLoader()

    const disc = textureLoader.load('visualization/sprites/disc.png')
    const material = new THREE.PointsMaterial({
      size: options.pointSize || 0.1,
      color: 0xffffff,
      map: disc,
      blending: THREE.AdditiveBlending,
      transparent : true
    })

    super(geometry, material)
  }
}

module.exports = Point
