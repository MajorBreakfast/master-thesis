'use strict'
const addon = require('bindings')('addon')
const THREE = require('three')
const { mat3 } = require('gl-matrix')

let calledConstructorCorrectlySymbol = Symbol('calledConstructorCorrectly')

class RGBDPoints extends THREE.Points {
  constructor (symbol, geometry, material) {
    if (symbol != calledConstructorCorrectlySymbol) {
      throw new Error('Use RGBDPoints.create() to create a new instance')
    }
    super(geometry, material)
  }

  static create (rgbImage,
                 depthImage,
                 depthScaleFactor,
                 inverseCameraMatrix) {
    const inverseCameraMatrixRowMajor = mat3.create()
    mat3.transpose(inverseCameraMatrixRowMajor, inverseCameraMatrix)

    const [count, positionsArrayBuffer, colorsArrayBuffer] = addon.createGeometry(
     rgbImage.buffer,
     depthImage.buffer,
     rgbImage.width,
     rgbImage.height,
     inverseCameraMatrixRowMajor,
     depthScaleFactor
    )

    const colors = new Float32Array(colorsArrayBuffer, 0, count * 3)
    const positions = new Float32Array(positionsArrayBuffer, 0, count * 3)

    const geometry = new THREE.BufferGeometry()
    geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3))
    geometry.addAttribute('color', new THREE.BufferAttribute(colors, 3))

    const material = new THREE.PointsMaterial({
      size: 0.015,
      vertexColors: THREE.VertexColors
    })

    return new RGBDPoints(calledConstructorCorrectlySymbol, geometry, material)
  }
}

module.exports = RGBDPoints
