const parseColor = require('color-parser')
const THREE = require('three')

class TrajectoryLine extends THREE.Line {
  constructor (positions, options = {}) {
    const geometry = new THREE.BufferGeometry()
    geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3))
    geometry.dynamic = true;
    
    const material = new THREE.LineBasicMaterial({
      color: rgbToNumber(parseColor(options.color || '#399aef'))
    })

    super(geometry, material)
  }

  set positions (positions) {
    this.geometry.attributes.position.array = positions
    this.geometry.attributes.position.needsUpdate = true
  }

  static fromPoses (poses, options) {
    const positions = new Float32Array(poses.length * 3)

    for (let i = 0; i < poses.length; ++i) {
      positions.set(poses[i].translation, i * 3)
    }

    return new TrajectoryLine(positions, options)
  }
}

function rgbToNumber({ r, g, b }) {
  return 256 * 256 * r + 256 * g + b
}

module.exports = TrajectoryLine
