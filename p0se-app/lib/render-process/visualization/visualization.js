const THREE = require('three')
const OrbitControls = require('three-orbit-controls')(THREE)
const Stats = require('stats.js')

class Visualization {
  constructor () {
    // Scene
    const scene = new THREE.Scene()
    scene.rotation.x = -Math.PI / 2 // Z axis is up
    this._scene = scene

    // Axis
    var axisHelper = new THREE.AxisHelper(1)
    scene.add(axisHelper)

    // Renderer
    const renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true
    })
    renderer.setPixelRatio(window.devicePixelRatio)
    this._renderer = renderer
    this.element = renderer.domElement

    // Camera
    const camera = new THREE.PerspectiveCamera(75, 1, 0.01, 1000)
    camera.position.z = -5
    this._camera = camera

    // Controls
    const controls = new OrbitControls(camera, renderer.domElement)
    controls.enableDamping = true
    controls.dampingFactor = 0.25
    //controls.autoRotate = true
    controls.enableZoom = true
    this._controls = controls

    // Stats
    const stats = new Stats()
    this._stats = stats
    this.statsElement = stats.dom
  }

  update () {
    this._controls.update()
    this._renderer.render(this._scene, this._camera)
    this._stats.update()
  }

  resize (width, height) {
    this._camera.aspect = width / height
    this._camera.updateProjectionMatrix()
    this._renderer.setSize(width, height)
  }

  add (object) {
    this._scene.add(object)
  }

  delete (object) {
    this._scene.remove(object)
  }

  clear () {
    for (let child of this._scene.children) {
      this._scene.remove(child)
    }
  }
}

module.exports = Visualization
