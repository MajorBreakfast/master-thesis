;(function () {
  /**
  The `ZoomViewMouseGesture` class handles the mouse events. It takes three
  callback methods:
  - `start()` Called when a new mouse gesture starts
  - `update({ deltaPosition, deltaScale, center })` Called everytime the mouse
    cursor is dragged or the wheel was turned. The `center` can and should be
    used as a fixpoint when scaling the content.
  - `end({ velocity })` Called at the end of the mouse gesture. The callback
    provides the `velocity` (in px/ms) which can be used to animate the content.
  */
  var ZoomViewMouseGesture = function (element, callbacks) {
    this._callbacks = callbacks
    this._element = element

    var e = element
    var w = window
    this._listeners = [
      { target: e, type: 'mousedown', fn: this._onMouseDown.bind(this) },
      { target: e, type: 'wheel', fn: this._onWheel.bind(this) },
      { target: e, type: 'dragstart', fn: this._onDragStart.bind(this) },
      { target: w, type: 'mousemove', fn: this._onGlobalMouseMove.bind(this) },
      { target: w, type: 'mouseup', fn: this._onGlobalMouseUp.bind(this) }
    ]
    this._listeners.forEach(function (l) {
      l.target.addEventListener(l.type, l.fn)
    })

    this._position = [0, 0]
    this._velocity = [0, 0]
    this._isMouseDown = false
    this._wasWheelTurned
    this._wheelTimeoutId = 0
  }

  ZoomViewMouseGesture.prototype = {
    destroy: function () {
      this._listeners.forEach(function (l) {
        l.target.removeEventListener(l.type, l.fn)
      })
    },

    _onMouseDown: function (event) {
      this._isMouseDown = true
      this._time = currentTimeInSeconds()
      this._startGestureIfAppropriate()
    },

    _onWheel: function (event) {
      this._wasWheelTurned = true
      this._startGestureIfAppropriate()

      var multiplier = 1
      if (event.deltaMode === 1) { multiplier = 10 }

      var deltaScale = Math.exp(-0.002 * event.deltaY * multiplier)
      this._callbacks.update({
        deltaPosition: [0, 0],
        deltaScale: deltaScale,
        center: this._position
      })

      clearTimeout(this._wheelTimeoutId)
      this._wheelTimeoutId = setTimeout(function () {
        this._wasWheelTurned = false
        this._endGestureIfAppropriate()
      }.bind(this), 150)
    },

    _onDragStart: function (event) {
      event.preventDefault()
    },

    _onGlobalMouseMove: function (event) {
      var prevPosition = this._position

      var rect = this._element.getBoundingClientRect()
      var position = [event.clientX - rect.left, event.clientY - rect.top]

      this._position = position

      var time = currentTimeInSeconds()
      var prevTime = this._time
      this._time = time

      if (this._isMouseDown) {
        var deltaPosition = [position[0] - prevPosition[0],
                             position[1] - prevPosition[1]]

        var deltaTime = time - prevTime
        this._velocity = [deltaPosition[0] / deltaTime,
                          deltaPosition[1] / deltaTime]

        this._callbacks.update({
          deltaPosition: deltaPosition,
          deltaScale: 1,
          center: position // Not really need, 'cause scale is 1
        })
      }
    },

    _onGlobalMouseUp: function () {
      this._isMouseDown = false
      this._endGestureIfAppropriate()
      this._velocity = [0, 0]
    },

    _startGestureIfAppropriate: function () {
      if (!this._isStarted) {
        this._isStarted = true
        this._callbacks.start()
      }
    },

    _endGestureIfAppropriate: function () {
      if (this._isStarted && !this._wasWheelTurned && !this._isMouseDown) {
        this._isStarted = false
        this._callbacks.end({ velocity: this._velocity })
      }
    }
  }

  function currentTimeInSeconds () {
    return performance.now() / 1000
  }

  window.ZoomViewMouseGesture = ZoomViewMouseGesture
})()
