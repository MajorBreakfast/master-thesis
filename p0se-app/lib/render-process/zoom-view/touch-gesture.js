;(function () {
  /**
  The `ZoomViewTouchGesture` class handles the touchstart, touchmove and
  touchstart events. It takes three callback methods:
  - `start()` Called when a new touch gesture starts
  - `update({ deltaPosition, deltaScale, center })` Called everytime the touch
    points on screen move. The `center` can and should be used as a fixpoint
    when scaling the content.
  - `end({ velocity })` Called at the end of the touch gesture. The callback
    provides the `velocity` (in px/ms) which can be used to animate the content.
  */
  var ZoomViewTouchGesture = function (element, callbacks) {
    this._callbacks = callbacks
    this._element = element

    var e = element
    this._listeners = [
      { target: e, type: 'touchstart', fn: this._onTouchStart.bind(this) },
      { target: e, type: 'touchmove', fn: this._onTouchMove.bind(this) },
      { target: e, type: 'touchend', fn: this._onTouchEnd.bind(this) },
      { target: e, type: 'touchcancel', fn: this._onTouchEnd.bind(this) }
    ]
    this._listeners.forEach(function (l) {
      l.target.addEventListener(l.type, l.fn)
    })

    this._touchMap = new Map()
  }

  ZoomViewTouchGesture.prototype = {
    destroy: function () {
      this._listeners.forEach(function (l) {
        l.target.removeEventListener(l.type, l.fn)
      })
    },

    _onTouchStart: function (event) {
      event.preventDefault()
      event.stopPropagation()
      var prevNumTouches = this._touchMap.size

      var changedTouches = event.changedTouches
      for (var i = 0; i < changedTouches.length; ++i) {
        var touch = changedTouches[i]
        var rect = this._element.getBoundingClientRect()
        var position = [touch.clientX - rect.left, touch.clientY - rect.top]
        this._touchMap.set(touch.identifier, position)
      }

      if (prevNumTouches === 0) {
        this._velocity = [0, 0]
        this._time = currentTimeInSeconds()
        this._callbacks.start()
      }
    },

    _onTouchMove: function (event) {
      event.preventDefault()
      event.stopPropagation()
      var changedTouches = event.changedTouches
      for (var i = 0; i < changedTouches.length; ++i) {
        var touch = changedTouches[i]
        var rect = this._element.getBoundingClientRect()
        var position = [touch.clientX - rect.left, touch.clientY - rect.top]
        if (this._touchMap.has(touch.identifier)) {
          this._touchMap.set(touch.identifier, position)
        }
      }

      var numTouches = this._touchMap.size
      var time = currentTimeInSeconds()

      // Compute center
      var sum = [0, 0]
      this._touchMap.forEach(function (position) {
        for (var dim = 0; dim < 2; ++dim) { sum[dim] += position[dim] }
      })
      var center = [sum[0] / numTouches, sum[1] / numTouches]

      // Compute distance
      var distance = 0
      this._touchMap.forEach(function (position) {
        var a = position[0] - center[0]
        var b = position[1] - center[1]
        distance += Math.sqrt(a*a + b*b)
      })

      // Load previous values
      var prevCenter = this._center
      var prevDistance = this._distance
      var prevTime = this._time
      var prevNumTouches = this._numTouches

      // Store current values
      this._center = center
      this._distance = distance
      this._time = time
      this._numTouches = numTouches

      if (numTouches === prevNumTouches) {
        var deltaScale = 1
        if (distance > 0 && prevDistance > 0) {
          deltaScale = distance / prevDistance
        }
        var deltaPosition = [center[0] - prevCenter[0],
                             center[1] - prevCenter[1]]

        var deltaTime = time - prevTime
        this._velocity = [deltaPosition[0] / deltaTime,
                          deltaPosition[1] / deltaTime]

        this._callbacks.update({
          deltaPosition: deltaPosition,
          deltaScale: deltaScale,
          center: prevCenter
        })
      }
    },

    _onTouchEnd: function () {
      event.preventDefault()
      event.stopPropagation()
      var changedTouches = event.changedTouches
      for (var i = 0; i < changedTouches.length; ++i) {
        var touch = changedTouches[i]
        this._touchMap.delete(touch.identifier)
      }

      var numTouches = this._touchMap.size

      this._numTouches = 0

      if (numTouches === 0) {
        this._callbacks.end({ velocity: this._velocity})
      }
    }
  }

  function currentTimeInSeconds () {
    return performance.now() / 1000
  }

  window.ZoomViewTouchGesture = ZoomViewTouchGesture
})()
