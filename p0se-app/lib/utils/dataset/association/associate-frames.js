const forEachClosest = require('./for-each-closest')

/**
Associates two arrays with timestamped objects in pairs so that:
- Each pair contains two objects that are maximally close to each other with
  respect to their timestamps
- But no object appears more than once in any pair

@function associateFrames
@param array1 First array with { timestamp, ... } objects
@param array2 Second array with { timestamp, ... } objects
*/

function associateFrames (array1, array2) {
  if (array1.length === 0 || array2.length === 0) { return [] }

  // Approach:
  // 1. First find for each object in the first array1 the closest object in the
  //    second array
  // 2. Then find for each object in the second array the closest object in the
  //    first array. If the two objects are also the closest to each other in
  //    the other direction (which was computed in first step), associate the
  //    two objects with each other

  const closestIndex2ForIndex1 = new Array(array1.length)

  // Forward
  forEachClosest(array1, array2, (_, __, index1, closestIndex2) => {
    closestIndex2ForIndex1[index1] = closestIndex2
  })

  const association = []

  // Backwards
  forEachClosest(array2, array1, (_, __, index2, closestIndex1) => {
    if (closestIndex2ForIndex1[closestIndex1] === index2) {
      association.push([array1[closestIndex1], array2[index2]])
    }
  })

  return association
}

module.exports = associateFrames
