/**
Finds for each item in array1 the closest item in array2.

@function forEachClosest
@param array1
@param array2
@param callback Callback of form (item1, closestItem2, index1, closestIndex2)
*/
function forEachClosest (array1, array2, callback) {
  let index2 = 0
  let item2 = array2[0]

  for (let index1 = 0; index1 < array1.length; ++index1) {
    const item1 = array1[index1]
    const timestamp1 = item1.timestamp

    // Increase index2 until closest second object is found
    let diff = Math.abs(timestamp1 - array2[index2].timestamp)
    while (true) {
      const newItem2 = array2[index2 + 1]
      if (!newItem2) { break }
      const newDiff = Math.abs(timestamp1 - newItem2.timestamp)
      if (newDiff < diff) { // Minimize time difference
        diff = newDiff
        item2 = newItem2
        index2 += 1
      } else { break }
    }

    callback(item1, item2, index1, index2)
  }
}

module.exports = forEachClosest
