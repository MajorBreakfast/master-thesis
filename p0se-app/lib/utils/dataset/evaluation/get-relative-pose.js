const { quat, mat4, vec3 } = require('gl-matrix')

const preMat = mat4.create()
const preMatInv = mat4.create()
const curMat = mat4.create()
const relMat = mat4.create()

function getRelativePose (beforePose, afterPose) {
  const beforeTimestamp = beforePose.timestamp
  const afterTimestamp = afterPose.timestamp
  const translation = vec3.create()
  const rotation = quat.create()

  // This could be implemeted without the matrices
  mat4.fromRotationTranslation(preMat, beforePose.rotation, beforePose.translation)
  mat4.invert(preMatInv, preMat)
  mat4.fromRotationTranslation(curMat, afterPose.rotation, afterPose.translation)
  mat4.multiply(relMat, preMatInv, curMat)
  mat4.getTranslation(translation, relMat)
  mat4.getRotation(rotation, relMat)
  quat.normalize(rotation, rotation)

  return { beforeTimestamp, afterTimestamp, translation, rotation }
}

module.exports = getRelativePose
