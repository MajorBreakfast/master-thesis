/**
Creates a frames array from its string representation. A frames array is an
array of frame objects of the form { timestamp, path }.

Example input string:
```
# Comments: Start with a hash sign and will be ignored
1341847980.722988 rgb/1341847980.722988.png
1341847980.754743 rgb/1341847980.754743.png
1341847980.786856 rgb/1341847980.786856.png
...
```

Example output frames array:
```
[
  {
    timestamp: 1341847980.722988,
    path: 'rgb/1341847980.722988.png'
  }, ...
]
```
*/

function parseFrames (string) {
  const images = []

  const lines = string.split('\n')

  for (let line of lines) {
    if (line[0] === '#') { continue } // Skip comment lines

    items = line.split(' ')
    if (items.length === 2) {
      images.push({
        timestamp: parseFloat(items[0], 10),
        path: items[1]
      })
    }
  }

  return images
}

module.exports = parseFrames
