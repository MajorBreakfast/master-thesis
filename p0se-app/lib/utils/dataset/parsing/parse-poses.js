const { vec3, quat } = require('gl-matrix')

/**
Creates a poses from its string representation. The output is an array
of pose objects of the form { timestamp, translation, rotation }.

Example input string:
```
# Comments: Start with a hash sign and will be ignored
1341847980.7900 -0.6832 2.6909 1.7373 0.0003 0.8617 -0.5072 -0.0145
1341847980.8000 -0.6829 2.6910 1.7373 0.0002 0.8613 -0.5078 -0.0146
1341847980.8100 -0.6824 2.6913 1.7372 0.0004 0.8610 -0.5084 -0.0148
...
```

Example output poses:
```
[
  {
    timestamp: 1341847980.7900,
    translation: new Float32Array([-0.6829, 2.6910, 1.7373]),
    rotation: new Float32Array([0.0004, 0.8610, -0.5084, -0.0148])
  }, ...
]
```
*/

function parsePoses (string, { normalizeQuaternion = true } = {}) {
  const poses = []

  const lines = string.split('\n')

  for (let line of lines) {
    if (line[0] === '#') { continue } // Skip comment lines

    items = line.split(' ')
    if (items.length === 8) {
      const timestamp = parseFloat(items[0], 10)
      const translation = vec3.fromValues(parseFloat(items[1], 10),
                                       parseFloat(items[2], 10),
                                       parseFloat(items[3], 10))
      const rotation = quat.fromValues(parseFloat(items[4], 10),
                                       parseFloat(items[5], 10),
                                       parseFloat(items[6], 10),
                                       parseFloat(items[7], 10))
      if (normalizeQuaternion) { quat.normalize(rotation, rotation) }
      poses.push({ timestamp, translation, rotation })
    }
  }

  return poses
}

module.exports = parsePoses
