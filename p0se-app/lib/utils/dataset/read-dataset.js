const { readFile } = require('fs-promise')
const { join, resolve } = require('path')
const { mat3 } = require('gl-matrix')
const associateFrames = require('./association/associate-frames')
const forEachClosest = require('./association/for-each-closest')
const parseFrames = require('./parsing/parse-frames')
const parsePoses = require('./parsing/parse-poses')

function readDataset (path) {
  return Promise.all([
    readFile(join(path, 'rgb.txt')),
    readFile(join(path, 'depth.txt')),
    readFile(join(path, 'dataset.json')),
    readFile(join(path, 'groundtruth.txt')).catch(error => null)
  ]).then(([rgbFramesBuffer, depthFramesBuffer,
            datasetJSONBuffer, groundTruthBuffer]) => {
    const json = JSON.parse(datasetJSONBuffer.toString())

    const cameraMatrix = mat3.fromValues(...json.cameraMatrix)
    mat3.transpose(cameraMatrix, cameraMatrix);

    const dataset = {
      path,
      cameraMatrix,
      inverseCameraMatrix: mat3.invert(mat3.create(), cameraMatrix),
      depthScaleFactor: json.depthScaleFactor,
      frames: [],
      groundTruthPoses: null
    }
    const rgbFrames = parseFrames(rgbFramesBuffer.toString())
    const depthFrames = parseFrames(depthFramesBuffer.toString())

    for (let [rgb, depth] of associateFrames(rgbFrames, depthFrames)) {
      dataset.frames.push({
        dataset,
        timestamp: rgb.timestamp,
        rgbImagePath: resolve(path, rgb.path),
        depthImagePath: resolve(path, depth.path)
      })
    }

    // Ground truth
    if (groundTruthBuffer) {
      const groundTruthPoses = parsePoses(groundTruthBuffer.toString())
      dataset.groundTruthPoses = []
      forEachClosest(dataset.frames, groundTruthPoses,
                     (_, closestGroundTruthPose) => {
        dataset.groundTruthPoses.push(closestGroundTruthPose)
      })
    }

    return dataset
  })
}

module.exports = readDataset
