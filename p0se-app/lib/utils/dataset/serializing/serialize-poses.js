const { vec3, quat } = require('gl-matrix')

/**
Example output string:
```
1341847980.7900 -0.6832 2.6909 1.7373 0.0003 0.8617 -0.5072 -0.0145
1341847980.8000 -0.6829 2.6910 1.7373 0.0002 0.8613 -0.5078 -0.0146
1341847980.8100 -0.6824 2.6913 1.7372 0.0004 0.8610 -0.5084 -0.0148
...
```
```
*/

function serializePoses (poses) {
  let txt = ''

  for (let pose of poses) {
    txt += pose.timestamp + ' '
    txt += pose.translation[0] + ' '
    txt += pose.translation[1] + ' '
    txt += pose.translation[2] + ' '
    txt += pose.rotation[0] + ' '
    txt += pose.rotation[1] + ' '
    txt += pose.rotation[2] + ' '
    txt += pose.rotation[3] + '\n'
  }

  return txt
}

module.exports = serializePoses
