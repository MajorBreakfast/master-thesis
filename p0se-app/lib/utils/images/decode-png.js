const addon = require('bindings')('addon')

function decodePNG (pngBuffer) {
  return new Promise((resolve, reject) => {
    addon.decodePNG(pngBuffer, (error, png) => {
      if (error) {
        reject(error)
      } else {
        resolve(png)
      }
    })
  })
}

module.exports = decodePNG
