const { readFile } = require('fs-promise')
const decodePNG = require('./decode-png')
const PngImg = require('png-img')

async function readPNG (path) {
  const pngBuffer = await readFile(path)
  return decodePNG(pngBuffer)
}

module.exports = readPNG
