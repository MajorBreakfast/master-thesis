const { quat, vec3 } = require('gl-matrix')
const Container = require('./container')
const DeviceImage = require('./device-image')
const addon = require('bindings')('addon')

class Aligner extends addon.NativeAligner {
  constructor ({ container, width, height, numLevels, mode = 'MODE_PHOTOMETRIC' }) {
    if (!(container instanceof Container)) {
      throw new Error('Invalid options.container')
    }

    if (typeof width !== 'number') {
      throw new Error('Invalid options.width')
    }

    if (typeof height !== 'number') {
      throw new Error('Invalid options.height')
    }

    if (typeof numLevels !== 'number') {
      throw new Error('Invalid options.numLevels')
    }

    super(container, width, height, numLevels, mode)

    this._width = width
    this._height = height
    this._numLevels = numLevels

    const readyPromise = new Promise((resolve, reject) => {
      this.resolveReadyPromise = resolve
      this.rejectReadyPromise = reject
    })

    this._runPromise = readyPromise
    this.ready = Promise.resolve(readyPromise)
    this.isReady = false
  }

  get buildLog () {
    if (!this.isReady) {
      throw new Error('Aligner is not ready')
    }
    return this._getBuildLog()
  }

  _constructedCallback (error) {
    if (error) {
      this.rejectReadyPromise(error)
    } else {
      this.isReady = true
      this.resolveReadyPromise()
    }
  }

  run ({ preDepthDeviceImage,
         preRGBDeviceImage,
         curDepthDeviceImage,
         curRGBDeviceImage,
         cameraMatrix,
         depthScaleFactor,
         numMaxIterationsPerLevel = 20,
         allowEarlyBreak = true,
         initialMotion = { rotation: quat.create(),
                           translation: vec3.create() },
         log = false } = {}) {
    if (!this.isReady) {
      throw new Error('Aligner is not ready')
    }
    return this._runPromise.catch(() => {}).then(() => {
      return new Promise((resolve, reject) => {
        assertDeviceImage(preDepthDeviceImage, 'preDepthDeviceImage',
                          this._width, this._height)
        assertDeviceImage(preRGBDeviceImage, 'preRGBDeviceImage',
                          this._width, this._height)
        assertDeviceImage(curDepthDeviceImage, 'curDepthDeviceImage',
                          this._width, this._height)
        assertDeviceImage(curRGBDeviceImage, 'curRGBDeviceImage',
                          this._width, this._height)

        if (!(cameraMatrix instanceof Float32Array) ||
            cameraMatrix.length !== 9) {
          throw new Error(
            'Expected cameraMatrix to be a Float32Array with 9 elements')
        }

        if (typeof depthScaleFactor !== 'number') {
          throw new Error('Expected depthScaleFactor to be a Number')
        }

        if (!(initialMotion.rotation instanceof Float32Array) ||
            initialMotion.rotation.length !== 4) {
          throw new Error('Expected initialMotion.rotation to be a ' +
                          'Float32Array with 4 elements')
        }

        if (!(initialMotion.translation instanceof Float32Array) ||
            initialMotion.translation.length !== 3) {
          throw new Error('Expected initialMotion.translation to be a ' +
                          'Float32Array with 3 elements')
        }

        super.run(
          this._width,
          this._height,
          preDepthDeviceImage.deviceBuffer,
          preRGBDeviceImage.deviceBuffer,
          curDepthDeviceImage.deviceBuffer,
          curRGBDeviceImage.deviceBuffer,
          cameraMatrix,
          depthScaleFactor,
          numMaxIterationsPerLevel,
          allowEarlyBreak,
          initialMotion.rotation,
          initialMotion.translation,
          log,
          (error, result) => {
            if (error) {
              reject(error)
            } else {
              result.motion.translation =
                new Float32Array(result.motion.translation, 0, 3)
              result.motion.rotation =
                new Float32Array(result.motion.rotation, 0, 4)
              resolve(result)
            }
          }
        )
      })
    })
  }
}

function assertDeviceImage (deviceImage, label, width, height) {
  if (!(deviceImage instanceof DeviceImage)) {
    throw new Error(label + ' must be a DeviceImage')
  }

  if (deviceImage.width !== width) {
    throw new Error(label + ' has unexpected width')
  }

  if (deviceImage.height !== height) {
    throw new Error(label + ' has unexpected height')
  }
}

module.exports = Aligner
