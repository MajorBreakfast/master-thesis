const addon = require('bindings')('addon')

class Container extends addon.NativeContainer {}

module.exports = Container
