const Container = require('./container')
const addon = require('bindings')('addon')

class DeviceBuffer extends addon.NativeDeviceBuffer {
  constructor (container, length) {
    if (!(container instanceof Container)) {
      throw new Error('Expected first argument to be a p0se Container')
    }

    if (typeof length !== 'number') {
      throw new Error('Expected second argument to be a Number')
    }

    super(container, length)
  }

  get length () { return this._getLength() }

  readSync (buffer) {
    if (!(buffer instanceof Buffer)) {
      throw new Error('Expected buffer as argument')
    }

    if (buffer.length !== this.length) {
      throw new Error('Buffer lengths do not match')
    }

    super.readSync(buffer)
  }

  writeSync (buffer) {
    if (!(buffer instanceof Buffer)) {
      throw new Error('Expected buffer as argument')
    }

    if (buffer.length !== this.length) {
      throw new Error('Buffer lengths do not match')
    }

    super.writeSync(buffer)
  }
}

module.exports = DeviceBuffer
