const DeviceBuffer = require('./device-buffer')

class DeviceImage {
  constructor (container, width, height, dataType) {
    this._width = width
    this._height = height
    this._dataType = dataType
    this._deviceBuffer = new DeviceBuffer(container, this.numBytes)
  }

  get width () { return this._width }
  get height () { return this._height }
  get dataType () { return this._dataType }
  get deviceBuffer () { return this._deviceBuffer }

  get numBytes () {
    switch (this._dataType) {
      case 'uchar3':
        return this._width * this._height * 3
      case 'ushort':
        return this._width * this._height * 2
    }
  }

  readSync (image) {
    if (arguments.length === 0) {
      image = {
        width: this.width,
        height: this.height,
        dataType: this.dataType,
        buffer: Buffer.alloc(this.numBytes)
      }
    }

    if (image.width !== this.width) {
      throw new Error('Image widths do not match')
    }

    if (image.height !== this.height) {
      throw new Error('Image heights do not match')
    }

    if (image.dataType !== this.dataType) {
      throw new Error('Image data types do not match')
    }

    this._deviceBuffer.readSync(image.buffer)

    if (arguments.length === 0) {
      return image
    } else {
      return this
    }
  }

  writeSync (image) {
    if (image.width !== this.width) {
      throw new Error('Image widths do not match')
    }

    if (image.height !== this.height) {
      throw new Error('Image heights do not match')
    }

    if (image.dataType !== this.dataType) {
      throw new Error('Image data types do not match')
    }

    this._deviceBuffer.writeSync(image.buffer)

    return this
  }
}

module.exports = DeviceImage
