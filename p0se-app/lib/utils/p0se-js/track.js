const { quat, vec3, mat4 } = require('gl-matrix')
const Container = require('./container')
const DeviceImage = require('./device-image')
const Aligner = require('./aligner')
const readPNG = require('../images/read-png')

/**
- options.platformIndex {Number} OpenCL platform (Default: 0)
- options.deviceIndex {Number} OpenCL device (Default: 0)
- options.width {Number}
- options.height {Number}
- options.cameraMatrix {Float32Array} Float32Array with length 9
- options.depthScaleFactor {Number} Float that is multiplied to the depth values
    to get a meaningful distance in meters
- options.numLevels {Number} (Default: 4) The number of image pyramid levels
    used by the optimization for the alignment
- options.numMaxIterationsPerLevel {Number} (Default: 20) The number of
    iterations for each pyramid level in the optimization
- options.allowEarlyBreak {Boolean} (Default: true) After some iterations the
    optimization usually reaches convergence and further iterations no longer
    improve the result. Early breaking can thus increase performance.
- options.frames {Array} Array with { timestamp, rgbImagePath, depthImagePath }
    objects
*/
async function track ({
  platformIndex = 0,
  deviceIndex = 0,
  width,
  height,
  mode,
  cameraMatrix,
  depthScaleFactor,
  numLevels = 4,
  numMaxIterationsPerLevel = 20,
  allowEarlyBreak = true,
  usePoseFromPreviousFrameAsFirstEstimate = false,
  initialPose =  { rotation: quat.create(), translation: vec3.create() },
  frames = [],
  callback = () => {}
} = {}) {
  const container = new Container({ platformIndex, deviceIndex })
  console.log(Container.getPlatforms())
  let preRGBDeviceImage =
    new DeviceImage(container, width, height, 'uchar3')
  let preDepthDeviceImage =
    new DeviceImage(container, width, height, 'ushort')
  let curRGBDeviceImage =
    new DeviceImage(container, width, height, 'uchar3')
  let curDepthDeviceImage =
    new DeviceImage(container, width, height, 'ushort')

  const aligner = new Aligner({ container, width, height, numLevels, mode })
  await aligner.ready
  //console.log(aligner.buildLog)

  let initialMotion = { rotation: quat.create(), translation: vec3.create() }
  const motionMat = mat4.create()
  const currentPoseMat = mat4.create()
  const postionsArrayBuffer = new ArrayBuffer(4 * 3 * frames.length)
  mat4.fromRotationTranslation(currentPoseMat, initialPose.rotation,
                                               initialPose.translation)
  const poses = [{
    rotation: initialPose.rotation,
    translation: initialPose.translation,
    timestamp: frames[0].timestamp
  }]
  const motions = []

  let positionsFloat32Array =
    new Float32Array(postionsArrayBuffer, 0, 3)
  positionsFloat32Array[0] = initialPose.translation[0]
  positionsFloat32Array[1] = initialPose.translation[1]
  positionsFloat32Array[2] = initialPose.translation[2]

  const statsArray = []

  for (let curIndex = 0; curIndex < frames.length; ++curIndex) {
    const preIndex = curIndex - 1
    const preFrame = frames[preIndex] // Undefined in first iteration
    const curFrame = frames[curIndex]

    // Swap device images
    let tmp
    tmp = preRGBDeviceImage
    preRGBDeviceImage = curRGBDeviceImage // Reuse buffer and content
    curRGBDeviceImage = tmp // Reuse buffer
    tmp = preDepthDeviceImage
    preDepthDeviceImage = curDepthDeviceImage // Reuse buffer and content
    curDepthDeviceImage = tmp // Reuse buffer

    // Load cur image
    const [curDepthImage, curRGBImage] = await Promise.all([
      readPNG(curFrame.depthImagePath),
      readPNG(curFrame.rgbImagePath)
    ])

    curRGBDeviceImage.writeSync(curRGBImage)
    curDepthDeviceImage.writeSync(curDepthImage)

    if (curIndex > 0) {
      // Perform alignment
      const result = await aligner.run({
        preRGBDeviceImage,
        preDepthDeviceImage,
        curRGBDeviceImage,
        curDepthDeviceImage,
        cameraMatrix,
        depthScaleFactor,
        numMaxIterationsPerLevel,
        allowEarlyBreak,
        initialMotion,
        callback
      })
      const { motion, stats } = result

      if (usePoseFromPreviousFrameAsFirstEstimate) {
        initialMotion = motion
      }

      mat4.fromRotationTranslation(motionMat, motion.rotation,
                                              motion.translation)
      mat4.multiply(currentPoseMat, currentPoseMat, motionMat)

      const pose = {
        rotation: quat.create(),
        translation: vec3.create(),
        timestamp: curFrame.timestamp
      }
      mat4.getTranslation(pose.translation, currentPoseMat)
      mat4.getRotation(pose.rotation, currentPoseMat)
      poses.push(pose)
      motions.push(motion)

      positionsFloat32Array =
        new Float32Array(postionsArrayBuffer, 0, 3 * (curIndex + 1))
      positionsFloat32Array[curIndex * 3]     = pose.translation[0]
      positionsFloat32Array[curIndex * 3 + 1] = pose.translation[1]
      positionsFloat32Array[curIndex * 3 + 2] = pose.translation[2]

      statsArray.push(stats)

      callback({
        poses, motions, stats, positionsFloat32Array, curIndex, preIndex
      })
    }
  }

  const stats = {
    totalDuration: 0,
    levelStats: []
  }
  for (let l = 0; l < numLevels; ++l) {
    stats.levelStats.push({ totalNumIterations: 0, totalDuration: 0 })
  }

  for (let s of statsArray) {
    stats.totalDuration += s.duration
    for (let l = 0; l < numLevels; ++l) {
      stats.levelStats[l].totalDuration += s.levelStats[l].duration
      stats.levelStats[l].totalNumIterations += s.levelStats[l].numIterations
    }
  }
  stats.averageDuration = stats.totalDuration / statsArray.length
  for (let l = 0; l < numLevels; ++l) {
    stats.levelStats[l].averageDuration = stats.levelStats[l].totalDuration / statsArray.length
    stats.levelStats[l].averageNumIterations = stats.levelStats[l].totalNumIterations / statsArray.length
  }

  return { poses, motions, stats }
}

module.exports = track
