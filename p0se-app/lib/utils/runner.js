const fs = require('fs-promise')
const { join } = require('path')
const readDataset = require('./dataset/read-dataset')
const serializePoses = require('./dataset/serializing/serialize-poses')
const track = require('./p0se-js/track')
const childProcess = require('child_process')

function exec (command) {
  return new Promise((resolve, reject) => {
    childProcess.exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error)
      } else {
        resolve({ stdout, stderr })
      }
    })
  })
}

async function listBenchmarks () {
  let folderPath = join(process.cwd(), '..', 'benchmarks')

  let entries = await fs.readdir(folderPath)
  entries = entries.filter(fileName => /\.json$/.test(fileName))

  const benchmarks = []
  await Promise.all(entries.map(async (fileName) => {
    const filePath = join(folderPath, fileName)
    const buffer = await fs.readFile(filePath)
    const json = JSON.parse(buffer.toString())
    json.fileName = fileName
    if (!json.datasetPath) { throw new Error('datasetPath was not specified ')}
    json.datasetPath = join(folderPath, json.datasetPath)
    json.outputFolder = join(folderPath, 'output', fileName.replace(/\.json$/, ''))

    if (json.modes) {
      for (let mode of json.modes) {
        const json2 = Object.assign({}, json)
        delete json2.modes
        json2.mode = mode
        json2.outputFolder = json2.outputFolder + '-' + mode
        benchmarks.push(json2)
      }
    } else {
      benchmarks.push(json)
    }
  }))
  return benchmarks
}

async function runAllBenchmarks (callbacks) {
  callbacks.onLoadedDataset = callbacks.onLoadedDataset || function () {}
  callbacks.onTrackedFrame = callbacks.onTrackedFrame || function () {}

  const benchmarks = await listBenchmarks()
  for (let benchmark of benchmarks) {
    await runBenchmark (benchmark, callbacks)
  }
}

async function runBenchmark (benchmark, callbacks) {
  const dataset = await readDataset(benchmark.datasetPath)

  callbacks.onLoadedDataset(dataset)

  const from = benchmark.from || 0
  const to = benchmark.to || dataset.frames.length

  const options = Object.assign({
    cameraMatrix: dataset.cameraMatrix,
    depthScaleFactor: dataset.depthScaleFactor,
    initialPose: dataset.groundTruthPoses[from],
    frames: dataset.frames.slice(from, to),
    callback: callbacks.onTrackedFrame
  }, benchmark)

  const result = await track(options)

  await fs.ensureDir(benchmark.outputFolder)

  const txt = serializePoses(result.poses)
  await fs.writeFile(join(benchmark.outputFolder, 'trajectory.txt'), txt)

  const stats = {}
  /*let ateCommand = [
    'python ../tools/tum-benchmark/evaluate_ate.py',
    join(benchmark.datasetPath, 'groundtruth.txt'),
    join(benchmark.outputFolder, 'trajectory.txt'),
    '--plot', join(benchmark.outputFolder, 'plot-ate.png'),
    '--verbose'
  ].join(' ')
  ate = await exec(ateCommand)
  Object.assign(stats, parseToObj(ate.stdout))*/

  let rpeCommand = [
    'python ../tools/tum-benchmark/evaluate_rpe.py',
    join(benchmark.datasetPath, 'groundtruth.txt'),
    join(benchmark.outputFolder, 'trajectory.txt'),
    '--max_pairs 10000',
    '--fixed_delta  --delta 1 --delta_unit s',
    '--offset 0 --scale 1',
    '--plot', join(benchmark.outputFolder, 'plot.png'),
    '--verbose'
  ].join(' ')
  rpe = await exec(rpeCommand)
  Object.assign(stats, parseToObj(rpe.stdout))

  delete stats.compared_pose_pairs
  Object.assign(stats, result.stats)

  await fs.writeFile(join(benchmark.outputFolder, 'stats.json'),
                     JSON.stringify(stats, null, 2))
}

function parseToObj (s) {
  const lines = s.split('\n')
  const obj = {}
  for (let line of lines) {
    const [path, value] = line.split(' ')
    set(obj, path, parseFloat(value, 10))
  }
  return obj
}

function set (obj, path, value) {
  if (!path) { return }
  const segs = path.split('.')
  let cur = obj
  for (let i = 0; i < segs.length; ++i) {
    const seg = segs[i]
    if (i === segs.length - 1) {
      cur[seg] = value
    } else {
      cur[seg] = cur[seg] || {}
      cur = cur[seg]
    }
  }
}

exports.listBenchmarks = listBenchmarks
exports.runAllBenchmarks = runAllBenchmarks
exports.runBenchmark = runBenchmark
