This directory contains the result of `python associate.py rgb.txt depth.txt > assoc.txt` run in the directory of the TUM RGBD Benchmark's "rgbd_dataset_freiburg3_long_office_household" dataset. `associate.py` is the python 2 script that comes with the TUM RGBD Benchmark. It associates the images listed in `rgb.txt` and `depth.txt` and produces matching pairs of rgb and depth images according to their timestamps. The result produced by this script was stored in `assoc.txt`.

RIP Python 2
