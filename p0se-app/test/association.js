const { resolve, join } = require('path')
const { readFileSync } = require('fs')
const parseFrames = require('../lib/utils/parse-frames')
const associateFrames = require('../lib/utils/associate-frames')

describe('association', function () {
  it('works like the Python 2 script', function () {
    const path = resolve(__dirname, 'association-test-dataset')

    const rgbFramesString = readFileSync(join(path, 'rgb.txt')).toString()
    const depthFramesString = readFileSync(join(path, 'depth.txt')).toString()

    const rgbFrames = parseFrames(rgbFramesString)
    const depthFrames = parseFrames(depthFramesString)

    const frames = associateFrames(rgbFrames, depthFrames)

    const expectedResultLines = readFileSync(join(path, 'assoc.txt'))
                                .toString().trim().split('\n')

    frames.length.should.equal(expectedResultLines.length)
    for (let i = 0; i < frames.length; ++i) {
      const [rgb, depth] = frames[i]
      const line = `${rgb.timestamp.toFixed(6)} ${rgb.path} ` +
                   `${depth.timestamp.toFixed(6)} ${depth.path}`
      line.should.equal(expectedResultLines[i].trim())
    }
  })
})
