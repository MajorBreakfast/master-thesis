const readPNG = require('../lib/utils/read-png')

function now () {
  const hr = process.hrtime()
  return (hr[0] * 1e9 + hr[1]) / 1e6
}

describe('readPNG()', function () {
  it('works', function (done) {
    const start = now()
    const path = '../datasets/rgbd_dataset_freiburg3_long_office_household/depth/1341847980.822989.png'
    readPNG(path, (error, png) => {
      console.log("Took ", now() - start, "ms")
      done(error)
    })
  })
})
