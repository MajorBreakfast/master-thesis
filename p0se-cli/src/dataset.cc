#include "dataset.h"

#include <algorithm>
#include <fstream>
#include <limits>
#include <sstream>
#include <Eigen/Geometry>
#include <iostream>

namespace p0seCLI {

struct ImageRow { double timestamp; std::string path; };
struct GroundTruthRow { double timestamp; Sophus::Vector6f pose; };

std::vector<ImageRow> readImageRows(std::string path);
std::vector<GroundTruthRow> readGroundTruthRows(std::string path);
Eigen::Matrix3f readCameraMatrix(std::string path);

std::vector<Dataset::Frame> createDatasetFrames(
    std::vector<ImageRow> rgb_rows, std::vector<ImageRow> depth_rows,
    std::vector<GroundTruthRow> ground_truth_rows, std::string path_prefix);

Dataset::Dataset(const std::string& path) {
  std::vector<ImageRow> rgb_rows = readImageRows(path + "/rgb.txt");
  std::vector<ImageRow> depth_rows = readImageRows(path + "/depth.txt");
  std::vector<GroundTruthRow> ground_truth_rows =
      readGroundTruthRows(path + "/groundtruth.txt");

  frames_ = createDatasetFrames(rgb_rows, depth_rows, ground_truth_rows, path);
  cameraMatrix_ = readCameraMatrix(path + "/camera-matrix.txt");
}

// Helper functions
std::vector<ImageRow> readImageRows(std::string path) {
  std::ifstream stream; stream.open(path.c_str());
  if (!stream.is_open()) { return {}; }

  std::vector<ImageRow> rows;
  std::string line;
  while (std::getline(stream, line)) {
    if (line.empty() || line.compare(0, 1, "#") == 0) { continue; }

    ImageRow row;
    std::istringstream(line) >> row.timestamp >> row.path;

    rows.push_back(std::move(row));
  }

  return rows;
}

std::vector<GroundTruthRow> readGroundTruthRows(std::string path) {
  std::ifstream stream; stream.open(path.c_str());
  if (!stream.is_open()) { return {}; }

  std::vector<GroundTruthRow> rows;
  std::string line;
  while (std::getline(stream, line)) {
    if (line.empty() || line.compare(0, 1, "#") == 0) { continue; }

    double timestamp;
    Eigen::Vector3f t;
    Eigen::Quaternionf q;

    std::istringstream(line)
      >> timestamp
      >> t.x() >> t.y() >> t.z() >> q.x() >> q.y() >> q.z() >> q.w();

    Eigen::Matrix4f m; m << q.toRotationMatrix(), t,
                            0,       0,        0, 1;

    GroundTruthRow row;
    row.timestamp = timestamp;
    row.pose = Sophus::SE3f(m).log();

    rows.push_back(std::move(row));
  }

  return rows;
}

Eigen::Matrix3f readCameraMatrix(std::string path) {
  std::ifstream stream; stream.open(path.c_str());
  if (!stream.is_open()) { return {}; }

  std::string line;
  std::string content;
  while (std::getline(stream, line)) {
    if (line.empty() || line.compare(0, 1, "#") == 0) { continue; }
    content +=  line + " ";
  }

  Eigen::Matrix3f c;
  std::istringstream(content)
      >> c(0,0) >> c(0,1) >> c(0,2)
      >> c(1,0) >> c(1,1) >> c(1,2)
      >> c(2,0) >> c(2,1) >> c(2,2);

  return c;
}

std::vector<Dataset::Frame> createDatasetFrames(
    std::vector<ImageRow> rgb_rows, std::vector<ImageRow> depth_rows,
    std::vector<GroundTruthRow> ground_truth_rows, std::string path_prefix) {
  std::vector<Dataset::Frame> frames;

  int32_t rgb_id = 0;
  int32_t depth_id = 0;
  int32_t ground_truth_id = 0;
  double t_max_last_pushed = -std::numeric_limits<double>::infinity();

  while (true) {
    // Break if it reached the end of one
    if (rgb_id >= rgb_rows.size()) { break; }
    if (depth_id >= depth_rows.size()) { break; }
    if (ground_truth_id >= ground_truth_rows.size()) { break; }

    double t_rgb = rgb_rows[rgb_id].timestamp;
    double t_depth = depth_rows[depth_id].timestamp;
    double t_ground_truth = ground_truth_rows[ground_truth_id].timestamp;

    double t_min = std::min(t_ground_truth, std::min(t_rgb, t_depth));
    double t_max = std::max(t_ground_truth, std::max(t_rgb, t_depth));

    // Remove last (and add better match in next step)
    if (t_max == t_max_last_pushed) { frames.pop_back(); }

    // Create a frame if all three entries are new
    if (t_max_last_pushed < t_min || t_max == t_max_last_pushed) {
      t_max_last_pushed = t_max;
      frames.push_back({
        t_min,
        path_prefix + "/" + rgb_rows[rgb_id].path,
        path_prefix + "/" + depth_rows[depth_id].path,
        ground_truth_rows[ground_truth_id].pose
      });
    }

    // Advance the one with the smallest timestamp
    if (t_rgb == t_min) { rgb_id++; }
    if (t_depth == t_min) { depth_id++; }
    if (t_ground_truth == t_min) { ground_truth_id++; }
  }

  return frames;
}

} // namespace p0seCLI
