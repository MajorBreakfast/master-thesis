#pragma once

#include <vector>
#include <string>
#include <Eigen/Core>
#include <sophus/se3.hpp>

namespace p0seCLI {

class Dataset {
 public:
  struct Frame {
    double timestamp;
    std::string rgbImagePath;
    std::string depthImagePath;
    Sophus::Vector6f groundTruthPose;
  };

  Dataset(const std::string& path);

  inline const std::vector<Frame>& frames() const { return frames_; }
  const Eigen::Matrix3f& cameraMatrix() const { return cameraMatrix_; }

 private:
  std::vector<Frame> frames_;
  Eigen::Matrix3f cameraMatrix_;
};

} // namespace p0seCLI
