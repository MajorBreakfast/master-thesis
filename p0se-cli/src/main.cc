#include <cinttypes>
#include <fstream>
#include <iostream>
#include <string>

#include <json/json.hpp>
#include <p0se/p0se.h>

#include "dataset.h"
#include "read-image.h"
#include "show-image.h"
#include "serialize-timer-group.h"
#include "save-poses.h"

namespace p0seCLI {

using json = nlohmann::json;

struct CLIParameters {
  std::string configFilePath;
  std::string outputFilePath;
  std::string timingsFilePath;
};

bool extractCLIParameters(int32_t argc,
                          char* argv[],
                          CLIParameters* cliParameters) {
  if (argc < 2) { return false; }

  cliParameters->configFilePath = argv[1];
  if (argc >= 3) { cliParameters->outputFilePath = argv[2]; }
  if (argc >= 4) { cliParameters->timingsFilePath = argv[3]; }

  return true;
}

bool readConfigFile(std::string configFilePath,
                    json* configJSON) {
  std::ifstream stream; stream.open(configFilePath);
  if (!stream.is_open()) { return false; }
  stream >> *configJSON;
  return true;
}

std::string dirname(std::string& path) {
  return path.substr(0, path.find_last_of("/\\"));
}

int main(int32_t argc, char* argv[]) {
  p0se::showImage = showImage;
  p0se::wait = wait;

  // Extract CLI Parameters
  CLIParameters cliParameters;
  if (!extractCLIParameters(argc, argv, &cliParameters)) {
    std::cout << "Incorrect parameters. Usage: "
      << "p0se <config-file> <output-file> <timings-file>"
      << std::endl;
    return EXIT_FAILURE;
  }

  // Read config file
  json configJSON;
  if (!readConfigFile(cliParameters.configFilePath, &configJSON)) {
    std::cout << "Couldn't read config file" << std::endl;
  };
  int32_t platformIndex = configJSON["clPlatformIndex"];
  int32_t deviceIndex = configJSON["clDeviceIndex"];

  // Get path to folder the config file resides in (used for relative paths)
  std::string path = dirname(cliParameters.configFilePath);

  // OpenCL setup
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);

  if (platformIndex >= platforms.size()) {
    std::cout << "Platform with index " << platformIndex
              << " does not exist" << std::endl;
    return EXIT_FAILURE;
  }

  cl::Platform platform = platforms[platformIndex];

  std::vector<cl::Device> devices;
  platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

  if (deviceIndex >= devices.size()) {
    std::cout << "Device with index " << deviceIndex
              << " does not exist" << std::endl;
    return EXIT_FAILURE;
  }

  cl::Device device = devices[deviceIndex];

  cl::Context context = cl::Context(device);

  cl::CommandQueue commandQueue = cl::CommandQueue(context);

  std::string platformName = platform.getInfo<CL_PLATFORM_NAME>();
  std::string deviceName = device.getInfo<CL_DEVICE_NAME>();
  std::cout << "Selected platform \"" << platformName << "\"\n";
  std::cout << "Selected device \"" << deviceName << "\"\n";

  // Run each scenario
  int32_t scenarioCount = static_cast<int32_t>(configJSON["scenarios"].size());
  for (int32_t i = 0; i < scenarioCount; ++i) {
    auto scenarioJSON = configJSON["scenarios"][i];

    // Skipping
    bool skip = scenarioJSON["skip"].is_boolean() ?
                scenarioJSON["skip"] : false;
    if (skip) { continue; }

    // Load dataset
    std::string datesetPath = scenarioJSON["datasetPath"];
    Dataset dataset(path + "/" + datesetPath);
    if (dataset.frames().size() == 0) {
      std::cout << "Skipped scenario " << i
                << " because it contains no frames.";
      continue;
    }

    // From, to
    int32_t from = scenarioJSON["from"].is_number_integer() ?
                   scenarioJSON["from"] : 0;
    int32_t to = scenarioJSON["to"].is_number_integer() ?
                 scenarioJSON["to"] : INT32_MAX;

    from = std::max(from, 0);
    to = std::min(to, static_cast<int32_t>(dataset.frames().size() - 1));

    // Infer dimensions from frame 0
    auto firstImage = readTUMRGBImage(dataset.frames()[0].rgbImagePath);
    uint32_t width = firstImage.width();
    uint32_t height = firstImage.height();

    // Extract various values from JSON
    uint32_t levelCount = scenarioJSON["levelCount"].is_number_integer() ?
                          scenarioJSON["levelCount"] : 4;
    uint32_t levelMaxIterationCount = scenarioJSON["levelMaxIterationCount"].is_number_integer() ?
                                      scenarioJSON["levelMaxIterationCount"] : 20;
    bool allowEarlyBreak = scenarioJSON["allowEarlyBreak"].is_boolean() ?
                           scenarioJSON["allowEarlyBreak"] : false;
    bool showInputImages = scenarioJSON["showInputImages"].is_boolean() ?
                           scenarioJSON["showInputImages"] : false;
    bool showResidualImagesForEachStep = scenarioJSON["showResidualImagesForEachStep"].is_boolean() ?
                           scenarioJSON["showResidualImagesForEachStep"] : false;
    std::string outputPath = scenarioJSON["outputPath"];
    float depthGain = scenarioJSON["depthGain"].is_number() ?
                      scenarioJSON["depthGain"] : 0.0f;
    float rgbGain = scenarioJSON["rgbGain"].is_number() ?
                    scenarioJSON["rgbGain"] : 1.0f;
    int32_t stride = scenarioJSON["stride"].is_number_integer() ?
                     scenarioJSON["stride"] : 1;
    bool showNormalMap = scenarioJSON["showNormalMap"].is_boolean() ?
                         scenarioJSON["showNormalMap"] : false;

    // Create depth image buffers
    auto preDepthDeviceImages = p0se::createDeviceImagePyramid(
        context, width, height, p0se::FLOAT, p0se::DEPTH, levelCount);
    auto curDepthDeviceImages = p0se::createDeviceImagePyramid(
        context, width, height, p0se::FLOAT, p0se::DEPTH, levelCount);

    // Create RGB image buffers
    auto preRGBDeviceImages = p0se::createDeviceImagePyramid(
        context, width, height, p0se::UCHAR3, p0se::RGB, levelCount);
    auto curRGBDeviceImages = p0se::createDeviceImagePyramid(
        context, width, height, p0se::UCHAR3, p0se::RGB, levelCount);

    // Create aligner
    p0se::Aligner aligner(context, width, height, levelCount, true);
    p0se::PyramidDownsampler pyramidDownsampler(context);

    // Output
    std::vector<Eigen::Matrix4f> poses;
    std::vector<double> timestamps;

    // State
    Sophus::Vector6f xi = dataset.frames()[from].groundTruthPose;
    Sophus::Vector6f preDeltaXi = Sophus::Vector6f::Zero();

    // (To be removed) Normal map calculator
    p0se::BilateralDepthBlur bilateralDepthBlur(context);
    auto curBlurredDepthDeviceImages = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::DEPTH, levelCount);

    p0se::NormalMapCalculator normalMapCalculator(context);
    auto normalMapDeviceImages = p0se::createDeviceImagePyramid(
      context, width, height, p0se::UCHAR3, p0se::NORMAL, levelCount);

    std::cout << "Running scenario " << i << std::endl;

    // Iterate over dataset frames
    for (int i = from; i <= to; i += stride) {
      auto& curFrame = dataset.frames()[i];

      // Load images from disk
      p0se::HostImage curRGBImage = readTUMRGBImage(curFrame.rgbImagePath);
      p0se::HostImage curDepthImage = readTUMDepthImage(curFrame.depthImagePath);

      // Fill image pyramid
      curRGBDeviceImages[0].enqueueWrite(commandQueue, curRGBImage);
      curDepthDeviceImages[0].enqueueWrite(commandQueue, curDepthImage);
      pyramidDownsampler.enqueue(commandQueue, &curRGBDeviceImages);
      pyramidDownsampler.enqueue(commandQueue, &curDepthDeviceImages);
      commandQueue.finish();

      if (showNormalMap) {
        bilateralDepthBlur.enqueue(commandQueue, curDepthDeviceImages[0], &curBlurredDepthDeviceImages[0]);

        showImage(curDepthDeviceImages[0].read(commandQueue), "Depth Current Frame", 10, 10);
        showImage(curBlurredDepthDeviceImages[0].read(commandQueue), "Blurred Depth Current Frame", 750, 10);

        normalMapCalculator.enqueue(commandQueue, curDepthDeviceImages[0], dataset.cameraMatrix(), &normalMapDeviceImages[0]);
        showImage(normalMapDeviceImages[0].read(commandQueue), "Normal Map of Depth Current Frame", 10, 500);

        normalMapCalculator.enqueue(commandQueue, curBlurredDepthDeviceImages[0], dataset.cameraMatrix(), &normalMapDeviceImages[0]);
        showImage(normalMapDeviceImages[0].read(commandQueue), "Normal Map of Blurred Depth Current Frame", 750, 500);


        wait(0);

        /*
        if (true) {
          showImage(curBlurredDepthDeviceImages[0].read(commandQueue), "Blurred Depth Current Frame", 750, 10);
          wait(0);
        }

        normalMapCalculator.enqueue(commandQueue, curBlurredDepthDeviceImages[0], dataset.cameraMatrix(), &normalMapDeviceImages[0]);
        //showImage(normalMapDeviceImages[0].read(commandQueue), "Normal Map Current Frame", 750, 10);
        wait(0);*/
      }

      // Estimate pose between previous und current frame
      if (i > from) {
        if (showInputImages) { // Show depth images
          showImage(preDepthDeviceImages[0].read(commandQueue), "Depth Previous Frame", 100, 10);
          showImage(curDepthDeviceImages[0].read(commandQueue), "Depth Current Frame", 750, 10);
          wait(0);
        }

        std::cout << "Compute pose from frame "
                  << i - stride << " to " << i << std::endl;

        p0se::TimerGroup timerGroup("alignment");

        commandQueue.finish();
        p0se::Timer timer = timerGroup.createAndStartTimer("frame");

        Sophus::Vector6f deltaXi;
        aligner.run(commandQueue,
                    preDepthDeviceImages,
                    preRGBDeviceImages,
                    curDepthDeviceImages,
                    curRGBDeviceImages,
                    dataset.cameraMatrix(),
                    preDeltaXi,
                    levelMaxIterationCount,
                    depthGain,
                    rgbGain,
                    allowEarlyBreak,
                    showResidualImagesForEachStep,
                    &deltaXi,
                    &timerGroup);

        commandQueue.finish();
        timer.end();
        std::chrono::duration<double, std::milli> duration = timer.getEnd() - timer.getStart();
        std::cout << "Frame time " << duration.count() << "ms" << std::endl;

        // Modify state
        xi = Sophus::SE3f::log(Sophus::SE3f::exp(xi) * Sophus::SE3f::exp(deltaXi).inverse());
        preDeltaXi = deltaXi;

        // Store frame output
        poses.push_back(Sophus::SE3f::exp(xi).matrix());
        timestamps.push_back(curFrame.timestamp);

        //std::cout << serializeTimerGroup(timerGroup).dump(2) << std::endl;
      } // End of pose estimation

      // Swap buffers
      // This iteration's current frame becomes next's previous frame
      std::swap(preRGBDeviceImages, curRGBDeviceImages);
      std::swap(preDepthDeviceImages, curDepthDeviceImages);
    } // End of frame loop

    // Store scenario output
    savePoses(path + "/" + outputPath + "/trajectory.txt", poses, timestamps);

  } // End of scenario loop

  std::cout << "Done" << std::endl;

  return EXIT_SUCCESS;
}

} // End of namespace p0seCLI

int main(int32_t argc, char* argv[]) {
  return p0seCLI::main(argc, argv);
}
