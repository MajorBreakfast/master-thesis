#include "read-image.h"
#include <cstring>
#include <cinttypes>
#include <opencv2/highgui/highgui.hpp>

namespace p0seCLI {

p0se::HostImage readTUMRGBImage(const std::string& path) {
  cv::Mat mat_in = cv::imread(path, CV_LOAD_IMAGE_ANYDEPTH |
                                    CV_LOAD_IMAGE_ANYCOLOR);

  if (mat_in.type() != CV_8UC3) {
    throw std::runtime_error("Image is not of type unsigned char3");
  }

  p0se::HostImage image(mat_in.cols, mat_in.rows, p0se::UCHAR3, p0se::RGB);

  int32_t pixelCount = image.pixelCount();
  const uchar* dataIn = static_cast<const uchar*>(mat_in.data);
  uchar* dataOut = static_cast<uchar*>(image.buffer());

  for (int32_t p = 0; p < pixelCount; p++) {
    dataOut[p * 3 + 2] = dataIn[p * 3];
    dataOut[p * 3 + 1] = dataIn[p * 3 + 1];
    dataOut[p * 3] = dataIn[p * 3 + 2];
  }

  return image;
}

p0se::HostImage readTUMDepthImage(const std::string& path) {
  cv::Mat mat_in = cv::imread(path, CV_LOAD_IMAGE_ANYDEPTH |
                                    CV_LOAD_IMAGE_ANYCOLOR);

  if (mat_in.type() != CV_16UC1) {
    throw std::runtime_error("Image is not of type unsigned short");
  }

  p0se::HostImage image(mat_in.cols, mat_in.rows, p0se::FLOAT, p0se::DEPTH);

  float multiplier = 1.0f / 5000;
  int32_t count = image.subpixelCount();
  float* dataOut = reinterpret_cast<float*>(image.buffer());
  uint16_t* dataIn = reinterpret_cast<uint16_t*>(mat_in.data);

  for (int32_t i = 0; i < count; ++i) {
    dataOut[i] = dataIn[i] * multiplier;
  }

  return image;
}

} // namespace p0seCLI
