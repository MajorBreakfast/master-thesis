#pragma once

#include <string>
#include "p0se/images/host-image.h"

namespace p0seCLI {

p0se::HostImage readTUMRGBImage(const std::string& path);
p0se::HostImage readTUMDepthImage(const std::string& path);

} // namespace p0seCLI
