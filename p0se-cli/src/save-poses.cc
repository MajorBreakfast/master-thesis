#include "save-poses.h"

#include <stdexcept>
#include <fstream>
#include <iomanip>
#include <Eigen/Geometry>

namespace p0seCLI {

void savePoses(const std::string &filename,
               const std::vector<Eigen::Matrix4f> &poses,
               const std::vector<double> &timestamps) {
  if (filename.empty()) { throw std::runtime_error("Empty filename"); }

  // Open file
  std::ofstream outFile;
  outFile.open(filename.c_str());
  if (!outFile.is_open()) { throw std::runtime_error("Can't open file"); }

  // Write data
  outFile << std::fixed << std::setprecision(6);
  for (size_t i = 0; i < poses.size(); i++) {
    double timestamp = timestamps[i];
    Eigen::Vector3f t = poses[i].topRightCorner(3, 1);
    Eigen::Matrix3f R = poses[i].topLeftCorner(3, 3);
    Eigen::Quaternionf q(R);

    outFile << timestamp << " "
            << t.x() << " " << t.y() << " " << t.z() << " "
            << q.x() << " " << q.y() << " " << q.z() << " " << q.w()
            << std::endl;
  }
  
  // Close file
  outFile.close();
}

} // End of namespace p0seCLI
