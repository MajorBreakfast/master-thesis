#pragma once

#include <vector>
#include <Eigen/Dense>

namespace p0seCLI {

void savePoses(const std::string &filename,
               const std::vector<Eigen::Matrix4f> &poses,
               const std::vector<double> &timestamps);

} // End of namespace p0seCLI
