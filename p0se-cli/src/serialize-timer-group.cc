#include "serialize-timer-group.h"

#include <chrono>

namespace p0seCLI {

nlohmann::json serializeTimer(const p0se::Timer& timer) {
  nlohmann::json j;

  j["label"] = timer.getLabel();

  std::chrono::duration<double, std::milli> duration = timer.getEnd() - timer.getStart();
  j["duration"] = duration.count();

  return j;
}

nlohmann::json serializeTimerGroup(const p0se::TimerGroup& timerGroup) {
  nlohmann::json j;

  j["label"] = timerGroup.getLabel();

  j["timers"] = nlohmann::json::array();
  for (auto timer : timerGroup.getTimers()) {
    j["timers"].push_back(serializeTimer(timer));
  }

  j["timerGroups"] = nlohmann::json::array();
  for (auto timerGroup : timerGroup.getTimerGroups()) {
    j["timerGroups"].push_back(serializeTimerGroup(timerGroup));
  }

  return j;
}

} // End of namespace p0seCLI
