#pragma once

#include <p0se/p0se.h>
#include <json/json.hpp>

namespace p0seCLI {

nlohmann::json serializeTimerGroup(const p0se::TimerGroup& timerGroup);

} // End of namespace p0seCLI
