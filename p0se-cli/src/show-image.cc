#include "show-image.h"
#include <opencv2/highgui/highgui.hpp>
#include <cinttypes>
#include <stdexcept>
#include <p0se/p0se.h>
#include <cmath>
#include <algorithm>

namespace p0seCLI {

cv::Mat uchar3HostImagetoCVMat(const p0se::HostImage& image) {
  if (image.dataType() != p0se::UCHAR3) {
    throw std::runtime_error("Expected UCHAR3 as dataType");
  }

  cv::Mat mat = cv::Mat(image.height(), image.width(), CV_8UC3);
  uint8_t* dataOut = static_cast<uint8_t*>(mat.data);

  int32_t pixelCount = image.pixelCount();
  const uchar* dataIn = static_cast<const uchar*>(image.buffer());

  // Flip color channels from RGB to OpenCV's BGR
  for (int32_t p = 0; p < pixelCount; p++) {
    dataOut[p * 3 + 2] = dataIn[p * 3];
    dataOut[p * 3 + 1] = dataIn[p * 3 + 1];
    dataOut[p * 3] = dataIn[p * 3 + 2];
  }

  return mat;
}

struct Color { uint8_t r, g, b; };

cv::Mat floatHostImagetoCVMat(const p0se::HostImage& image,
                              float minValue, // Mapped to black
                              float maxValue, // Mapped to white
                              Color nanColor,
                              bool treatZeroAsNaN) {
  if (image.dataType() != p0se::FLOAT) {
    throw std::runtime_error("Expected FLOAT as dataType");
  }

  cv::Mat mat = cv::Mat(image.height(), image.width(), CV_8UC3);
  uint8_t* dataOut = static_cast<uint8_t*>(mat.data);

  int32_t pixelCount = image.pixelCount();
  const float* dataIn = static_cast<const float*>(image.buffer());

  float factor = 255.0f / (maxValue - minValue);

  int32_t count = image.pixelCount();
  for (int32_t i = 0; i < count; ++i) {
    float value = dataIn[i];
    if (isnan(value) || (treatZeroAsNaN && value == 0.0f)) { // NaN color
      dataOut[i * 3] = nanColor.b;
      dataOut[i * 3 + 1] = nanColor.g;
      dataOut[i * 3 + 2] = nanColor.r;
    } else { // Valid value
      uint8_t intValue = static_cast<uint8_t>(
        std::max(0.0f, std::min(255.0f, (value - minValue) * factor)));
      dataOut[i * 3] = intValue;
      dataOut[i * 3 + 1] = intValue;
      dataOut[i * 3 + 2] = intValue;
    }
  }

  return mat;
}

void showImage(const p0se::HostImage& image,
  const std::string& title,
  int x, int y) {
  cv::Mat mat;

  Color orange = { 0xef, 0xa9, 0x5f };
  Color green = { 0x9f, 0xef, 0x5f };

  switch (image.purpose()) {
  case p0se::NORMAL:
  case p0se::RGB:
    mat = uchar3HostImagetoCVMat(image); break;
  case p0se::DEPTH:
    mat = floatHostImagetoCVMat(image, 7.0f, 0.0f, orange, true); break;
  case p0se::RESIDUAL:
    mat = floatHostImagetoCVMat(image, 0.0f, 1.0f, green, false); break;
  case p0se::JACOBIAN:
    mat = floatHostImagetoCVMat(image, -15.0f, 15.0f, green, false); break;
  }

  cv::imshow(title, mat);
  cv::moveWindow(title, x, y);
}

void wait(int delay) { cv::waitKey(delay); }

} // namespace p0seCLI
