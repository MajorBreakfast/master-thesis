#pragma once

#include "p0se/p0se.h"
#include <string>
#include <cinttypes>

namespace p0seCLI {

void showImage(const p0se::HostImage& image,
               const std::string& title,
               int x, int y);

void wait(int delay);

} // namespace p0seCLI
