#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "p0se-tests/utils/cl.h"

int main(int32_t argc, char* argv[]) {// Platform
  p0se_tests::cl_utils::PrintInfo();
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
