#include <cinttypes>
#include <functional>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <p0se/kernel-functions/compute-scale/compute-scale.h>
#include <p0se/images/host-image.h>
#include "p0se-tests/utils/cl.h"

namespace p0se_tests {

class ComputeScaleTest : public ::testing::Test {
 protected:
  virtual void SetUp();
  void RunTestScenario();
  void ComputeScaleCpu(float* covariance);

  cl::Device device_;
  cl::Context context_;
  cl::CommandQueue command_queue_;

  p0se::ComputeScale compute_scale_;

  p0se::HostImage residuals_host_image_;
  p0se::HostImage weights_host_image_;

  p0se::DeviceImage residuals_device_image_;
  p0se::DeviceImage weights_device_image_;

  float* residuals_;
  float* weights_;
  float mean_;

  int32_t num_residuals_;
  int32_t num_valid_residuals_;
};

TEST_F(ComputeScaleTest, Scenario1) {
  for (int32_t i = 0; i < num_residuals_; ++i) {
    residuals_[i] = (i % 10 == 0) ? NAN : static_cast<float>(1.0f);
    weights_[i] = 1.0f;
  }
  mean_ = 0.0f;
  RunTestScenario();
}

TEST_F(ComputeScaleTest, Scenario2) {
  for (int32_t i = 0; i < num_residuals_; ++i) {
    residuals_[i] = (i % 10 == 0) ? NAN : static_cast<float>(i);
    weights_[i] = 1.0f;
  }
  mean_ = 0.0f;
  RunTestScenario();
}

TEST_F(ComputeScaleTest, Scenario3) {
  for (int32_t i = 0; i < num_residuals_; ++i) {
    residuals_[i] = (i % 10 == 0) ? NAN : 1.0f;
    weights_[i] = static_cast<float>(i);
  }
  mean_ = 0.0f;
  RunTestScenario();
}

TEST_F(ComputeScaleTest, Scenario4) {
  for (int32_t i = 0; i < num_residuals_; ++i) {
    residuals_[i] = static_cast<float>(2 * i);
    weights_[i] = static_cast<float>(i);
  }
  mean_ = 0.0f;
  RunTestScenario();
}

TEST_F(ComputeScaleTest, Scenario5) {
  for (int32_t i = 0; i < num_residuals_; ++i) {
    residuals_[i] = (i % 10 == 0) ? NAN : static_cast<float>(2 * i);
    weights_[i] = static_cast<float>(i);
  }
  mean_ = 20000.0f;
  RunTestScenario();
}

void ComputeScaleTest::SetUp() {
  device_ = cl_utils::GetDevice();
  context_ = cl::Context(device_);
  command_queue_ = cl::CommandQueue(context_, device_);
  compute_scale_ = p0se::ComputeScale(context_);

  residuals_host_image_ = p0se::HostImage(640, 480, p0se::Image::FLOAT);
  residuals_ = reinterpret_cast<float*>(residuals_host_image_.GetBuffer());

  weights_host_image_ = p0se::HostImage(640, 480, p0se::Image::FLOAT);
  weights_ = reinterpret_cast<float*>(weights_host_image_.GetBuffer());

  residuals_device_image_ = p0se::DeviceImage(640, 480, p0se::Image::FLOAT);
  residuals_device_image_.AllocBuffer(context_);

  weights_device_image_ = p0se::DeviceImage (640, 480, p0se::Image::FLOAT);
  weights_device_image_.AllocBuffer(context_);

  num_residuals_ = residuals_host_image_.GetNumPixels();
}

void ComputeScaleTest::ComputeScaleCpu(float* covariance) {
  float acc = 0;
  for (int32_t i = 0; i < num_residuals_; ++i) {
    float residual = residuals_[i];
    float weight = weights_[i];

    if (std::isfinite(residual)) {
      float diff = residual - mean_;
      acc += weight * diff * diff;
    }
  }
  *covariance = acc / num_valid_residuals_;
}

void EXPECT_RELATIVELY_EQUAL(float a, float b, float max_rel_diff) {
  // https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
  float diff = fabs(a - b);
  a = fabs(a);
  b = fabs(b);
  float largest = (b > a) ? b : a;
  EXPECT_TRUE(diff <= largest * max_rel_diff);
}

void ComputeScaleTest::RunTestScenario() {
  residuals_device_image_.WriteBuffer(command_queue_, residuals_);
  weights_device_image_.WriteBuffer(command_queue_, weights_);

  num_valid_residuals_ = 0;
  for (int32_t i = 0; i < num_residuals_; ++i) {
    if (std::isfinite(residuals_[i])) { num_valid_residuals_ += 1; }
  }

  float covariance;
  compute_scale_.Run(command_queue_,
                     residuals_device_image_,
                     weights_device_image_,
                     mean_,
                     num_valid_residuals_,
                     &covariance);

  float expected_covariance;
  ComputeScaleCpu(&expected_covariance);

  // std::cout << "scale gpu: " << covariance << std::endl;
  // std::cout << "scale host: " << expected_covariance << std::endl;
  EXPECT_RELATIVELY_EQUAL(covariance, expected_covariance, 0.0001f);
}

} // End of namespace p0se_tests
