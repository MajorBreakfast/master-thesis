#include "p0se-tests/utils/cl.h"
#include <iostream>
#include <vector>

namespace p0se_tests {
namespace cl_utils {

cl::Device GetDevice() {
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
  if (P0SE_TESTS_CL_PLATFORM_INDEX >= platforms.size()) {
    throw std::runtime_error("Invalid platform index");
  }
  cl::Platform platform = platforms[P0SE_TESTS_CL_PLATFORM_INDEX];

  // Device
  std::vector<cl::Device> devices;
  platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
  if (P0SE_TESTS_CL_DEVICE_INDEX >= devices.size()) {
    throw std::runtime_error("Invalid device index");
  }
  return devices[P0SE_TESTS_CL_DEVICE_INDEX];
}

void PrintInfo() {
  cl::Device device = GetDevice();
  cl::Platform platform(device.getInfo<CL_DEVICE_PLATFORM>());
  std::cout << "OpenCL platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
  std::cout << "OpenCL device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
}

}} // End of namespace p0se_tests::utils::cl
