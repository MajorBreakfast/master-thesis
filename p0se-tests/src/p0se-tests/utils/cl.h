#include <CL/cl2.hpp>

namespace p0se_tests {
namespace cl_utils {

void PrintInfo();
cl::Device GetDevice();

}} // End of namespace p0se_tests::cl_utils
