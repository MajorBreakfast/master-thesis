if(CMAKE_PROJECT_NAME)
  set(this_file ${CMAKE_CURRENT_LIST_FILE})

  # Provide command
  function(BUNDLE_FILES dst prefix)
    set(src "${ARGN}")

    set(src_prefixed "")
    foreach(x ${src})
       list(APPEND src_prefixed "${prefix}/${x}")
    endforeach(x)

    string(REPLACE ";" "$<SEMICOLON>" src_escaped "${src}")

    add_custom_command(
      OUTPUT ${dst}
      COMMAND ${CMAKE_COMMAND}
        -D SRC:STRING="${src_escaped}"
        -D DST:STRING="${dst}"
        -D PREFIX:STRING="${prefix}"
        -P ${this_file}
      DEPENDS ${src_prefixed}
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      COMMENT "Bundling files into ${dst}"
    )
  endfunction(BUNDLE_FILES dst)
else()
  # Bundle
  foreach(file ${SRC})
    file(READ "${PREFIX}/${file}" source)
    string(REPLACE "\\" "\\\\" source "${source}") # Escape backslashes
    string(REPLACE "\"" "\\\"" source "${source}") # Escape strings
    string(REPLACE "\n" "\\n\\\n" source "${source}") # Escape newlines
    set(output "${output}{\n\"${file}\",\n\"${source}\"\n},\n")
  endforeach()

  string(REGEX REPLACE ",\n$" "\n" output "${output}")
  file(WRITE ${DST} "${output}")
endif(CMAKE_PROJECT_NAME)
