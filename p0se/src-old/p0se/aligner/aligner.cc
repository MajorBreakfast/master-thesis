#include "p0se/aligner/aligner.h"

#include <cinttypes>
#include <iostream>
#include <string>

#include "p0se/images/device-image-pyramid.h"
#include "p0se/utils/downsample-camera-matrix.h"

namespace p0se {

Aligner::Aligner(const cl::Context& context,
                 int32_t width,
                 int32_t height,
                 int32_t levelCount,
                 bool isVisualizationEnabled)
  : width_(width),
    height_(height),
    levelCount_(levelCount),
    numericDepthRGBAlignmentStep_(context, width, height, levelCount, 1024, isVisualizationEnabled) {}

void Aligner::run(const cl::CommandQueue& commandQueue,
                  const std::vector<DeviceImage>& preDepthDeviceImages,
                  const std::vector<DeviceImage>& preRGBDeviceImages,
                  const std::vector<DeviceImage>& curDepthDeviceImages,
                  const std::vector<DeviceImage>& curRGBDeviceImages,
                  const Eigen::Matrix3f& cameraMatrix,
                  const Sophus::Vector6f& initialXi,
                  int32_t levelMaxIterationCount,
                  float depthGain,
                  float rgbGain,
                  bool allowEarlyBreak,
                  bool showResidualAndJacobianForEachStep,
                  Sophus::Vector6f* pXi,
                  TimerGroup* pTimerGroup) {
  *pXi = initialXi;
  int32_t numLevels = static_cast<int32_t>(curDepthDeviceImages.size());

  for (int32_t l = numLevels - 1; l >= 0; --l) { // Level loop
    TimerGroup* pLevelTimerGroup = nullptr;
    Timer* pLevelTimer = nullptr;
    if (pTimerGroup) {
      std::string label = std::string("level") + std::to_string(l);
      pLevelTimerGroup = &pTimerGroup->createTimerGroup(label);
      pLevelTimer = &pTimerGroup->createAndStartTimer(label);
    }

    float preError = INFINITY;
    Eigen::Matrix3f levelCameraMatrix = downsampleCameraMatrix(cameraMatrix, l);

    for (int32_t i = 0; i < levelMaxIterationCount; ++i) { // Iteration loop
      Timer* pIterationTimer = nullptr;
      if (pLevelTimerGroup) {
        std::string label = std::string("iteration") + std::to_string(i);
        pIterationTimer = &pLevelTimerGroup->createAndStartTimer(label);
      }

      float curError;
      Sophus::Vector6f updatedXi;
      Histogram histogram(0.0f, 1.0f, 1024);
      numericDepthRGBAlignmentStep_.run(commandQueue,
                                        preDepthDeviceImages[l],
                                        preRGBDeviceImages[l],
                                        curDepthDeviceImages[l],
                                        curRGBDeviceImages[l],
                                        levelCameraMatrix,
                                        *pXi,
                                        depthGain,
                                        rgbGain,
                                        l,
                                        showResidualAndJacobianForEachStep,
                                        &updatedXi,
                                        &histogram,
                                        &curError);

      if (allowEarlyBreak && curError / preError > 0.999) { break; }
      preError = curError;
      *pXi = updatedXi;

      if (pIterationTimer) { pIterationTimer->end(); }
    } // End of iteration loop

    if (pLevelTimer) { pLevelTimer->end(); }
  } // End of level loop
}

} // End of namespace p0se
