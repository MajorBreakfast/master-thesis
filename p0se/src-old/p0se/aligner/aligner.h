#pragma once

#include <cinttypes>
#include <Eigen/Core>
#include <vector>
#include <cinttypes>

#include "p0se/utils.h"
#include "p0se/images/device-image.h"
#include "p0se/timing/timer-group.h"
#include "p0se/median-approximator/median-approximator.h"
#include "p0se/alignment-step/numeric/depth-rgb/numeric-depth-rgb-alignment-step.h"

namespace p0se {

class Aligner {
 public:
  Aligner(const cl::Context& context,
          int32_t width,
          int32_t height,
          int32_t levelCount,
          bool isVisualizationEnabled);

  void Aligner::run(const cl::CommandQueue& commandQueue,
                    const std::vector<DeviceImage>& preDepthDeviceImages,
                    const std::vector<DeviceImage>& preRGBDeviceImages,
                    const std::vector<DeviceImage>& curDepthDeviceImages,
                    const std::vector<DeviceImage>& curRGBDeviceImages,
                    const Eigen::Matrix3f& cameraMatrix,
                    const Sophus::Vector6f& initialXi,
                    int32_t levelMaxIterationCount,
                    float depthGain,
                    float rgbGain,
                    bool allowEarlyBreak,
                    bool showResidualAndJacobianForEachStep,
                    Sophus::Vector6f* pDeltaXi,
                    TimerGroup* pTimerGroup = nullptr);

 private:
  int32_t width_;
  int32_t height_;
  int32_t levelCount_;
  NumericDepthRGBAlignmentStep numericDepthRGBAlignmentStep_;
  std::vector<DeviceImage> residualDeviceImages_;
};

}
