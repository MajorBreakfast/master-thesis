typedef struct {
  // J' * J (symmetric matrix)
  float a00;
  float a10, a11;
  float a20, a21, a22;
  float a30, a31, a32, a33;
  float a40, a41, a42, a43, a44;
  float a50, a51, a52, a53, a54, a55;

  // -J' * r
  float b0;
  float b1;
  float b2;
  float b3;
  float b4;
  float b5;

  float error;
  int validEntryCount;

  float minResidual, maxResidual;
} Accumulator;

void resetAccumulator(local Accumulator* pAcc) {
  pAcc->a00 = 0.0f;
  pAcc->a10 = 0.0f;
  pAcc->a11 = 0.0f;
  pAcc->a20 = 0.0f;
  pAcc->a21 = 0.0f;
  pAcc->a22 = 0.0f;
  pAcc->a30 = 0.0f;
  pAcc->a31 = 0.0f;
  pAcc->a32 = 0.0f;
  pAcc->a33 = 0.0f;
  pAcc->a40 = 0.0f;
  pAcc->a41 = 0.0f;
  pAcc->a42 = 0.0f;
  pAcc->a43 = 0.0f;
  pAcc->a44 = 0.0f;
  pAcc->a50 = 0.0f;
  pAcc->a51 = 0.0f;
  pAcc->a52 = 0.0f;
  pAcc->a53 = 0.0f;
  pAcc->a54 = 0.0f;
  pAcc->a55 = 0.0f;
  pAcc->b0 = 0.0f;
  pAcc->b1 = 0.0f;
  pAcc->b2 = 0.0f;
  pAcc->b3 = 0.0f;
  pAcc->b4 = 0.0f;
  pAcc->b5 = 0.0f;
  pAcc->error = 0.0f;
  pAcc->validEntryCount = 0.0f;
  pAcc->minResidual = INFINITY;
  pAcc->maxResidual = -INFINITY;
}

void addRowToAccumulator(const float residual,
                         const float jacobianRow[],
                         const float weight,
                         local Accumulator* pAcc) {
  pAcc->a00 += jacobianRow[0] * jacobianRow[0] * weight;
  pAcc->a10 += jacobianRow[1] * jacobianRow[0] * weight;
  pAcc->a11 += jacobianRow[1] * jacobianRow[1] * weight;
  pAcc->a20 += jacobianRow[2] * jacobianRow[0] * weight;
  pAcc->a21 += jacobianRow[2] * jacobianRow[1] * weight;
  pAcc->a22 += jacobianRow[2] * jacobianRow[2] * weight;
  pAcc->a30 += jacobianRow[3] * jacobianRow[0] * weight;
  pAcc->a31 += jacobianRow[3] * jacobianRow[1] * weight;
  pAcc->a32 += jacobianRow[3] * jacobianRow[2] * weight;
  pAcc->a33 += jacobianRow[3] * jacobianRow[3] * weight;
  pAcc->a40 += jacobianRow[4] * jacobianRow[0] * weight;
  pAcc->a41 += jacobianRow[4] * jacobianRow[1] * weight;
  pAcc->a42 += jacobianRow[4] * jacobianRow[2] * weight;
  pAcc->a43 += jacobianRow[4] * jacobianRow[3] * weight;
  pAcc->a44 += jacobianRow[4] * jacobianRow[4] * weight;
  pAcc->a50 += jacobianRow[5] * jacobianRow[0] * weight;
  pAcc->a51 += jacobianRow[5] * jacobianRow[1] * weight;
  pAcc->a52 += jacobianRow[5] * jacobianRow[2] * weight;
  pAcc->a53 += jacobianRow[5] * jacobianRow[3] * weight;
  pAcc->a54 += jacobianRow[5] * jacobianRow[4] * weight;
  pAcc->a55 += jacobianRow[5] * jacobianRow[5] * weight;
  pAcc->b0 -= jacobianRow[0] * residual * weight;
  pAcc->b1 -= jacobianRow[1] * residual * weight;
  pAcc->b2 -= jacobianRow[2] * residual * weight;
  pAcc->b3 -= jacobianRow[3] * residual * weight;
  pAcc->b4 -= jacobianRow[4] * residual * weight;
  pAcc->b5 -= jacobianRow[5] * residual * weight;
  pAcc->error += residual * residual * weight;
  pAcc->validEntryCount += 1;
  pAcc->minResidual = fmin(pAcc->minResidual, residual);
  pAcc->maxResidual = fmax(pAcc->maxResidual, residual);
}

void addAccumulatorToAccumulator(const local Accumulator* pInAcc,
                                 local Accumulator* pInOutAcc) {
  pInOutAcc->a00 += pInAcc->a00;
  pInOutAcc->a10 += pInAcc->a10;
  pInOutAcc->a11 += pInAcc->a11;
  pInOutAcc->a20 += pInAcc->a20;
  pInOutAcc->a21 += pInAcc->a21;
  pInOutAcc->a22 += pInAcc->a22;
  pInOutAcc->a30 += pInAcc->a30;
  pInOutAcc->a31 += pInAcc->a31;
  pInOutAcc->a32 += pInAcc->a32;
  pInOutAcc->a33 += pInAcc->a33;
  pInOutAcc->a40 += pInAcc->a40;
  pInOutAcc->a41 += pInAcc->a41;
  pInOutAcc->a42 += pInAcc->a42;
  pInOutAcc->a43 += pInAcc->a43;
  pInOutAcc->a44 += pInAcc->a44;
  pInOutAcc->a50 += pInAcc->a50;
  pInOutAcc->a51 += pInAcc->a51;
  pInOutAcc->a52 += pInAcc->a52;
  pInOutAcc->a53 += pInAcc->a53;
  pInOutAcc->a54 += pInAcc->a54;
  pInOutAcc->a55 += pInAcc->a55;
  pInOutAcc->b0 += pInAcc->b0;
  pInOutAcc->b1 += pInAcc->b1;
  pInOutAcc->b2 += pInAcc->b2;
  pInOutAcc->b3 += pInAcc->b3;
  pInOutAcc->b4 += pInAcc->b4;
  pInOutAcc->b5 += pInAcc->b5;
  pInOutAcc->error += pInAcc->error;
  pInOutAcc->validEntryCount += pInAcc->validEntryCount;
  pInOutAcc->minResidual = fmin(pInOutAcc->minResidual, pInAcc->minResidual);
  pInOutAcc->maxResidual = fmax(pInOutAcc->maxResidual, pInAcc->maxResidual);
}

void storeSumOfWorkgroupAccumulators(local Accumulator* pLocalAccumulators,
                                     global Accumulator* pGlobalAccumulators) {
  barrier(CLK_LOCAL_MEM_FENCE);
  for(int offset = get_local_size(0) / 2; offset > 0; offset = offset / 2) {
    if (get_local_id(0) < offset) {
      addAccumulatorToAccumulator(pLocalAccumulators + get_local_id(0) + offset,
                                  pLocalAccumulators + get_local_id(0));
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  if (get_local_id(0) == 0) {
    pGlobalAccumulators[get_group_id(0)] = pLocalAccumulators[0];
  }
}
