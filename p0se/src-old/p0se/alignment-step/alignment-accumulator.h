#pragma once

#include <Eigen/Dense>
#include <CL/cl2.hpp>

namespace p0se {

class AlignmentAccumulator {
  // J' * J (symmetric matrix)
  cl_float a00;
  cl_float a10, a11;
  cl_float a20, a21, a22;
  cl_float a30, a31, a32, a33;
  cl_float a40, a41, a42, a43, a44;
  cl_float a50, a51, a52, a53, a54, a55;

  // -J' * r
  cl_float b0;
  cl_float b1;
  cl_float b2;
  cl_float b3;
  cl_float b4;
  cl_float b5;

  cl_float error;
  cl_int validEntryCount;

  cl_float minResidual_, maxResidual_;

 public:
  AlignmentAccumulator& operator+=(const AlignmentAccumulator& acc2);

  Eigen::Matrix<float, 6, 6> a();
  Eigen::Matrix<float, 6, 1> b();

  float minResidual() { return minResidual_; }
  float maxResidual() { return maxResidual_; }
};

} // End of namespace p0se
