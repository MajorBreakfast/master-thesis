typedef struct {
  AffineTransform3f orig;
  AffineTransform3f perm[6];
} TransformsParam;

inline float fn(float x) { return fabs(x); }

kernel void alignDepthRGBNumeric(// Input
                                 const global float* pPreDepth,
                                 const global uchar* pPreRGB,
                                 const global float* pCurDepth,
                                 const global uchar* pCurRGB,
                                 const int2 size,
                                 const float invEps,
                                 const TransformsParam transforms,
                                 const float depthGain,
                                 const float rgbGain,

                                 // Input: Histogram
                                 const float histogramMinValue,
                                 const float histogramBucketWidth,
                                 const int histogramSize,

                                 // Input: Visualization
                                 char visualize,
                                 global float* pResidual,
                                 global float* pCurDepthReprojected,
                                 global uchar* pCurRGBReprojected,
                                 global float* pJacobianColumn0,
                                 global float* pJacobianColumn1,
                                 global float* pJacobianColumn2,
                                 global float* pJacobianColumn3,
                                 global float* pJacobianColumn4,
                                 global float* pJacobianColumn5,

                                 // Local memory
                                 local Accumulator* pLocalAccumulators,
                                 local int* pLocalHistogram,

                                 // Output: Accumulators, Histogram
                                 global Accumulator* pGlobalAccumulators,
                                 global int* pGlobalHistograms) {
  local Accumulator* pAcc = pLocalAccumulators + get_local_id(0);
  resetAccumulator(pAcc);

  int pixelCount = size.x * size.y;

  // Loop over pixels: Accumulate each pixel's contribution to J' * J and J' * r
  for (int i = get_global_id(0); i < pixelCount; i += get_global_size(0)) {
    // Get 2d position
    int2 prePosInt = delinearize(i, size.x);
    float2 prePos = convert_float2(prePosInt);

    // Load previous frame depth and rgb values
    float preDepth = readDepth(prePosInt, size, pPreDepth);
    float3 preRGB = readRGB(prePosInt, size, pPreRGB);

    // Reproject into current frame using non-permuated transform
    float2 curPosOrig = transformPreToCur(prePos, preDepth, transforms.orig);

    // Compute depth residual for non-permutated transform
    float curDepthOrig = readDepthInterp(curPosOrig, size, pCurDepth);
    float depthResidualOrig = fn(preDepth - curDepthOrig);

    // Compute rgb residual for non-permutated transform
    float3 curRGBOrig = readRGBInterp(curPosOrig, size, pCurRGB);
    float rgbResidualOrig = fn(distance(preRGB, curRGBOrig));

    // Combined residual for non-permutated transform
    float residualOrig = depthResidualOrig * depthGain +
                         rgbResidualOrig * rgbGain;

    // Compute jacobian row
    float jacobianRow[6];
    #pragma unroll
    for (int d = 0; d < 6; ++d) { // 6 degrees of freedom => 6 permutations
      // Reproject into current frame using permutated transform
      float2 curPosPerm = transformPreToCur(prePos, preDepth, transforms.perm[d]);

      // Compute depth residual for permutated transform
      float curDepthPerm = readDepthInterp(curPosPerm, size, pCurDepth);
      float depthResidualPerm = fn(preDepth - curDepthPerm);

      // Compute rgb residual for permutated transform
      float3 curRGBPerm = readRGBInterp(curPosPerm, size, pCurRGB);
      float rgbResidualPerm = fn(distance(preRGB, curRGBPerm));

      // Combined residual for permutated transform
      float residualPerm = depthResidualPerm * depthGain +
                           rgbResidualPerm * rgbGain;

      // Approximate jacobian by a forward difference of the residual values
      jacobianRow[d] = (residualPerm - residualOrig) * invEps;
    } // End loop of 6 degrees of freedom


    bool validJacobian = !isnan(jacobianRow[0]) && !isnan(jacobianRow[1]) &&
                         !isnan(jacobianRow[2]) && !isnan(jacobianRow[3]) &&
                         !isnan(jacobianRow[4]) && !isnan(jacobianRow[5]);

    // Add jacobian row to the accumulator
    if (validJacobian) {
      addRowToAccumulator(residualOrig, jacobianRow, 0.0001f, pAcc);
    }

    if (visualize) {
      pCurDepthReprojected[i] = curDepthOrig;
      vstore3(convert_uchar3(curRGBOrig * 255.0f), i, pCurRGBReprojected);
      pResidual[i] = residualOrig;
      pJacobianColumn0[i] = validJacobian ? jacobianRow[0] : NAN;
      pJacobianColumn1[i] = validJacobian ? jacobianRow[1] : NAN;
      pJacobianColumn2[i] = validJacobian ? jacobianRow[2] : NAN;
      pJacobianColumn3[i] = validJacobian ? jacobianRow[3] : NAN;
      pJacobianColumn4[i] = validJacobian ? jacobianRow[4] : NAN;
      pJacobianColumn5[i] = validJacobian ? jacobianRow[5] : NAN;
    }
  } // End loop over pixels

  // Perform reduction and store accumulator in global buffer
  storeSumOfWorkgroupAccumulators(pLocalAccumulators, pGlobalAccumulators);
}
