#include "p0se/alignment-step/numeric/depth-rgb/numeric-depth-rgb-alignment-step.h"

#include <iostream>

#include "p0se/kernels.h"
#include "p0se/utils.h"
#include "p0se/utils/transform-from-xi.h"
#include "p0se/images/device-image-pyramid.h"
#include <stdexcept>

namespace p0se {

NumericDepthRGBAlignmentStep::NumericDepthRGBAlignmentStep(
    const cl::Context& context,
    int32_t width,
    int32_t height,
    int32_t levelCount,
    int32_t histogramSize,
    bool isVisualizationEnabled) {
  cl::Device device = context.getInfo<CL_CONTEXT_DEVICES>()[0];
  int32_t computeUnitCount = device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();

  int32_t workgroupCount = computeUnitCount * 8;

  // Allocate memory
  accumulators_.resize(workgroupCount);
  accumulatorsBuffer_ = cl::Buffer(context, CL_MEM_READ_WRITE,
                                   accumulators_.size() * sizeof(AlignmentAccumulator));

  histograms_.resize(workgroupCount * histogramSize);
  histogramsBuffer_ = cl::Buffer(context, CL_MEM_READ_WRITE,
                                 histograms_.size() * sizeof(cl_int));

  // Build kernel
  kernel_ = createOrReuseKernel(context, "alignDepthRGBNumeric", {
    "p0se/math.cl",
    "p0se/alignment-step/alignment-accumulator.cl",
    "p0se/alignment-step/numeric/depth-rgb/align-depth-rgb-numeric.cl"
  });

  if (isVisualizationEnabled) {
    curDepthReprojectedDeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::DEPTH, levelCount);
    curRGBReprojectedDeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::UCHAR3, p0se::RGB, levelCount);
    residualDeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::RESIDUAL, levelCount);
    jacobianColumn0DeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::JACOBIAN, levelCount);
    jacobianColumn1DeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::JACOBIAN, levelCount);
    jacobianColumn2DeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::JACOBIAN, levelCount);
    jacobianColumn3DeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::JACOBIAN, levelCount);
    jacobianColumn4DeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::JACOBIAN, levelCount);
    jacobianColumn5DeviceImages_ = p0se::createDeviceImagePyramid(
      context, width, height, p0se::FLOAT, p0se::JACOBIAN, levelCount);
  }
}

void NumericDepthRGBAlignmentStep::run(
    const cl::CommandQueue& commandQueue,
    const DeviceImage& preDepthDeviceImage,
    const DeviceImage& preRGBDeviceImage,
    const DeviceImage& curDepthDeviceImage,
    const DeviceImage& curRGBDeviceImage,
    const Eigen::Matrix3f& cameraMatrix,
    const Sophus::Vector6f& xi,
    float depthGain,
    float rgbGain,
    int32_t level, // Pyramid level, only needed if visualizing
    bool showResidualAndJacobian,
    Sophus::Vector6f* pUpdatedXi,
    Histogram* pHistogram,
    float* pError) {
  int32_t workgroupSize = 128;

  // Compute transforms from xi
  float eps = 0.000001f;// / std::powf(2, level);
  TransformsParam transformsParam;
  transformsParam.orig = transformFromXi(xi, cameraMatrix);
  for (int i = 0; i < 6; ++i) { // Loop over 6 degrees of freedom
    Sophus::Vector6f epsVec = Sophus::Vector6f::Zero(); epsVec(i) = eps;
    transformsParam.perm[i] = transformFromXi(xi + epsVec, cameraMatrix);
  } // End of loop over 6 degrees of freedom

  { // Reset global histogram buffer to zeros
    cl_int zero = 0;
    clEnqueueFillBuffer(commandQueue(),
      histogramsBuffer_(),
      &zero, sizeof(zero),
      0, histograms_.size() * sizeof(cl_int),
      0, nullptr, nullptr);
  }

  // Set kernel args
  // Input
  kernel_.setArg(0, preDepthDeviceImage.buffer()); // const global float* pPreDepth
  kernel_.setArg(1, preRGBDeviceImage.buffer()); // const global uchar* pPreRGB
  kernel_.setArg(2, curDepthDeviceImage.buffer()); // const global float* pCurDepth
  kernel_.setArg(3, curRGBDeviceImage.buffer()); // const global uchar* pCurRGB
  kernel_.setArg(4, cl_int2{ preDepthDeviceImage.width(),
                             preDepthDeviceImage.height() }); // const int2 size
  kernel_.setArg(5, 1.0f / eps); // const float invEps
  kernel_.setArg(6, transformsParam); // const TransformsParam transform
  kernel_.setArg(7, depthGain); // const float depthGain
  kernel_.setArg(8, rgbGain); // const float rgbGain

  // Input: Histogram
  kernel_.setArg(9, 0.0f); // const float histogramMinValue
  kernel_.setArg(10, 1.0f); // const float histogramBucketWidth
  kernel_.setArg(11, 1024); // const int histogramSize

  // Input: Visualization
  kernel_.setArg(12, cl_char(showResidualAndJacobian));
  kernel_.setArg(13, residualDeviceImages_[level].buffer());
  kernel_.setArg(14, curDepthReprojectedDeviceImages_[level].buffer());
  kernel_.setArg(15, curRGBReprojectedDeviceImages_[level].buffer());
  kernel_.setArg(16, jacobianColumn0DeviceImages_[level].buffer());
  kernel_.setArg(17, jacobianColumn1DeviceImages_[level].buffer());
  kernel_.setArg(18, jacobianColumn2DeviceImages_[level].buffer());
  kernel_.setArg(19, jacobianColumn3DeviceImages_[level].buffer());
  kernel_.setArg(20, jacobianColumn4DeviceImages_[level].buffer());
  kernel_.setArg(21, jacobianColumn5DeviceImages_[level].buffer());

  // Local memory
  kernel_.setArg(22, workgroupSize * sizeof(AlignmentAccumulator), nullptr); // local Accumulator* pLocalAccumulators
  kernel_.setArg(23, 1 * sizeof(cl_int), nullptr);  // local int* pLocalHistogram

  // Output: Accumulators, Histogram
  kernel_.setArg(24, accumulatorsBuffer_); // global Accumulator* pGlobalAccumulators
  kernel_.setArg(25, accumulatorsBuffer_); // global int* pGlobalHistograms

  // Start kernel
  cl::NDRange offset = cl::NullRange;
  cl::NDRange localSize(workgroupSize);
  cl::NDRange globalSize(accumulators_.size() * workgroupSize);
  commandQueue.enqueueNDRangeKernel(kernel_, offset, globalSize, localSize);

  // Read accumulators buffer
  commandQueue.enqueueReadBuffer(
    accumulatorsBuffer_, false,
    0, accumulators_.size() * sizeof(AlignmentAccumulator),
    accumulators_.data());

  // Read histograms buffer
  commandQueue.enqueueReadBuffer(histogramsBuffer_, false,
    0, histograms_.size() * sizeof(cl_int),
    histograms_.data());

  commandQueue.finish();

  AlignmentAccumulator acc = {};
  for (const AlignmentAccumulator& acc2 : accumulators_) { acc += acc2; }

  Sophus::Vector6f deltaXi = acc.a().ldlt().solve(acc.b());
  *pUpdatedXi = xi + deltaXi; // Sophus::SE3f::log(Sophus::SE3f::exp(deltaXi) * Sophus::SE3f::exp(xi));

  if (showResidualAndJacobian) {
    showImage(residualDeviceImages_[level].read(commandQueue), "Residual", 10, 10);
    showImage(preDepthDeviceImage.read(commandQueue), "Pre Depth", 700, 10);
    showImage(curDepthDeviceImage.read(commandQueue), "Cur Depth", 700, 10);
    showImage(curDepthReprojectedDeviceImages_[level].read(commandQueue), "Cur Depth Reprojected", 700, 10);
    showImage(preRGBDeviceImage.read(commandQueue), "Pre RGB", 1000, 10);
    showImage(curRGBDeviceImage.read(commandQueue), "Cur RGB", 1000, 10);
    showImage(curRGBReprojectedDeviceImages_[level].read(commandQueue), "Cur RGB Reprojected", 1000, 10);
    showImage(jacobianColumn0DeviceImages_[level].read(commandQueue), "Jacobian Column 0", 0, 400);
    showImage(jacobianColumn1DeviceImages_[level].read(commandQueue), "Jacobian Column 1", 200, 400);
    showImage(jacobianColumn2DeviceImages_[level].read(commandQueue), "Jacobian Column 2", 400, 400);
    showImage(jacobianColumn3DeviceImages_[level].read(commandQueue), "Jacobian Column 3", 600, 400);
    showImage(jacobianColumn4DeviceImages_[level].read(commandQueue), "Jacobian Column 4", 800, 400);
    showImage(jacobianColumn5DeviceImages_[level].read(commandQueue), "Jacobian Column 5", 1000, 400);
    wait(0);
  }
}

} // End of namespace p0se
