#pragma once

#include <Eigen/Dense>
#include <sophus/se3.hpp>

#include "p0se/images/device-image.h"
#include "p0se/timing/timer-group.h"
#include "p0se/kernel-structs.h"
#include "p0se/alignment-step/alignment-accumulator.h"
#include "p0se/histogram/histogram.h"

namespace p0se {

struct AlignmentVisualizationImages {
  std::vector<DeviceImage> residuals_;
  std::vector<DeviceImage> jacobianColumns0_;
  std::vector<DeviceImage> jacobianColumns1_;
  std::vector<DeviceImage> jacobianColumns2_;
  std::vector<DeviceImage> jacobianColumns3_;
  std::vector<DeviceImage> jacobianColumns4_;
  std::vector<DeviceImage> jacobianColumns5_;
};

class NumericDepthRGBAlignmentStep {
 public:
  NumericDepthRGBAlignmentStep(const cl::Context& context,
                               int32_t width,
                               int32_t height,
                               int32_t levelCount,
                               int32_t histogramSize,
                               bool isVisualizationEnabled);

  void run(const cl::CommandQueue& commandQueue,
           const DeviceImage& preDepthDeviceImage,
           const DeviceImage& preRGBDeviceImage,
           const DeviceImage& curDepthDeviceImage,
           const DeviceImage& curRGBDeviceImage,
           const Eigen::Matrix3f& cameraMatrix,
           const Sophus::Vector6f& xi,
           float depthGain,
           float rgbGain,
           int32_t level, // Pyramid level, only needed if visualizing
           bool showResidualAndJacobian,
           Sophus::Vector6f* pUpdatedXi,
           Histogram* pHistogram,
           float* pError); // Error for the old xi, not the updated xi

 private:
   struct TransformsParam {
    KernelAffineTransform3f orig;
    KernelAffineTransform3f perm[6];
  };

  cl::Kernel kernel_;
  int32_t workgroupCount_;

  cl::Buffer accumulatorsBuffer_;
  std::vector<AlignmentAccumulator> accumulators_;

  int32_t histgramSize_;
  cl::Buffer histogramsBuffer_;
  std::vector<cl_int> histograms_;

  // Buffers for visualization
  std::vector<DeviceImage> curDepthReprojectedDeviceImages_;
  std::vector<DeviceImage> curRGBReprojectedDeviceImages_;
  std::vector<DeviceImage> residualDeviceImages_;
  std::vector<DeviceImage> jacobianColumn0DeviceImages_;
  std::vector<DeviceImage> jacobianColumn1DeviceImages_;
  std::vector<DeviceImage> jacobianColumn2DeviceImages_;
  std::vector<DeviceImage> jacobianColumn3DeviceImages_;
  std::vector<DeviceImage> jacobianColumn4DeviceImages_;
  std::vector<DeviceImage> jacobianColumn5DeviceImages_;
};

} // End of namespace p0se
