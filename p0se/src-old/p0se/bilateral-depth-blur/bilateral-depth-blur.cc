#include "p0se/bilateral-depth-blur/bilateral-depth-blur.h"

#include "p0se/kernels.h"
#include "p0se/kernel-structs.h"

namespace p0se {

BilateralDepthBlur::BilateralDepthBlur(const cl::Context& context) {
  kernel_ = createOrReuseKernel(context, "bilateralDepthBlur", {
    "p0se/math.cl",
    "p0se/bilateral-depth-blur/bilateral-depth-blur.cl"
  });
}

void BilateralDepthBlur::enqueue(const cl::CommandQueue& commandQueue,
                                 const DeviceImage& depthDeviceImage,
                                 DeviceImage* pBlurredDepthDeviceImage) {
  // Settings
  const int32_t halfFilterSize = 10;
  const float rangeSigma = 0.002f;
  const float distanceSigma = 10.0f;

  // Set kernel args
  kernel_.setArg(0, depthDeviceImage.buffer()); // const global float* pDepth,
  kernel_.setArg(1, cl_int2{ depthDeviceImage.width(),
                             depthDeviceImage.height() }); // const int2 size,
  kernel_.setArg(2, cl_int(halfFilterSize)); // const int halfFilterSize,
  kernel_.setArg(3, cl_float(1.0f / rangeSigma)); // const float invSigmaRangeSquared,
  kernel_.setArg(4, cl_float(1.0f / distanceSigma)); // const float invSigmaDistanceSquared,
  kernel_.setArg(5, pBlurredDepthDeviceImage->buffer()); // global float* pBlurredDepth

  // Start kernel
  cl::NDRange offset = cl::NullRange;
  cl::NDRange localSize(16, 16);
  cl::NDRange globalSize(nextMultiple(depthDeviceImage.width(), 16),
                         nextMultiple(depthDeviceImage.height(), 16));
  commandQueue.enqueueNDRangeKernel(kernel_, offset, globalSize, localSize);
}

} // End of namespace p0se
