inline float fsquare (float x) { return x*x; }
inline bool isDepthValueValid (float d) { return d != 0.0f && !isnan(d); }

kernel void bilateralDepthBlur(
    const global float* pDepth,
    const int2 size,
    const int halfFilterSize,
    const float invSigmaRangeSquared,
    const float invSigmaDistanceSquared,
    global float* pBlurredDepth) {
  int2 pos = (int2)(get_global_id(0), get_global_id(1));

  float blurredDepth = NAN;

  if (pos.x >= halfFilterSize && pos.x < size.x - halfFilterSize &&
      pos.y >= halfFilterSize && pos.y < size.y - halfFilterSize) {
    // Load depth value
    float depth0 = pDepth[linearize(pos, size.x)];

    if (isDepthValueValid(depth0)) {
      float valueAcc = 0.0f;
      float weightAcc = 0;

      for (int ky = -halfFilterSize; ky < halfFilterSize; ky++) {
        for (int kx = -halfFilterSize; kx < halfFilterSize; kx++) {
          float depth = pDepth[linearize(pos + (int2)(kx, ky), size.x)];

          if (isDepthValueValid(depth)) {
            float squaredRangeDiff = fsquare(depth - depth0);
            float squaredDistanceDiff = kx*kx + ky*ky;

            float weight = exp(-0.5 * (squaredRangeDiff * invSigmaRangeSquared +
                                       squaredDistanceDiff * invSigmaDistanceSquared));
            valueAcc += depth * weight;
            weightAcc += weight;
          }
        }
      }
      blurredDepth = valueAcc / weightAcc;
    }
  }

  pBlurredDepth[linearize(pos, size.x)] = blurredDepth;
}
