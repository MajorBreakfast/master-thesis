#pragma once

#include "p0se/images/device-image.h"
#include "p0se/utils.h"

namespace p0se {

class BilateralDepthBlur {
 public:
  BilateralDepthBlur(const cl::Context& context);
  void enqueue(const cl::CommandQueue& commandQueue,
               const DeviceImage& deviceImage,
               DeviceImage* pBlurredDeviceImage);

 private:
  cl::Kernel kernel_;
};

} // End of namespace p0se
