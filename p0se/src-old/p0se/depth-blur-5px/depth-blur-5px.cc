#include "p0se/depth-blur-5px/depth-blur-5px.h"

#include "p0se/kernels.h"
#include "p0se/kernel-structs.h"

namespace p0se {

DepthBlur5px::DepthBlur5px(const cl::Context& context) {
  kernel_ = createOrReuseKernel(context, "depthBlur5px", {
    "p0se/math.cl",
    "p0se/depth-blur-5px/depth-blur-5px.cl"
  });
}

void DepthBlur5px::enqueue(const cl::CommandQueue& commandQueue,
                           const DeviceImage& depthDeviceImage,
                           DeviceImage* pBlurredDepthDeviceImage) {
  // Set kernel args
  kernel_.setArg(0, depthDeviceImage.buffer()); // global const float* pDepth
  kernel_.setArg(1, cl_int2{ depthDeviceImage.width(),
                             depthDeviceImage.height() }); // const int2 size
  kernel_.setArg(2, pBlurredDepthDeviceImage->buffer()); // global float* pBlurredDepth

  // Start kernel
  cl::NDRange offset = cl::NullRange;
  cl::NDRange localSize(16, 16);
  cl::NDRange globalSize(nextMultiple(depthDeviceImage.width(), 16),
                         nextMultiple(depthDeviceImage.height(), 16));
  commandQueue.enqueueNDRangeKernel(kernel_, offset, globalSize, localSize);
}

} // End of namespace p0se
