constant float gaussKernel[] = {
  1.0f,  4.0f,  7.0f,  4.0f, 1.0f,
  4.0f, 16.0f, 26.0f, 16.0f, 4.0f,
  7.0f, 26.0f, 41.0f, 26.0f, 7.0f,
  4.0f, 16.0f, 26.0f, 16.0f, 4.0f,
  1.0f,  4.0f,  7.0f,  4.0f, 1.0f
};

kernel void depthBlur5px(
    const global float* pDepth,
    const int2 size,
    global float* pBlurredDepth) {
  const float depthThreshold = 0.1;

  int2 pos = (int2)(get_global_id(0), get_global_id(1));

  if (pos.x >= 2 && pos.x < size.x - 2 &&
      pos.y >= 2 && pos.y < size.y - 2) {
    float valueAcc = 0.0f;
    float weightAcc = 0;
    float minDepth = INFINITY;
    float maxDepth = -INFINITY;

    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 5; j++) {
        float d = pDepth[linearize(pos + (int2)(i, j) - (int2)(2, 2), size.x)];
        if (d > 0) {
          minDepth = fmin(minDepth, d);
          maxDepth = fmax(maxDepth, d);
          float weight = gaussKernel[linearize((int2)(i, j), 5)];
          valueAcc += d * weight;
          weightAcc += weight;
        }
      }
    }

    bool isAcceptable = (maxDepth - minDepth) < depthThreshold;
    pBlurredDepth[linearize(pos, size.x)] = isAcceptable ?
                                            valueAcc / weightAcc :
                                            0.0f;
  }
}
