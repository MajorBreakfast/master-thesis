kernel void calculateDepthResidual(global const float* pPreDepth,
                                   global const float* pCurDepth,
                                   const int2 size,
                                   const AffineTransform3f transform,
                                   global float* pResidual) {
  int pixelCount = size.x * size.y;

  if (get_global_id(0) < pixelCount) {
    // Get 2d position
    int2 prePosInt = delinearize(get_global_id(0), size.x);
    float2 prePos = convert_float2(prePosInt);

    // Load previous depth depth value
    float preDepth = readDepth(prePosInt, size, pPreDepth);

    // Load matching current depth value for not permutated transfrom
    float2 curPos = transformPreToCur(prePos, preDepth, transform);
    float curDepth = readDepthInterp(curPos, size, pCurDepth);
    pResidual[get_global_id(0)] = preDepth - curDepth;
  }
}
