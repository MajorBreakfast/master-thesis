#include "p0se/depth-residual-calculator/depth-residual-calculator.h"

#include <cinttypes>

#include "p0se/kernels.h"
#include "p0se/kernel-structs.h"
#include "p0se/utils.h"
#include "p0se/utils/transform-from-xi.h"

namespace p0se {

DepthResidualCalculator::DepthResidualCalculator(const cl::Context& context) {
  kernel_ = createOrReuseKernel(context, "calculateDepthResidual", {
    "p0se/math.cl",
    "p0se/depth-residual-calculator/calculate-depth-residual.cl"
  });
}

void DepthResidualCalculator::enqueue(const cl::CommandQueue& commandQueue,
                                      const DeviceImage& preDepthDeviceImage,
                                      const DeviceImage& curDepthDeviceImage,
                                      const Sophus::Vector6f xi,
                                      const Eigen::Matrix3f cameraMatrix,
                                      DeviceImage* pResidualDeviceImage) {
  // Compute transform from xi
  Eigen::Matrix4f transform = transformFromXi(xi, cameraMatrix);

  // Set kernel args
  kernel_.setArg(0, preDepthDeviceImage.buffer()); // global const float* pPreDepth
  kernel_.setArg(1, curDepthDeviceImage.buffer()); // global const float* pCurDepth
  kernel_.setArg(2, cl_int2{ preDepthDeviceImage.width(),
                             preDepthDeviceImage.height() }); // const int2 size
  kernel_.setArg(3, KernelAffineTransform3f(transform)); // const AffineTransform3f transform
  kernel_.setArg(4, pResidualDeviceImage->buffer()); // global float* pResidual

  // Start kernel
  int32_t workgroupSize = 128;
  cl::NDRange offset = cl::NullRange;
  cl::NDRange localSize(workgroupSize);
  cl::NDRange globalSize(nextMultiple(preDepthDeviceImage.pixelCount(),
                                      workgroupSize));
  commandQueue.enqueueNDRangeKernel(kernel_, offset, globalSize, localSize);
}

} // End of namespace p0se
