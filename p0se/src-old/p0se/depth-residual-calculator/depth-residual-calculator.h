#pragma once

#include <Eigen/Core>
#include <sophus/se3.hpp>

#include "p0se/images/device-image.h"

namespace p0se {

class DepthResidualCalculator {
 public:
  DepthResidualCalculator(const cl::Context& context);
  void enqueue(const cl::CommandQueue& commandQueue,
               const DeviceImage& preDepthDeviceImage,
               const DeviceImage& curDepthDeviceImage,
               const Sophus::Vector6f xi,
               const Eigen::Matrix3f cameraMatrix,
               DeviceImage* pResidualDeviceImage);

 private:
  cl::Kernel kernel_;
};

} // End of namespace p0se
