kernel void DownsampleFloatDepth(
    const global float* src_buf,
    const int2 dst_size,
    global float* dst_buf) {
  int2 dst_pos = (int2)(get_global_id(0), get_global_id(1));

  if (dst_pos.x >= dst_size.x || dst_pos.y >= dst_size.y) { return; }

  int2 src_pos = 2 * dst_pos; //
  int src_ld = 2 * dst_size.x; // Leading dimension (width)

  float v00 = src_buf[linearize(src_pos               , src_ld)];
  float v10 = src_buf[linearize(src_pos + (int2)(1, 0), src_ld)];
  float v01 = src_buf[linearize(src_pos + (int2)(0, 1), src_ld)];
  float v11 = src_buf[linearize(src_pos + (int2)(1, 1), src_ld)];

  float acc = v00 + v10 + v01 + v11;
  int num_valid = (v00 > 0) + (v10 > 0) + (v01 > 0) + (v11 > 0);

  dst_buf[linearize(dst_pos, dst_size.x)] = num_valid > 0 ? acc / num_valid : 0;
}
