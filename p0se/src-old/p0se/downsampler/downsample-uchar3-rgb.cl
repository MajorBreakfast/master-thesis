kernel void DownsampleUchar3RGB(
    const global uchar* src_buf,
    const int2 dst_size,
    global uchar* dst_buf) {
  int2 dst_pos = (int2)(get_global_id(0), get_global_id(1));

  if (dst_pos.x >= dst_size.x || dst_pos.y >= dst_size.y) { return; }

  int2 src_pos = 2 * dst_pos;
  int src_ld = 2 * dst_size.x;

  uchar3 v00 = vload3(linearize(src_pos               , src_ld), src_buf);
  uchar3 v10 = vload3(linearize(src_pos + (int2)(1, 0), src_ld), src_buf);
  uchar3 v01 = vload3(linearize(src_pos + (int2)(0, 1), src_ld), src_buf);
  uchar3 v11 = vload3(linearize(src_pos + (int2)(1, 1), src_ld), src_buf);

  uchar3 res;
  res.x = (uchar)(((int)v00.x + v10.x + v01.x + v11.x) / 4);
  res.y = (uchar)(((int)v00.y + v10.y + v01.y + v11.y) / 4);
  res.z = (uchar)(((int)v00.z + v10.z + v01.z + v11.z) / 4);

  vstore3(res, linearize(dst_pos, dst_size.x), dst_buf);
}
