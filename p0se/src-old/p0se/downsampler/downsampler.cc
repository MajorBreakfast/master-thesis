#include "p0se/downsampler/downsampler.h"
#include "p0se/kernels.h"
#include <stdexcept>

namespace p0se {

Downsampler::Downsampler(const cl::Context& context) {
  uchar3RGBKernel_ = createOrReuseKernel(context, "DownsampleUchar3RGB", {
    "p0se/math.cl",
    "p0se/downsampler/downsample-uchar3-rgb.cl"
  });

  floatDepthKernel_ = createOrReuseKernel(context, "DownsampleFloatDepth", {
    "p0se/math.cl",
    "p0se/downsampler/downsample-float-depth.cl"
  });
}

void Downsampler::enqueue(const cl::CommandQueue& commandQueue,
                          const DeviceImage& srcDeviceImage,
                          DeviceImage* pDstDeviceImage) {
  if (srcDeviceImage.width() / 2 != pDstDeviceImage->width() ||
      srcDeviceImage.height() / 2 != pDstDeviceImage->height() ||
      srcDeviceImage.dataType() != pDstDeviceImage->dataType() ||
      srcDeviceImage.purpose() != pDstDeviceImage->purpose()) {
    throw std::runtime_error("Input and output device images are not compatible");
  }

  cl::Kernel kernel;
  switch (srcDeviceImage.purpose()) {
    case RGB: kernel = uchar3RGBKernel_; break;
    case DEPTH: kernel = floatDepthKernel_; break;
    default: throw std::runtime_error("Purpose of the images is not supported");
  }

  kernel.setArg(0, srcDeviceImage.buffer());
  cl_int2 size = { static_cast<cl_int>(pDstDeviceImage->width()),
                   static_cast<cl_int>(pDstDeviceImage->height()) };
  kernel.setArg(1, size);
  kernel.setArg(2, pDstDeviceImage->buffer());

  cl::NDRange global(nextMultiple(pDstDeviceImage->width(), 32),
                     nextMultiple(pDstDeviceImage->height(), 32));
  commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, global,
                                    cl::NullRange);
}

} // End of namespace p0se
