#pragma once

#include "p0se/images/device-image.h"
#include "p0se/utils.h"

namespace p0se {

class Downsampler {
 public:
  Downsampler(const cl::Context& context);
  void enqueue(const cl::CommandQueue& commandQueue,
               const DeviceImage& srcDeviceImage,
               DeviceImage* pDstDeviceImage);

 private:
  cl::Kernel uchar3RGBKernel_;
  cl::Kernel floatDepthKernel_;
};

} // End of namespace p0se
