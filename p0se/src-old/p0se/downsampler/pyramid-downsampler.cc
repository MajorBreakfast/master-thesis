#include "p0se/downsampler/pyramid-downsampler.h"

namespace p0se {

PyramidDownsampler::PyramidDownsampler(const cl::Context& context)
  : downsampler_(context) {}

void PyramidDownsampler::enqueue(const cl::CommandQueue& commandQueue,
                                 std::vector<DeviceImage>* pImages) {
  int32_t levelCount = static_cast<int32_t>(pImages->size());

  for (int32_t l = 0; l < levelCount - 1; ++l) {
    downsampler_.enqueue(commandQueue,
                         pImages->at(l),
                         &pImages->at(l + 1));
  }
}

} // End of namespace p0se
