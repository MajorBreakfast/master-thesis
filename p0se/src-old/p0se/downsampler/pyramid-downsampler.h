#pragma once

#include "p0se/downsampler/downsampler.h"
#include <vector>

namespace p0se {

class PyramidDownsampler {
 public:
  PyramidDownsampler(const cl::Context& context);
  void enqueue(const cl::CommandQueue& commandQueue,
               std::vector<DeviceImage>* pImages);

 private:
  Downsampler downsampler_;
};

} // End of namespace p0se
