#include "p0se/histogram/histogram.h"

namespace p0se {

Histogram::Histogram(float minValue, float maxValue, int32_t size)
  : minValue_(minValue),
    maxValue_(maxValue),
    size_(size) {}

} // End of namespace p0se
