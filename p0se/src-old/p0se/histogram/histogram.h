#pragma once

#include <vector>
#include <cinttypes>

namespace p0se {

class Histogram {
public:
  Histogram(float minValue, float maxValue, int32_t size);

  float minValue() const { return minValue_; }
  float maxValue() const { return maxValue_; }
  float size() const { return size_;  }
  std::vector<int32_t> buckets() { return buckets_; }

private:
  float minValue_;
  float maxValue_;
  int32_t size_;
  std::vector<int32_t> buckets_;
};

} // End of namespace p0se
