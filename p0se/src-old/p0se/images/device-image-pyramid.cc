#include "p0se/images/device-image-pyramid.h"
#include <cassert>

namespace p0se {

std::vector<DeviceImage> createDeviceImagePyramid(
    cl::Context context,
    int32_t width, int32_t height,
    ImageDataType dataType,
    ImagePurpose purpose,
    int32_t num_levels) {
  std::vector<DeviceImage> deviceImages;
  for (int l = 0; l < num_levels; ++l) {
    deviceImages.push_back(
        DeviceImage(context, width >> l, height >> l, dataType, purpose));
  }
  return deviceImages;
}

} // namespace p0se
