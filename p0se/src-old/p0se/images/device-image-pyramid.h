#pragma once

#include <cinttypes>
#include <vector>
#include <CL/cl2.hpp>
#include "p0se/images/device-image.h"

namespace p0se {

/**
Creates a device image pyramid which is just a vector of device images with
length `numLevels`. Level zero has the full width and height and each following
level has half the width and height of its respective previous level.
*/
std::vector<DeviceImage> createDeviceImagePyramid(
    cl::Context context,
    int32_t width, int32_t height,
    ImageDataType dataType,
    ImagePurpose purpose,
    int32_t levelCount);

} // namespace p0se
