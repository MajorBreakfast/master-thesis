#include <p0se/images/device-image.h>
#include <cassert>
#include <iostream>
#include <stdexcept>

namespace p0se {

DeviceImage::DeviceImage(cl::Context context,
            int32_t width,
            int32_t height,
            ImageDataType dataType,
            ImagePurpose purpose)
    : Image(width, height, dataType, purpose) {
  int32_t size = width * height;
  switch (dataType) {
    case FLOAT: size *= sizeof(float); break;
    case UCHAR3: size *= sizeof(float) * 3; break;
  }
  buffer_ = cl::Buffer(context, CL_MEM_READ_WRITE, size);
}

void DeviceImage::enqueueWrite(const cl::CommandQueue& command_queue,
                               const HostImage& hostImage) {
  assert(hostImage.width() == width());
  assert(hostImage.height() == height());
  assert(hostImage.dataType() == dataType());
  command_queue.enqueueWriteBuffer(buffer(), false, 0,
                                   hostImage.byteCount(),
                                   hostImage.buffer());
  command_queue.flush(); // WTF, but required (just on nvidia?)
}

void DeviceImage::enqueueRead(const cl::CommandQueue& command_queue,
                              HostImage *pHostImage) const {
  assert(pHostImage->width() == width());
  assert(pHostImage->height() == height());
  assert(pHostImage->dataType() == dataType());
  command_queue.enqueueReadBuffer(buffer(), false, 0,
                                  byteCount(),
                                  pHostImage->buffer());
}

HostImage DeviceImage::read(const cl::CommandQueue& command_queue) const {
  HostImage hostImage(width(), height(), dataType(), purpose());
  enqueueRead(command_queue, &hostImage);
  command_queue.finish();
  return hostImage;
}

void DeviceImage::EnqueueWrite(const cl::CommandQueue& command_queue,
                               const void* data, size_t size,
                               const std::vector<cl::Event>* events,
                               cl::Event* event) {
  if (byteCount() != static_cast<int32_t>(size)) {
    throw std::runtime_error("Buffer has unexpected length");
  }
  command_queue.enqueueWriteBuffer(buffer(), false, 0, byteCount(), data,
                                   events, event);
  command_queue.flush(); // WTF, but required (just on nvidia?)
}

void DeviceImage::EnqueueRead(const cl::CommandQueue& command_queue,
                              void* data, size_t size,
                              const std::vector<cl::Event>* events,
                              cl::Event* event) const {
  if (byteCount() != static_cast<int32_t>(size)) {
    throw std::runtime_error("Buffer has unexpected length");
  }
  command_queue.enqueueReadBuffer(buffer(), false, 0, byteCount(), data,
                                  events, event);
}

void DeviceImage::WriteSync(const cl::CommandQueue& command_queue,
                            const void* data, size_t size) {
  if (byteCount() != static_cast<int32_t>(size)) {
    throw std::runtime_error("Buffer has unexpected length");
  }
  command_queue.enqueueWriteBuffer(buffer(), true, 0, byteCount(), data);
  command_queue.flush(); // WTF, but required (just on nvidia?)
}

void DeviceImage::ReadSync(const cl::CommandQueue& command_queue,
                           void* data, size_t size) {
  if (byteCount() != static_cast<int32_t>(size)) {
    throw std::runtime_error("Buffer has unexpected length");
  }
  command_queue.enqueueReadBuffer(buffer(), true, 0, byteCount(), data);
}

} // End of namespace p0se
