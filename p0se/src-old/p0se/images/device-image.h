#pragma once

#include <cinttypes>
#include <CL/cl2.hpp>
#include <vector>
#include "p0se/images/image.h"
#include "p0se/images/host-image.h"

namespace p0se {

class DeviceImage : public Image {
 public:
  DeviceImage(cl::Context context,
              int32_t width,
              int32_t height,
              ImageDataType dataType,
              ImagePurpose purpose = WHATEVER);

  const cl::Buffer buffer() const { return buffer_; }

  void enqueueWrite(const cl::CommandQueue& commandQueue,
                    const HostImage& hostImage);
  void enqueueRead(const cl::CommandQueue& commandQueue,
                   HostImage *pHostImage) const;
  HostImage read(const cl::CommandQueue& commandQueue) const;

  void EnqueueWrite(const cl::CommandQueue& commandQueue,
                    const void* data, size_t size,
                    const std::vector<cl::Event>* events = NULL,
                    cl::Event* event = NULL);
  void EnqueueRead(const cl::CommandQueue& commandQueue,
                   void* data, size_t size,
                   const std::vector<cl::Event>* events = NULL,
                   cl::Event* event = NULL) const;
  void WriteSync(const cl::CommandQueue& commandQueue,
                 const void* data, size_t size);
  void ReadSync(const cl::CommandQueue& commandQueue,
                void* data, size_t size);

 private:
  cl::Buffer buffer_;
};

} // namespace p0se
