#pragma once

#include <cinttypes>
#include <memory>
#include "image.h"

namespace p0se {

class HostImage : public Image {
 public:
  HostImage(int32_t width,
            int32_t height,
            ImageDataType dataType,
            ImagePurpose purpose = WHATEVER)
    : Image(width, height, dataType, purpose) {
    buffer_ = std::shared_ptr<void>(malloc(byteCount()), free);
  }

  void* buffer() { return buffer_.get(); }
  const void* buffer() const { return buffer_.get(); }
 private:
  std::shared_ptr<void> buffer_;
};

} // namespace p0se
