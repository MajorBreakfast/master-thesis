#pragma once

// Common header for DeviceImage and HostImage

namespace p0se {

enum ImageDataType {
  FLOAT,
  UCHAR3,
};

enum ImagePurpose {
  WHATEVER,
  RGB,
  DEPTH,
  RESIDUAL,
  JACOBIAN,
  NORMAL
};

class Image {
 public:
  Image(int32_t width,
        int32_t height,
        ImageDataType dataType,
        ImagePurpose purpose = WHATEVER)
    : width_(width), height_(height), dataType_(dataType), purpose_(purpose) {}

  const int32_t width() const { return width_; }
  const int32_t height() const { return height_; }
  const ImageDataType dataType() const { return dataType_; }
  const ImagePurpose purpose() const { return purpose_; }

  const int32_t pixelCount() const { return width_ * height_; }

  const int32_t subpixelCount() const {
    switch (dataType_) {
    case FLOAT: return width_ * height_;
    case UCHAR3: return 3 * width_ * height_;
    }
  }

  const int32_t byteCount() const {
    switch (dataType_) {
      case FLOAT: return sizeof(float) * width_ * height_;
      case UCHAR3: return 3 * sizeof(uint8_t) * width_ * height_;
      default: return 0;
    }
  }

 private:
  int32_t width_, height_;
  ImageDataType dataType_;
  ImagePurpose purpose_ = WHATEVER;
};

} // namespace p0se
