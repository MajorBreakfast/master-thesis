#pragma once

#include <Eigen/Core>

#include <CL/cl2.hpp>

namespace p0se {

struct KernelAffineTransform3f {
  cl_float4 row0, row1, row2;

  KernelAffineTransform3f() {}
  KernelAffineTransform3f(const Eigen::Matrix4f& mat) {
    row0 = { mat(0, 0), mat(0, 1), mat(0, 2), mat(0, 3) };
    row1 = { mat(1, 0), mat(1, 1), mat(1, 2), mat(1, 3) };
    row2 = { mat(2, 0), mat(2, 1), mat(2, 2), mat(2, 3) };
  }
};

struct KernelAffineTransform2f {
  cl_float3 row0, row1;

  KernelAffineTransform2f() {}
  KernelAffineTransform2f(const Eigen::Matrix3f& mat) {
    row0 = { mat(0, 0), mat(0, 1), mat(0, 2) };
    row1 = { mat(1, 0), mat(1, 1), mat(1, 2) };
  }
};

}
