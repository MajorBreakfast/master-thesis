#include "p0se/kernels.h"

#include <iostream>
#include <map>

namespace p0se {

std::map<std::string, std::string> kKernels = {
#include "p0se/kernels-inc.h"
};

static std::map<std::string, std::map<cl_context, cl::Kernel>> built_kernels;

cl::Kernel createOrReuseKernel(const cl::Context& context,
                               const std::string& name,
                               std::vector<std::string> file_names) {
  cl::Kernel& kernel = built_kernels[name][context()];

  if (kernel() == nullptr) {
    std::vector<std::string> sources;
    for (auto fileName : file_names) { sources.push_back(kKernels[fileName]); }
    cl::Program program = cl::Program(context, sources);

    // Build it
    for (auto& device : context.getInfo<CL_CONTEXT_DEVICES>()) {
      program.build({ device });
      std::cout << "Build log for \"" << name << "\":" << std::endl
        << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device)
        << std::endl << "End log" << std::endl;
    }

    built_kernels[name][context()] = cl::Kernel(program, name.c_str());
  }

  return kernel;
}

cl::Kernel createOrReuseKernel(const cl::CommandQueue& command_queue,
                               const std::string& name,
                               std::vector<std::string> file_names) {
  const cl::Context& context = command_queue.getInfo<CL_QUEUE_CONTEXT>();
  return createOrReuseKernel(context, name, file_names);
}

cl::Kernel getExitingKernel(const cl::Context& context,
                            const std::string& name) {
  return built_kernels[name][context()];
}

cl::Kernel getExitingKernel(const cl::CommandQueue& command_queue,
                            const std::string& name) {
  const cl::Context& context = command_queue.getInfo<CL_QUEUE_CONTEXT>();
  return built_kernels[name][context()];
}

} // namespace p0se
