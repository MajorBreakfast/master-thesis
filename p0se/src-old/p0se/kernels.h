#pragma once

#include <string>
#include <vector>

#include <CL/cl2.hpp>

namespace p0se {

cl::Kernel createOrReuseKernel(const cl::Context& context,
                               const std::string& name,
                               std::vector<std::string> fileNames);

cl::Kernel createOrReuseKernel(const cl::CommandQueue& command_queue,
                               const std::string& name,
                               std::vector<std::string> fileNames);

cl::Kernel getExitingKernel(const cl::Context& context,
                            const std::string& name);

cl::Kernel getExitingKernel(const cl::CommandQueue& command_queue,
                            const std::string& name);

} // namespace p0se
