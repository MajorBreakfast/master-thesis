inline int linearize(int2 pos, int ld) { return pos.x + pos.y * ld; }
inline int2 delinearize(int i, int ld) { return (int2)(i % ld, i / ld); }

typedef struct { float4 row0, row1, row2; } AffineTransform3f;

inline float4 multiplyAffineTransform3fAndVector(const AffineTransform3f trans,
                                                 const float4 vec) {
  return (float4)(dot(trans.row0, vec),
                  dot(trans.row1, vec),
                  dot(trans.row2, vec),
                  vec.s3);
}

typedef struct { float3 row0, row1; } AffineTransform2f;

typedef struct {
  float depth;
  uchar3 rgb;
  uchar3 normal;
} RGBDNPixel; // RGBDN = Red, green, blue, depth, normal

inline float3 multiplyAffineTransform2fAndVector(const AffineTransform2f trans,
                                                 const float3 vec) {
  return (float3)(dot(trans.row0, vec),
                  dot(trans.row1, vec),
                  vec.s2);
}

float2 transformPreToCur(float2 prePos,
                         float preDepth,
                         AffineTransform3f transform) {
  float4 prePoint = (float4)(prePos * preDepth, preDepth, 1);
  float4 curPoint = multiplyAffineTransform3fAndVector(transform, prePoint);
  return curPoint.xy / curPoint.z;
}

inline float readDepth(int2 pos, int2 size, global const float* pDepth) {
  float depth = pDepth[linearize(pos, size.x)];
  return depth == 0 ? NAN : depth;
}

inline float readDepthInterp(float2 pos, int2 size, global const float* pDepth) {
  if (pos.x >= 0 && pos.x < size.x - 1 &&
      pos.y >= 0 && pos.y < size.y - 1) { // Inside?
    int x0 = (int)pos.x;
    int y0 = (int)pos.y;

    float lx = (x0 + 1) - pos.x; // Influence (lambda) of x0
    float ly = (y0 + 1) - pos.y; // Influence (lambda) of y0

    float v00 = pDepth[linearize((int2)(x0    , y0    ), size.x)];
    float v10 = pDepth[linearize((int2)(x0 + 1, y0    ), size.x)];
    float v01 = pDepth[linearize((int2)(x0    , y0 + 1), size.x)];
    float v11 = pDepth[linearize((int2)(x0 + 1, y0 + 1), size.x)];

    if (v00 == 0.0f || v10 == 0.0f || v01 == 0.0f || v11 == 0.0f) {
      return NAN;
    }

    return v00 * (lx       ) * (ly       ) +
           v10 * (1.0f - lx) * (ly       ) +
           v01 * (lx       ) * (1.0f - ly) +
           v11 * (1.0f - lx) * (1.0f - ly);
  } else {
    return NAN;
  }
}

constant float rgbScaleFactor = 1 / 255.0f;

inline float3 readRGB(int2 pos, int2 size, const global uchar* pRGB) {
  return convert_float3(vload3(linearize(pos, size.x), pRGB)) * rgbScaleFactor;
}

inline float3 readRGBInterp(float2 pos, int2 size, const global uchar* pRGB) {
  if (pos.x >= 0 && pos.x < size.x - 1 &&
      pos.y >= 0 && pos.y < size.y - 1) { // Inside?
    int x0 = (int)pos.x;
    int y0 = (int)pos.y;

    float lx = (x0 + 1) - pos.x; // Influence (lambda) of x0
    float ly = (y0 + 1) - pos.y; // Influence (lambda) of y0

    float3 v00 = convert_float3(vload3(linearize((int2)(x0    , y0    ), size.x), pRGB));
    float3 v10 = convert_float3(vload3(linearize((int2)(x0 + 1, y0    ), size.x), pRGB));
    float3 v01 = convert_float3(vload3(linearize((int2)(x0    , y0 + 1), size.x), pRGB));
    float3 v11 = convert_float3(vload3(linearize((int2)(x0 + 1, y0 + 1), size.x), pRGB));

    return (v00 * (lx       ) * (ly       ) +
            v10 * (1.0f - lx) * (ly       ) +
            v01 * (lx       ) * (1.0f - ly) +
            v11 * (1.0f - lx) * (1.0f - ly)) * rgbScaleFactor;
  } else {
    return (float3)(NAN, NAN, NAN);
  }
}
