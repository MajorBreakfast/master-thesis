kernel void approximateMedian(const int count,
                              global const float* data,
                              const float minValue,
                              const float bucketWidth,
                              const int bucketCount,
                              local int* localHistogram,
                              global int* globalHistogram) {
  // Initialize local histogram with zeros
  for (int i = get_local_id(0); i < bucketCount; i += get_local_size(0)) {
    localHistogram[i] = 0;
  }

  // Iterate over data array and fill local histogram
  for (int i = get_global_id(0); i < count; i += get_global_size(0)) {
    int bucketId = 0;
    float x = (data[i] - minValue) / bucketWidth;
    if (x >= 0.0f) {
      bucketId = min((int)x + 1, bucketCount - 1);
    }
    atomic_inc(localHistogram + bucketId);
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  // Write to global histogram
  for (int i = get_local_id(0); i < bucketCount; i += get_local_size(0)) {
    atomic_add(globalHistogram + i, localHistogram[i]);
  }
}
