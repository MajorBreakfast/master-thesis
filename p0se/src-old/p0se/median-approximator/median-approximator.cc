#include "p0se/median-approximator/median-approximator.h"
#include "p0se/kernels.h"

#include <iostream>

namespace p0se {

MedianApproximator::MedianApproximator(const CreateInfo& createInfo)
  : commandQueue_(createInfo.commandQueue),
    min_(createInfo.min),
    max_(createInfo.max),
    bucketCount_(createInfo.bucketCount),
    workgroupSize_(createInfo.workgroupSize),
    histogram_(createInfo.bucketCount) {
  cl::Context clContext = commandQueue_.getInfo<CL_QUEUE_CONTEXT>();
  cl::Device clDevice = commandQueue_.getInfo<CL_QUEUE_DEVICE>();

  kernel_ = createOrReuseKernel(commandQueue_, "approximateMedian", {
    "p0se/median-approximator/approximate-median.cl"
  });

  workgroupCount_ = clDevice.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() * 8;

  histogramBuffer_ = cl::Buffer(clContext, CL_MEM_READ_WRITE,
                                bucketCount_ * sizeof(cl_int));
}

MedianApproximator::Result MedianApproximator::operator()(
    const DeviceImage& image) {
  // .........|----------|----------|     |------------|------------|...........
  // Bucket 0 | bucket 1 | bucket 2 | ... | bucket n-3 | bucket n-2 | bucket n-1
  // .........|----------|----------|     |------------|------------|...........
  //         min                                                   max
  // => The range gets divided into n-2 segments
  float bucketWidth = (max_ - min_) / (bucketCount_ - 2);

  { // Reset global histogram buffer to zeros
    cl_int zero = 0;
    clEnqueueFillBuffer(commandQueue_(),
                        histogramBuffer_(),
                        &zero, sizeof(zero),
                        0, bucketCount_ * sizeof(cl_int),
                        0, nullptr, nullptr);
  }

  // Set arguments
  kernel_.setArg(0, static_cast<cl_int>(image.pixelCount())); // const int count
  kernel_.setArg(1, image.buffer()); // global const float* data
  kernel_.setArg(2, static_cast<cl_float>(min_)); // const float minValue
  kernel_.setArg(3, static_cast<cl_float>(bucketWidth)); // const float bucketWidth
  kernel_.setArg(4, static_cast<cl_int>(bucketCount_)); // const int bucketCount
  kernel_.setArg(5, bucketCount_ * sizeof(cl_int), nullptr); // local int* localHistogram
  kernel_.setArg(6, histogramBuffer_); // global int* globalHistogram

  // Start kernel
  cl::NDRange offset = cl::NullRange;
  cl::NDRange localSize(workgroupSize_);
  cl::NDRange globalSize(workgroupCount_ * workgroupSize_);
  commandQueue_.enqueueNDRangeKernel(kernel_, offset, globalSize, localSize);

  // Read result
  commandQueue_.enqueueReadBuffer(histogramBuffer_, true,
                                  0, bucketCount_ * sizeof(cl_int),
                                  histogram_.data());

  /*int32_t sum = 0;
  for (int32_t j = 0; j < bucketCount_; ++j) {
    sum += histogram_[j];
  }*/

  // Find bucket i that contains median
  int32_t halfCount = image.pixelCount() / 2;
  int32_t seenCount = 0;
  int32_t i = 0;
  int32_t curBucketCount;
  while (true) {
    curBucketCount = histogram_[i];
    seenCount += curBucketCount;
    if (seenCount < halfCount) { i += 1; } else { break; }
  }

  // Approximate median using histogram
  Result result;
  if (i == 0) { // 0th bucket contains median
    result.rangeStatus = SMALLER;
    result.median = min_;
  } else if (i == bucketCount_ - 1) {  // (n-1)th bucket contains median
    result.rangeStatus = BIGGER;
    result.median = max_;
  } else {
    // Interporlate inside the bucket to improve the result
    int32_t beforeMedianInBucket = halfCount - (seenCount - curBucketCount);
    float approx = static_cast<float>(beforeMedianInBucket) / curBucketCount;

    result.rangeStatus = INSIDE;
    result.median = min_ + bucketWidth * ((i - 1) + approx);
    // Note: Subtract 1 to accommodate for bucket 0

    /*
    std::cout << "before " << (min_ + bucketWidth * (i - 1)) << std::endl;
    std::cout << "approx " << (min_ + bucketWidth * (i - 1 + approx)) << std::endl;
    std::cout << "after " << (min_ + bucketWidth * (i)) << std::endl;
    */
  }
  return result;
}

} // End of namespace p0se
