#pragma once

#include <cinttypes>
#include <p0se/images/device-image.h>

namespace p0se {

class MedianApproximator {
 public:
  struct CreateInfo {
    cl::CommandQueue commandQueue;
    float min = 0.0f;
    float max = 1.0f;
    int32_t bucketCount = 2048;
    int32_t workgroupSize = 128;
  };

  enum RangeStatus { SMALLER, INSIDE, BIGGER };

  struct Result {
    RangeStatus rangeStatus;
    float median;
  };

  MedianApproximator(const CreateInfo& createInfo);
  Result operator()(const DeviceImage& image);
 private:
  float min_, max_;
  int32_t bucketCount_;
  cl::Kernel kernel_;
  cl::CommandQueue commandQueue_;
  cl::Buffer histogramBuffer_;
  int32_t workgroupSize_;
  int32_t workgroupCount_;
  std::vector<int32_t> histogram_;
};

} // End of namespace p0se
