inline float3 getVertex (int2 pos,
                         int ld,
                         const global float* dDepth,
                         AffineTransform2f cameraMatrix) {
  float d = dDepth[linearize(pos, ld)];
  if (d == 0) { d = NAN; }
  float3 point = (float3)(d * pos.x, d * pos.y, d);
  point = multiplyAffineTransform2fAndVector(cameraMatrix, point);
  return point;
}

kernel void calculateNormalMap(
    const global float* pDepth,
    const int2 size,
    AffineTransform2f cameraMatrix,
    global uchar* pNormalMap) {
  const float depthThreshold = 0.05f;

  int2 pos = (int2)(get_global_id(0), get_global_id(1));

  if (pos.x >= 1 && pos.x < size.x + 1 &&
      pos.y >= 1 && pos.y < size.y + 1) {
    float3 vertX0 = getVertex(pos + (int2)(-1, 0), size.x, pDepth, cameraMatrix);
    float3 vertX1 = getVertex(pos + (int2)( 1, 0), size.x, pDepth, cameraMatrix);
    float3 vertY0 = getVertex(pos + (int2)(0, -1), size.x, pDepth, cameraMatrix);
    float3 vertY1 = getVertex(pos + (int2)(0,  1), size.x, pDepth, cameraMatrix);

    float3 tangentX = vertX1 - vertX0;
    float3 tangentY = vertY1 - vertY0;

    float3 normal = (float3)(NAN, NAN, NAN);
    if (length(tangentX) < depthThreshold &&
        length(tangentY) < depthThreshold) {
      normal = normalize(cross(tangentX, tangentY));
    }

    uchar3 normalUchar3 = convert_uchar3((normal + (float)(1.0f, 1.0f, 1.0f)) * 128.0f);
    vstore3(normalUchar3, linearize(pos, size.x), pNormalMap);
  }
}
