#include "p0se/normal-map-calculator/normal-map-calculator.h"

#include "p0se/kernels.h"
#include "p0se/kernel-structs.h"

namespace p0se {

NormalMapCalculator::NormalMapCalculator(const cl::Context& context) {
  kernel_ = createOrReuseKernel(context, "calculateNormalMap", {
    "p0se/math.cl",
    "p0se/normal-map-calculator/calculate-normal-map.cl"
  });
}

void NormalMapCalculator::enqueue(const cl::CommandQueue& commandQueue,
                                  const DeviceImage& depthDeviceImage,
                                  const Eigen::Matrix3f& cameraMatrix,
                                  DeviceImage* pNormalMapDeviceImage) {


  // Set kernel args
  kernel_.setArg(0, depthDeviceImage.buffer()); // global const float* pDepth
  kernel_.setArg(1, cl_int2{ depthDeviceImage.width(),
                             depthDeviceImage.height() }); // const int2 size
  kernel_.setArg(2, KernelAffineTransform2f(cameraMatrix.inverse())); // const AffineTransform2f cameraMatrix
  kernel_.setArg(3, pNormalMapDeviceImage->buffer()); // global uchar* pNormalMap

  // Start kernel
  cl::NDRange offset = cl::NullRange;
  cl::NDRange localSize(16, 16);
  cl::NDRange globalSize(nextMultiple(depthDeviceImage.width(), 16),
                         nextMultiple(depthDeviceImage.height(), 16));
  commandQueue.enqueueNDRangeKernel(kernel_, offset, globalSize, localSize);
}

} // End of namespace p0se
