#pragma once

#include "p0se/images/device-image.h"
#include "p0se/utils.h"

namespace p0se {

class NormalMapCalculator {
 public:
  NormalMapCalculator(const cl::Context& context);
  void enqueue(const cl::CommandQueue& commandQueue,
               const DeviceImage& depthDeviceImage,
               const Eigen::Matrix3f& cameraMatrix,
               DeviceImage* pNormalMapDeviceImage);

 private:
  cl::Kernel kernel_;
};

} // End of namespace p0se
