#include "p0se/timing/timer.h"
#include "p0se/timing/timer-group.h"

namespace p0se {

TimerGroup::TimerGroup(std::string label, TimerGroup* pParent) :
    label_(label),
    pParent_(pParent) {}

Timer& TimerGroup::createTimer(std::string label) {
  timers_.push_back(Timer(label, this));
  return timers_.back();
}

Timer& TimerGroup::createAndStartTimer(std::string label) {
  Timer& timer = createTimer(label);
  timer.start();
  return timer;
}

TimerGroup& TimerGroup::createTimerGroup(std::string label) {
  timerGroups_.push_back(TimerGroup(label, this));
  return timerGroups_.back();
}

} // End of namespace p0se
