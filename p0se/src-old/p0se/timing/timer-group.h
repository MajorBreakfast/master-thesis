#pragma once

#include <chrono>
#include <string>
#include <vector>

#include <p0se/timing/timer.h>

namespace p0se {

class TimerGroup {
 public:
   TimerGroup(std::string label, TimerGroup* pParent = nullptr);

  const std::string getLabel() const { return label_; }
  const TimerGroup* getParent() const { return pParent_; }
  const std::vector<Timer> getTimers() const { return timers_; }
  const std::vector<TimerGroup> getTimerGroups() const { return timerGroups_; }

  Timer& createTimer(std::string label);
  Timer& createAndStartTimer(std::string label);

  TimerGroup& createTimerGroup(std::string label);

 private:
  std::string label_;
  TimerGroup* pParent_;
  std::vector<Timer> timers_;
  std::vector<TimerGroup> timerGroups_;
};

} // End of namespace p0se
