#include "p0se/timing/timer.h"

#include <iostream>

namespace p0se {

Timer::Timer(std::string label,
             TimerGroup* pTimerGroup) :
  label_(label),
  pTimerGroup_(pTimerGroup) {}

void Timer::start() {
  start_ = std::chrono::steady_clock::now();
}

void Timer::end() {
  end_ = std::chrono::steady_clock::now();
}

} // End of namespace p0se
