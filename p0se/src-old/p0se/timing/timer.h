#pragma once

#include <chrono>
#include <string>
#include <vector>

namespace p0se {

class TimerGroup;

class Timer {
 friend TimerGroup;

 public:
  // Note: No public constructor, use TimeGroup#createTimer() instead

  const std::string getLabel() const { return label_; }
  const std::chrono::steady_clock::time_point getStart() const { return start_; }
  const std::chrono::steady_clock::time_point getEnd() const { return end_; }

  void start();
  void end();

 private:
  Timer(std::string label,
        TimerGroup* pTimerGroup);

   std::string label_;
   TimerGroup* pTimerGroup_;
   std::chrono::steady_clock::time_point start_;
   std::chrono::steady_clock::time_point end_;
};

} // End of namespace p0se
