#include "p0se/utils.h"

#include <iostream>

namespace p0se {

void (*showImage)(const HostImage&, const std::string&, int, int) =
    [](const HostImage&, const std::string&, int, int) {
  std::cout << "showImage() not implemented: "
               "To use it, assign p0se::ShowImage pointer a function\n";
};

void (*wait)(int) = [](int) {
  std::cout << "wait() not implemented: "
               "To use it, assign p0se::Wait pointer a function\n";
};

int32_t nextMultiple(int32_t size, int32_t factor) {
  return ((size + factor - 1) / factor) * factor;
}

} // namespace p0se
