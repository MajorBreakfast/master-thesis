#pragma once

#include <Eigen/Dense>
#include <sophus/se3.hpp>

#include <vector>
#include <iostream>
#include <Eigen/Dense>
#include <sophus/se3.hpp>

#include <CL/cl2.hpp>
#include "p0se/images/host-image.h"

namespace p0se {

enum Result {
  SUCCESS = 0,
  ERROR = -1,
  INVALID_INPUT = -2,
  NOT_SUPPORTED = -3,
};

extern void (*showImage)(const HostImage& image,
                         const std::string& title, int x, int y);
extern void (*wait)(int delay);

int32_t nextMultiple(int32_t size, int32_t factor);

Eigen::Matrix3f downsampleCameraMatrix(Eigen::Matrix3f mat, int level);

} // namespace p0se
