#include "p0se/utils/downsample-camera-matrix.h"

namespace p0se {

Eigen::Matrix3f downsampleCameraMatrix(Eigen::Matrix3f mat, int level) {
  mat(0, 2) += 0.5; mat(1, 2) += 0.5;
  mat.topLeftCorner(2, 3) *= 1.0f / (1 << level);
  mat(0, 2) -= 0.5; mat(1, 2) -= 0.5;
  return mat;
}

} // End namespace p0se
