#pragma once

#include <Eigen/Core>

namespace p0se {

Eigen::Matrix3f downsampleCameraMatrix(Eigen::Matrix3f mat, int level);

} // End namespace p0se
