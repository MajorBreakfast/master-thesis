/**
Packing makes the memory layout consistent between host and device

Usage:
```C++
P0SE_PACKED_STRUCT StructName {
  ...
} P0SE_END_PACKED_STRUCT;
```
*/

#if defined(_MSC_VER)
#define P0SE_PACKED_STRUCT __pragma(pack(push, 1)) struct
#define P0SE_END_PACKED_STRUCT __pragma(pack(pop))
#else
#define P0SE_PACKED_STRUCT struct __attribute__((packed))
#define P0SE_END_PACKED_STRUCT
#endif
