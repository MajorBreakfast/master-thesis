#include "p0se/utils/transform-from-xi.h"

namespace p0se {

Eigen::Matrix4f transformFromXi(const Sophus::Vector6f& xi,
                                const Eigen::Matrix3f& cameraMatrix) {
  const Eigen::Matrix3f& K = cameraMatrix;
  Sophus::SE3f T = Sophus::SE3f::exp(xi);

  Eigen::Matrix4f transform;
  transform << K * T.rotationMatrix() * K.inverse(), K * T.translation(),
               0,              0,                 0,                  1;
  return transform;
}

} // End namespace p0se
