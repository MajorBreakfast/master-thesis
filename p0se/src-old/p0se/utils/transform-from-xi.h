#pragma once

#include <Eigen/Core>
#include <sophus/se3.hpp>

namespace p0se {

/**
The reporjection can be almost done entirely using an affine transform. More
details on this in the background chapter of the thesis.
*/
Eigen::Matrix4f transformFromXi(const Sophus::Vector6f& xi,
                                const Eigen::Matrix3f& cameraMatrix);

} // End namespace p0se
