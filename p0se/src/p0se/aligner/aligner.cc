#include <limits>
#include <chrono>
#include "p0se/aligner/aligner.h"
#include "p0se/images/create-device-image-pyramid.h"
#include "p0se/utils/concat.h"
#include "p0se/aligner/downsample-camera-matrix.h"

namespace p0se {

void FillNan(const cl::CommandQueue& command_queue,
             const DeviceImage& device_image) {
  float pattern[] = { NAN };
  clEnqueueFillBuffer(
    command_queue(),
    device_image.GetBuffer()(),
    pattern,
    sizeof(float),
    0,
    device_image.GetNumBytes(),
    0,
    nullptr,
    nullptr);
}

Aligner::Aligner(cl::Context context,
                 const int32_t width,
                 const int32_t height,
                 const int32_t num_levels,
                 const std::string& mode)
  : context_(context),
    width_(width),
    height_(height),
    num_levels_(num_levels),
    downsampler_depth_(context),
    downsampler_rgb_(context),
    alignment_numeric_(context, mode),
    compute_residual_histogram_(context,
                                Histogram(-0.2f, 512, 0.4f / 512.0f),
                                Histogram(-256.0f, 512, 512.0f / 512.0f)),
    compute_residual_scale_mad_(context) {
  // Create device image pyramids
  pre_depth_device_images_ =
      CreateDeviceImagePyramid(context, width, height, Image::USHORT, num_levels);
  pre_rgb_device_images_ =
      CreateDeviceImagePyramid(context, width, height, Image::UCHAR3, num_levels);
  cur_depth_device_images_ =
      CreateDeviceImagePyramid(context, width, height, Image::USHORT, num_levels);
  cur_rgb_device_images_ =
      CreateDeviceImagePyramid(context, width, height, Image::UCHAR3, num_levels);
  debug_device_images0_ =
      CreateDeviceImagePyramid(context, width, height, Image::FLOAT, num_levels);
  debug_device_images1_ =
      CreateDeviceImagePyramid(context, width, height, Image::FLOAT, num_levels);
  debug_device_images2_ =
      CreateDeviceImagePyramid(context, width, height, Image::FLOAT, num_levels);
  debug_device_images3_ =
      CreateDeviceImagePyramid(context, width, height, Image::FLOAT, num_levels);
  debug_device_images4_ =
      CreateDeviceImagePyramid(context, width, height, Image::FLOAT, num_levels);
  debug_device_images5_ =
      CreateDeviceImagePyramid(context, width, height, Image::FLOAT, num_levels);

  debug_device_images0_[0].AllocBuffer(context);
  debug_device_images1_[0].AllocBuffer(context);
  debug_device_images2_[0].AllocBuffer(context);
  debug_device_images3_[0].AllocBuffer(context);
  debug_device_images4_[0].AllocBuffer(context);
  debug_device_images5_[0].AllocBuffer(context);

  for (size_t i = 1; i < num_levels; ++i) {
    pre_depth_device_images_[i].AllocBuffer(context);
    pre_rgb_device_images_[i].AllocBuffer(context);
    cur_depth_device_images_[i].AllocBuffer(context);
    cur_rgb_device_images_[i].AllocBuffer(context);

    debug_device_images0_[i].SetBuffer(
        debug_device_images0_[0].GetBuffer()); // Reuse in all levels
    debug_device_images1_[i].SetBuffer(
        debug_device_images1_[0].GetBuffer()); // Reuse in all levels
    debug_device_images2_[i].SetBuffer(
        debug_device_images2_[0].GetBuffer()); // Reuse in all levels
    debug_device_images3_[i].SetBuffer(
        debug_device_images3_[0].GetBuffer()); // Reuse in all levels
    debug_device_images4_[i].SetBuffer(
        debug_device_images4_[0].GetBuffer()); // Reuse in all levels
    debug_device_images5_[i].SetBuffer(
        debug_device_images5_[0].GetBuffer()); // Reuse in all levels
  }

  // Build log
  build_log_ += downsampler_depth_.GetBuildLog();
  build_log_ += downsampler_rgb_.GetBuildLog();
  build_log_ += alignment_numeric_.GetBuildLog();
  build_log_ += compute_residual_histogram_.GetBuildLog();
  build_log_ += compute_residual_scale_mad_.GetBuildLog();
}

void Aligner::Run(cl::CommandQueue command_queue,
                  const DeviceImage& pre_depth_device_image,
                  const DeviceImage& pre_rgb_device_image,
                  const DeviceImage& cur_depth_device_image,
                  const DeviceImage& cur_rgb_device_image,
                  const Eigen::Matrix3f& camera_matrix,
                  const float depth_scale_factor,
                  const int32_t num_max_iterations_per_level,
                  const bool allow_early_break,
                  const Sophus::SE3f& initial_motion,
                  Sophus::SE3f* motion,
                  Stats* stats,
                  Logger* logger) {
  std::chrono::steady_clock::time_point start_time = std::chrono::steady_clock::now();

  // Use provided buffers as level 0 in the image pyramid
  pre_depth_device_images_[0].SetBuffer(pre_depth_device_image);
  pre_rgb_device_images_[0].SetBuffer(pre_rgb_device_image);
  cur_depth_device_images_[0].SetBuffer(cur_depth_device_image);
  cur_rgb_device_images_[0].SetBuffer(cur_rgb_device_image);

  if (logger) {
    logger->LogString("# Pyramid");

    logger->LogDeviceImage(Concat("l0-pre-depth"),
                           command_queue, pre_depth_device_images_[0],
                           ImageLogMessage::DEPTH);
    logger->LogDeviceImage(Concat("l0-pre-rgb"),
                           command_queue, pre_rgb_device_images_[0],
                           ImageLogMessage::RGB);
    logger->LogDeviceImage(Concat("l0-cur-depth"),
                           command_queue, cur_depth_device_images_[0],
                           ImageLogMessage::DEPTH);
    logger->LogDeviceImage(Concat("l0-cur-rgb"),
                           command_queue, cur_rgb_device_images_[0],
                           ImageLogMessage::RGB);
  }

  for (int32_t l = 1; l < num_levels_; ++l) { // Pyramid level loop
    downsampler_depth_.Enqueue( // pre depth
        command_queue,
        pre_depth_device_images_[l-1], // Src (fine)
        &pre_depth_device_images_[l]); // Dst (coarse)

    downsampler_rgb_.Enqueue( // pre rgb
        command_queue,
        pre_rgb_device_images_[l-1], // Src (fine)
        &pre_rgb_device_images_[l]); // Dst (coarse)

    downsampler_depth_.Enqueue( // cur depth
        command_queue,
        cur_depth_device_images_[l-1], // Src (fine)
        &cur_depth_device_images_[l]); // Dst (coarse)

    downsampler_rgb_.Enqueue( // cur rgb
        command_queue,
        cur_rgb_device_images_[l-1], // Src (fine)
        &cur_rgb_device_images_[l]); // Dst (coarse)

    if (logger) {
      logger->LogDeviceImage(Concat("l", l, "-pre-depth"),
                             command_queue, pre_depth_device_images_[l],
                             ImageLogMessage::DEPTH);
      logger->LogDeviceImage(Concat("l", l, "-pre-rgb"),
                             command_queue, pre_rgb_device_images_[l],
                             ImageLogMessage::RGB);
      logger->LogDeviceImage(Concat("l", l, "-cur-depth"),
                             command_queue, cur_depth_device_images_[l],
                             ImageLogMessage::DEPTH);
      logger->LogDeviceImage(Concat("l", l, "-cur-rgb"),
                             command_queue, cur_rgb_device_images_[l],
                             ImageLogMessage::RGB);
    }
  } // End of pyramid level loop

  // Optimization

  if (logger) { logger->LogString("# Optimization"); }
  Sophus::SE3f current_motion_estimate = initial_motion;

  if (stats) {
    stats->level_stats.resize(num_levels_);
  }

  // Optimization level loop
  for (int32_t l = num_levels_ - 1; l >= 0; --l) {
    std::chrono::steady_clock::time_point level_start_time =
        std::chrono::steady_clock::now();
    if (stats) {
      stats->level_stats[l].num_iterations = 0;
    }

    Eigen::Matrix3f downsampled_camera_matrix =
        DownsampleCameraMatrix(camera_matrix, l);

    float previous_error = std::numeric_limits<float>::infinity();
    // Optimization iteration loop
    for (int32_t i = 0; i < num_max_iterations_per_level; ++i) {
      if (stats) {
        stats->level_stats[l].num_iterations = i;
      }
      if (logger) {
        logger->LogString(Concat("## Level", l, " Iteration", i));
      }
      if (logger) {
        compute_residual_histogram_.Run(
            command_queue,
            pre_depth_device_images_[l],
            pre_rgb_device_images_[l],
            cur_depth_device_images_[l],
            cur_rgb_device_images_[l],
            depth_scale_factor,
            current_motion_estimate,
            downsampled_camera_matrix);

        logger->LogHistogram(Concat("l", l, "-i", i, "-geometric-histogram"),
          compute_residual_histogram_.GetGeometricHistogram());

        logger->LogHistogram(Concat("l", l, "-i", i, "-photometric-histogram"),
          compute_residual_histogram_.GetPhotometricHistogram());
      }

      if (false && logger) { // Fill debug buffers with NAN
        FillNan(command_queue, debug_device_images0_[l]);
        FillNan(command_queue, debug_device_images1_[l]);
        FillNan(command_queue, debug_device_images2_[l]);
        FillNan(command_queue, debug_device_images3_[l]);
        FillNan(command_queue, debug_device_images4_[l]);
        FillNan(command_queue, debug_device_images5_[l]);
      }

      float geometric_residual_scale = 0.006f;
      float photometric_residual_scale = 4.3f;
      // Note: The scale estimation is not really needed
      /*compute_residual_scale_mad_.Run(
          command_queue,
          pre_depth_device_images_[l],
          pre_rgb_device_images_[l],
          cur_depth_device_images_[l],
          cur_rgb_device_images_[l],
          depth_scale_factor,
          current_motion_estimate,
          downsampled_camera_matrix,
          &geometric_residual_scale,
          &photometric_residual_scale);

      if (logger) {
        logger->LogString(Concat("Geometric residual scale",
                                 geometric_residual_scale));
        logger->LogString(Concat("Photometric residual scale",
                                photometric_residual_scale));
      }*/

      Sophus::SE3f updated_motion_estimate;
      float error;
      alignment_numeric_.Run(
        command_queue,
        pre_depth_device_images_[l],
        pre_rgb_device_images_[l],
        cur_depth_device_images_[l],
        cur_rgb_device_images_[l],
        depth_scale_factor,
        current_motion_estimate,
        downsampled_camera_matrix,
        geometric_residual_scale,
        photometric_residual_scale,
        UNIT,
        HUBER,
        &updated_motion_estimate,
        &error,
        &debug_device_images0_[l],
        &debug_device_images1_[l],
        &debug_device_images2_[l],
        &debug_device_images3_[l],
        &debug_device_images4_[l],
        &debug_device_images5_[l]);

      if (logger) {
        logger->LogString(Concat("Error ", error));
        logger->LogDeviceImage(Concat("l", l, "-i", i, "-debug0"),
                               command_queue, debug_device_images0_[l],
                               ImageLogMessage::RESIDUAL);
        logger->LogDeviceImage(Concat("l", l, "-i", i, "-debug1"),
                               command_queue, debug_device_images1_[l],
                               ImageLogMessage::RESIDUAL);
        logger->LogDeviceImage(Concat("l", l, "-i", i, "-debug2"),
                               command_queue, debug_device_images2_[l],
                               ImageLogMessage::JACOBIAN);
        logger->LogDeviceImage(Concat("l", l, "-i", i, "-debug3"),
                               command_queue, debug_device_images3_[l],
                               ImageLogMessage::RESIDUAL);
        logger->LogDeviceImage(Concat("l", l, "-i", i, "-debug4"),
                               command_queue, debug_device_images4_[l],
                               ImageLogMessage::RESIDUAL);
        logger->LogDeviceImage(Concat("l", l, "-i", i, "-debug5"),
                               command_queue, debug_device_images5_[l],
                               ImageLogMessage::RESIDUAL);
      }

      if (allow_early_break && previous_error < error) {
        break;
      } else {
        current_motion_estimate = updated_motion_estimate;
        previous_error = error;
      }

      if (stats) {
        std::chrono::steady_clock::time_point level_end_time =
            std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> duration =
            level_end_time - level_start_time;
        stats->level_stats[l].duration = duration.count();
      }
    } // End of optimization iteration loop

    // Convert motion from pre -> cur to cur -> pre by inverting it
    *motion = current_motion_estimate.inverse();

    if (stats) {
      std::chrono::steady_clock::time_point end_time =
          std::chrono::steady_clock::now();
      std::chrono::duration<double, std::milli> duration = end_time - start_time;
      stats->duration = duration.count();
    }
  } // End of optimization level loop
}

} // End of namespace p0se
