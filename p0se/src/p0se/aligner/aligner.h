#pragma once

#include <cinttypes>
#include <vector>
#include <string>
#include <CL/cl2.hpp>
#include <Eigen/Core>
#include <sophus/se3.hpp>
#include "p0se/images/device-image.h"
#include "p0se/kernel-functions/downsampler-depth/downsampler-depth.h"
#include "p0se/kernel-functions/downsampler-rgb/downsampler-rgb.h"
#include "p0se/kernel-functions/alignment-numeric/alignment-numeric.h"
#include "p0se/kernel-functions/compute-residual-histogram/compute-residual-histogram.h"
#include "p0se/kernel-functions/compute-residual-scale-mad/compute-residual-scale-mad.h"
#include "p0se/logger/logger.h"

namespace p0se {

class Aligner {
 public:
  Aligner(cl::Context context,
          const int32_t width,
          const int32_t height,
          const int32_t num_levels,
          const std::string& mode = "MODE_PHOTOMETRIC_HUBER");

  struct LevelStats {
    int32_t num_iterations;
    double duration;
  };

  struct Stats {
    double duration;
    std::vector<LevelStats> level_stats;
  };

  void Run(cl::CommandQueue command_queue,
           const DeviceImage& pre_depth_device_image,
           const DeviceImage& pre_rgb_device_image,
           const DeviceImage& cur_depth_device_image,
           const DeviceImage& cur_rgb_device_image,
           const Eigen::Matrix3f& camera_matrix,
           const float depth_scale_factor,
           const int32_t num_max_iterations_per_level,
           const bool allow_early_break,
           const Sophus::SE3f& initial_motion,
           Sophus::SE3f* motion,
           Stats* stats = nullptr,
           Logger* logger = nullptr);

  const std::string& GetBuildLog() const { return build_log_; }
 private:
  cl::Context context_;
  int32_t width_;
  int32_t height_;
  int32_t num_levels_;
  std::string build_log_;

  // Device image pyramids
  std::vector<DeviceImage> pre_depth_device_images_;
  std::vector<DeviceImage> pre_rgb_device_images_;
  std::vector<DeviceImage> cur_depth_device_images_;
  std::vector<DeviceImage> cur_rgb_device_images_;

  // Debug device image pyramids
  std::vector<DeviceImage> debug_device_images0_;
  std::vector<DeviceImage> debug_device_images1_;
  std::vector<DeviceImage> debug_device_images2_;
  std::vector<DeviceImage> debug_device_images3_;
  std::vector<DeviceImage> debug_device_images4_;
  std::vector<DeviceImage> debug_device_images5_;

  // Kernel functions
  DownsamplerDepth downsampler_depth_;
  DownsamplerRgb downsampler_rgb_;
  AlignmentNumeric alignment_numeric_;
  ComputeResidualHistogram compute_residual_histogram_;
  ComputeResidualScaleMad compute_residual_scale_mad_;
};

}
