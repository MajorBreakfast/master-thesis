#pragma once

#include <cinttypes>
#include <Eigen/Core>

namespace p0se {

inline Eigen::Matrix3f DownsampleCameraMatrix(
    const Eigen::Matrix3f& camera_matrix, const int32_t level) {
  Eigen::Matrix3f mat = camera_matrix;
  mat(0, 2) += 0.5; mat(1, 2) += 0.5;
  mat.topLeftCorner(2, 3) *= 1.0f / (1 << level);
  mat(0, 2) -= 0.5; mat(1, 2) -= 0.5;
  return mat;
}

} // End namespace p0se
