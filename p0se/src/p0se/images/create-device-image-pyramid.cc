#include "create-device-image-pyramid.h"

namespace p0se {

std::vector<DeviceImage> CreateDeviceImagePyramid(
    const cl::Context context,
    const int32_t width,
    const int32_t height,
    const Image::DataType data_type,
    const int32_t num_levels) {
  std::vector<DeviceImage> device_images;
  for (int l = 0; l < num_levels; ++l) {
    DeviceImage device_image(width >> l, height >> l, data_type);
    device_images.push_back(std::move(device_image));
  }
  return device_images;
}

} // namespace p0se
