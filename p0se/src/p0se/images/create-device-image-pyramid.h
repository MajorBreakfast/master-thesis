#pragma once

#include <cinttypes>
#include <vector>
#include <CL/cl2.hpp>
#include "p0se/images/device-image.h"

namespace p0se {

/**
Creates a device image pyramid which is a vector of device images with
length `num_levels`. Level zero has the full width and height and each following
level has half the width and height of its respective previous level.
*/
std::vector<DeviceImage> CreateDeviceImagePyramid(
    const cl::Context context,
    const int32_t width,
    const int32_t height,
    const Image::DataType data_type,
    const int32_t num_levels);

} // namespace p0se
