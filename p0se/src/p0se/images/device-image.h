#pragma once

#include "p0se/images/image.h"
#include <CL/cl2.hpp>

namespace p0se {

class DeviceImage : public Image {
 public:
  DeviceImage() : Image() {}
  DeviceImage(int32_t width,
              int32_t height,
              DataType data_type,
              cl::Buffer buffer)
  : Image(width, height, data_type),
    buffer_(buffer) {}

DeviceImage(int32_t width,
            int32_t height,
            DataType data_type)
  : DeviceImage(width, height, data_type, cl::Buffer()) {}

  void AllocBuffer(cl::Context context) {
    buffer_ = cl::Buffer(context, CL_MEM_READ_WRITE, GetNumBytes());
  }

  cl::Buffer GetBuffer () { return buffer_; }
  const cl::Buffer GetBuffer () const { return buffer_; }
  void SetBuffer (cl::Buffer buffer) { buffer_ = buffer; }
  void SetBuffer (const DeviceImage& device_image) {
    if (device_image.GetWidth() != width_ ||
        device_image.GetHeight() != height_ ||
        device_image.GetDataType() != data_type_) {
      throw new std::runtime_error("Width, height and data_type must match");
    }
    buffer_ = device_image.GetBuffer();
  }

  cl_int2 GetSize () const {
    return cl_int2{ static_cast<cl_int>(width_),
                    static_cast<cl_int>(height_) };
  }

  void WriteBuffer(cl::CommandQueue command_queue, void *host_buffer) {
    command_queue.enqueueWriteBuffer(
        buffer_, true, 0, GetNumBytes(), host_buffer);
  }

  void ReadBuffer(cl::CommandQueue command_queue, void *host_buffer) {
    command_queue.enqueueReadBuffer(
        buffer_, true, 0, GetNumBytes(), host_buffer);
  }

 protected:
  cl::Buffer buffer_;
};

} // namespace p0se
