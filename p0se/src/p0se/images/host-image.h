#pragma once

#include "p0se/images/image.h"
#include <memory>

namespace p0se {

/**
The HostImage class stores image data as a std::vector.
This means that it is not a lightweight class like the DeviceImage which just
stores a pointer (cl::Buffer).
*/
class HostImage : public Image {
 public:
  HostImage() : Image() {}
  HostImage(int32_t width,
            int32_t height,
            DataType data_type)
  : Image(width, height, data_type),
    buffer_(GetNumBytes()) {}

  char* GetBuffer () { return buffer_.data(); }
  const char* GetBuffer () const { return buffer_.data(); }

 protected:
  std::vector<char> buffer_;
};

} // namespace p0se
