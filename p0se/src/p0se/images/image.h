#pragma once

#include <cinttypes>
#include <string>

namespace p0se {

class Image {
 public:
  enum DataType { UCHAR3, UCHAR4, USHORT, FLOAT };

  Image() {}
  Image(int32_t width,
        int32_t height,
        DataType data_type)
  : width_(width),
    height_(height),
    data_type_(data_type) {}

  int32_t GetWidth() const { return width_; }
  int32_t GetHeight() const { return height_; }
  DataType GetDataType() const { return data_type_; }

  const std::string GetDataTypeAsString() const {
    switch (data_type_) {
      case UCHAR3: return "uchar3";
      case UCHAR4: return "uchar4";
      case USHORT: return "ushort";
      case FLOAT: return "float";
      default: abort();
    }
  }

  int32_t GetNumPixels() const { return width_ * height_; }

  int32_t GetNumSubpixels() const {
    switch (data_type_) {
      case UCHAR3: return 3 * width_ * height_;
      case UCHAR4: return 4 * width_ * height_;
      case USHORT: return width_ * height_;
      case FLOAT: return width_ * height_;
    }
  }

  int32_t GetNumBytes() const {
    switch (data_type_) {
      case UCHAR3: return 3 * width_ * height_;
      case UCHAR4: return 4 * width_ * height_;
      case USHORT: return 2 * width_ * height_;
      case FLOAT: return 4 * width_ * height_;
      default: return 0;
    }
  }

 protected:
  int32_t width_, height_;
  DataType data_type_;
};

} // namespace p0se
