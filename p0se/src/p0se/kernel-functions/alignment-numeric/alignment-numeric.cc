#include "p0se/kernel-functions/alignment-numeric/alignment-numeric.h"
#include "p0se/kernel-functions/common/numeric-affine-transforms.h"
#include "p0se/utils/concat.h"

namespace p0se {

AlignmentNumeric::AlignmentNumeric(cl::Context context, const std::string& mode)
  : KernelFunction(context) {
  kernel_ = CreateAndBuildKernel("AlignmentNumeric", {
    "p0se/kernel-functions/common/linearize.cl",
    "p0se/kernel-functions/common/affine-transform.cl",
    "p0se/kernel-functions/common/numeric-affine-transforms.cl",
    "p0se/kernel-functions/common/alignment-accumulator.cl",
    "p0se/kernel-functions/common/read-depth.cl",
    "p0se/kernel-functions/common/read-rgb.cl",
    "p0se/kernel-functions/common/robust-weights.cl",
    "p0se/kernel-functions/common/alignment-modes.cl",
    Concat("#define MODE ", mode),
    "p0se/kernel-functions/alignment-numeric/alignment-numeric.cl"
  });

  cl::Device device = context.getInfo<CL_CONTEXT_DEVICES>()[0];
  int32_t num_compute_units = device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();

  num_workgroups_ = num_compute_units * 8;

  // Allocate memory
  accumulators_.resize(num_workgroups_);
  accumulators_buffer_ = cl::Buffer(
      context,
      CL_MEM_READ_WRITE,
      accumulators_.size() * sizeof(AlignmentAccumulator));
}

void AlignmentNumeric::Run(
    cl::CommandQueue command_queue,
    const DeviceImage& pre_depth_image,
    const DeviceImage& pre_rgb_image,
    const DeviceImage& cur_depth_image,
    const DeviceImage& cur_rgb_image,
    const float depth_scale_factor,
    const Sophus::SE3f current_motion_estimate,
    const Eigen::Matrix3f camera_matrix,
    float geometric_residual_scale,
    float photometric_residual_scale,
    RobustWeightType geometric_weight_type,
    RobustWeightType photometric_weight_type,
    Sophus::SE3f* updated_motion_estimate,
    float* error,
    DeviceImage* debug_image0,
    DeviceImage* debug_image1,
    DeviceImage* debug_image2,
    DeviceImage* debug_image3,
    DeviceImage* debug_image4,
    DeviceImage* debug_image5) {
  int32_t workgroup_size = 128;
  float eps = 0.000001f;

  SetKernelArgs(
      pre_depth_image.GetBuffer(),
      pre_rgb_image.GetBuffer(),
      cur_depth_image.GetBuffer(),
      cur_rgb_image.GetBuffer(),
      pre_depth_image.GetSize(),
      depth_scale_factor,
      NumericAffineTransforms::CreateForReprojection(
          current_motion_estimate, camera_matrix, eps),
      1.0f / geometric_residual_scale,
      1.0f / photometric_residual_scale,
      geometric_weight_type,
      photometric_weight_type,
      LocalBuffer(workgroup_size * sizeof(AlignmentAccumulator)),
      accumulators_buffer_,
      debug_image0->GetBuffer(),
      debug_image1->GetBuffer(),
      debug_image2->GetBuffer(),
      debug_image3->GetBuffer(),
      debug_image4->GetBuffer(),
      debug_image5->GetBuffer());

  command_queue.enqueueNDRangeKernel(
      kernel_,
      cl::NullRange,
      cl::NDRange(num_workgroups_ * workgroup_size),
      cl::NDRange(workgroup_size));

  command_queue.enqueueReadBuffer(
      accumulators_buffer_, true,
      0, accumulators_.size() * sizeof(AlignmentAccumulator),
      accumulators_.data());

  AlignmentAccumulator acc = {};
  for (auto& acc2 : accumulators_) { acc += acc2; }

  Sophus::SE3f delta_motion = Sophus::SE3f::exp(acc.a().ldlt().solve(acc.b()));
  *updated_motion_estimate = current_motion_estimate * delta_motion;
  *error = acc.error / acc.weight;
}

} // End of namespace p0se
