// Requires common/linearize.cl
// Requires common/affine-transform.cl
// Requires common/numeric-affine-transforms.cl
// Requires common/alignment-accumulator.cl
// Requires common/read-depth.cl
// Requires common/read-rgb.cl
// Requires common/robust-weights.cl
// Requires common/alignment-modes.cl

#undef MODE
#define MODE MODE_PHOTOMETRIC_HUBER_TIMES_TUKEY_GEOMETRIC

#define COMPUTE_GEOMETRIC_RESIDUAL (MODE == MODE_GEOMETRIC || \
                                    MODE == MODE_GEOMETRIC_HUBER || \
                                    MODE == MODE_GEOMETRIC_TUKEY || \
                                    MODE == MODE_PHOTOMETRIC_HUBER_GEOMETRIC || \
                                    MODE == MODE_PHOTOMETRIC_TUKEY_GEOMETRIC || \
                                    MODE == MODE_PHOTOMETRIC_HUBER_TIMES_HUBER_GEOMETRIC || \
                                    MODE == MODE_PHOTOMETRIC_HUBER_TIMES_TUKEY_GEOMETRIC)
#define COMPUTE_GEOMETRIC_JACOBIAN (MODE == MODE_GEOMETRIC || \
                                    MODE == MODE_GEOMETRIC_HUBER || \
                                    MODE == MODE_GEOMETRIC_TUKEY)
#define USE_GEOMETRIC_JACOBIAN COMPUTE_GEOMETRIC_JACOBIAN

#define COMPUTE_PHOTOMETRIC_RESIDUAL (MODE == MODE_PHOTOMETRIC || \
                                      MODE == MODE_PHOTOMETRIC_HUBER || \
                                      MODE == MODE_PHOTOMETRIC_TUKEY || \
                                      MODE == MODE_PHOTOMETRIC_HUBER_GEOMETRIC || \
                                      MODE == MODE_PHOTOMETRIC_TUKEY_GEOMETRIC || \
                                      MODE == MODE_PHOTOMETRIC_HUBER_TIMES_HUBER_GEOMETRIC || \
                                      MODE == MODE_PHOTOMETRIC_HUBER_TIMES_TUKEY_GEOMETRIC)
#define COMPUTE_PHOTOMETRIC_JACOBIAN (MODE == MODE_PHOTOMETRIC || \
                                      MODE == MODE_PHOTOMETRIC_HUBER || \
                                      MODE == MODE_PHOTOMETRIC_TUKEY || \
                                      MODE == MODE_PHOTOMETRIC_HUBER_GEOMETRIC ||  \
                                      MODE == MODE_PHOTOMETRIC_TUKEY_GEOMETRIC || \
                                      MODE == MODE_PHOTOMETRIC_HUBER_TIMES_HUBER_GEOMETRIC || \
                                      MODE == MODE_PHOTOMETRIC_HUBER_TIMES_TUKEY_GEOMETRIC)
#define USE_PHOTOMETRIC_JACOBIAN COMPUTE_PHOTOMETRIC_JACOBIAN

kernel void AlignmentNumeric(
    const global ushort* pre_depth_buf,
    const global uchar* pre_rgb_buf,
    const global ushort* cur_depth_buf,
    const global uchar* cur_rgb_buf,
    const int2 size,
    const float depth_scale_factor,
    const NumericAffineTransforms transforms, // cur <-- K T K^-1 <-- pre
    const float geometric_residual_inv_scale,
    const float photometric_residual_inv_scale,
    const RobustWeightType geometric_weight_type,
    const RobustWeightType photometric_weight_type,
    local AlignmentAccumulator* local_accumulator_buf,
    global AlignmentAccumulator* global_accumulator_buf,
    global float* debug_buf0,
    global float* debug_buf1,
    global float* debug_buf2,
    global float* debug_buf3,
    global float* debug_buf4,
    global float* debug_buf5) {
  local AlignmentAccumulator* acc = local_accumulator_buf + get_local_id(0);
  ResetAlignmentAccumulator(acc);

  int num_pixels = size.x * size.y;

  // Loop over pixels:
  // Accumulate each pixel's contribution to A = J' * J and b = J' * r
  for (int i = get_global_id(0); i < num_pixels; i += get_global_size(0)) {
    int2 pos = Delinearize(i, size.x);

    // Reprojection
    float pre_depth = ReadDepth(pos, size, depth_scale_factor, pre_depth_buf);
    float2 pre_point2d = convert_float2(pos);
    float4 pre_point3dh = (float4)(pre_point2d * pre_depth, pre_depth, 1);
    float4 pre_warped_point3dh =
        MultiplyAffineTransform3fAndVector(transforms.orig, pre_point3dh);
    float2 cur_point2d = pre_warped_point3dh.xy / pre_warped_point3dh.z;

    // Geometric resiudal
    #if COMPUTE_GEOMETRIC_RESIDUAL
    float cur_depth = ReadDepthInterp(cur_point2d, size, depth_scale_factor,
                                      cur_depth_buf);
    float geometric_residual = (pre_warped_point3dh.z - cur_depth)
                               * geometric_residual_inv_scale;
    debug_buf0[i] = geometric_residual;
    #endif

    // Photometric resiudal
    #if COMPUTE_PHOTOMETRIC_RESIDUAL
    float3 pre_rgb = ReadRgb(pos, size, pre_rgb_buf);
    float3 cur_rgb = ReadRgbInterp(cur_point2d, size, cur_rgb_buf);
    float photometric_residual = (length(pre_rgb) - length(cur_rgb))
                                 * photometric_residual_inv_scale;
    debug_buf1[i] = photometric_residual;
    #endif

    // Compute jacobian row
    float geometric_jacobian_row[6];
    float photometric_jacobian_row[6];
    #pragma unroll
    for (int d = 0; d < 6; ++d) { // Loop over 6 degrees of freedom
      // Reprojection
      float4 pre_warped_point3dh_perm =
          MultiplyAffineTransform3fAndVector(transforms.perm[d], pre_point3dh);
      float2 cur_point2d_perm =
          pre_warped_point3dh_perm.xy / pre_warped_point3dh_perm.z;

      // Geometric residual & jacobian approximation
      #if COMPUTE_GEOMETRIC_JACOBIAN
      float cur_depth_perm = ReadDepthInterp(cur_point2d_perm, size,
                                             depth_scale_factor, cur_depth_buf);
      float geometric_residual_perm = (pre_warped_point3dh_perm.z - cur_depth_perm)
                                      * geometric_residual_inv_scale;
      geometric_jacobian_row[d] = (geometric_residual_perm - geometric_residual)
                                  * transforms.inv_eps;
      #endif

      // Photometric residual & jacobian approximation
      #if COMPUTE_PHOTOMETRIC_JACOBIAN
      float3 cur_rgb_perm = ReadRgbInterp(cur_point2d_perm, size, cur_rgb_buf);
      float photometric_residual_perm = (length(pre_rgb) - length(cur_rgb_perm))
                                        * photometric_residual_inv_scale;
      photometric_jacobian_row[d] = (photometric_residual_perm -
                                     photometric_residual) * transforms.inv_eps;
      debug_buf2[i] = photometric_jacobian_row[0];
      #endif
    } // End of loop over 6 degrees of freedom

    // Add geometric jacobian row to the accumulator
    #if USE_GEOMETRIC_JACOBIAN
    if (isfinite(geometric_jacobian_row[0]) &&
        isfinite(geometric_jacobian_row[1]) &&
        isfinite(geometric_jacobian_row[2]) &&
        isfinite(geometric_jacobian_row[3]) &&
        isfinite(geometric_jacobian_row[4]) &&
        isfinite(geometric_jacobian_row[5])) {
      #if MODE == MODE_GEOMETRIC
      float weight = 1.0f;
      #endif

      #if MODE == MODE_GEOMETRIC_HUBER
      float weight = ComputeHuberWeight(geometric_residual);
      #endif

      #if MODE == MODE_GEOMETRIC_TUKEY
      float weight = ComputeTukeyWeight(geometric_residual);
      #endif

      AddRowToAlignmentAccumulator(geometric_residual,
                                   geometric_jacobian_row, weight, acc);
    }
    #endif

    // Add photometric jacobian row to the accumulator
    #if USE_PHOTOMETRIC_JACOBIAN
    if (isfinite(photometric_jacobian_row[0]) &&
        isfinite(photometric_jacobian_row[1]) &&
        isfinite(photometric_jacobian_row[2]) &&
        isfinite(photometric_jacobian_row[3]) &&
        isfinite(photometric_jacobian_row[4]) &&
        isfinite(photometric_jacobian_row[5])) {
      #if MODE == MODE_PHOTOMETRIC
      float weight = 1.0f;
      #endif

      #if MODE == MODE_PHOTOMETRIC_HUBER
      float weight = ComputeHuberWeight(photometric_residual);
      #endif

      #if MODE == MODE_PHOTOMETRIC_TUKEY
      float weight = ComputeTukeyWeight(photometric_residual);
      #endif

      #if COMPUTE_GEOMETRIC_RESIDUAL
      if (isfinite(geometric_residual)) {
      #endif

        #if MODE == MODE_PHOTOMETRIC_HUBER_GEOMETRIC
        float weight = ComputeHuberWeight(geometric_residual);
        #endif

        #if MODE == MODE_PHOTOMETRIC_TUKEY_GEOMETRIC
        float weight = ComputeTukeyWeight(geometric_residual);
        #endif

        #if MODE == MODE_PHOTOMETRIC_HUBER_TIMES_HUBER_GEOMETRIC
        float weight = ComputeHuberWeight(photometric_residual) *
                       ComputeHuberWeight(geometric_residual);
        #endif

        #if MODE == MODE_PHOTOMETRIC_HUBER_TIMES_TUKEY_GEOMETRIC
        float weight = ComputeHuberWeight(photometric_residual) *
                       ComputeTukeyWeight(geometric_residual);
        #endif

        AddRowToAlignmentAccumulator(photometric_residual,
                                     photometric_jacobian_row, weight, acc);

      #if COMPUTE_GEOMETRIC_RESIDUAL
      }
      #endif
    }
    #endif
  } // End of loop over pixels

  // Perform reduction and store accumulator in global buffer
  StoreSumOfWorkgroupAlignmentAccumulators(local_accumulator_buf,
                                           global_accumulator_buf);
}
