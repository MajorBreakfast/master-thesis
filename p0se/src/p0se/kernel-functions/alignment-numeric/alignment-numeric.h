#pragma once

#include "p0se/kernel-functions/kernel-function.h"
#include <cinttypes>
#include <string>
#include <Eigen/Core>
#include <sophus/se3.hpp>
#include "p0se/images/device-image.h"
#include "p0se/kernel-functions/common/alignment-accumulator.h"
#include "p0se/kernel-functions/common/robust-weights.h"

namespace p0se {

class AlignmentNumeric : public KernelFunction {
 public:
  AlignmentNumeric(cl::Context context, const std::string& mode);

  void Run(cl::CommandQueue command_queue,
           const DeviceImage& pre_depth_image,
           const DeviceImage& pre_rgb_image,
           const DeviceImage& cur_depth_image,
           const DeviceImage& cur_rgb_image,
           const float depth_scale_factor,
           const Sophus::SE3f current_motion_estimate,
           const Eigen::Matrix3f camera_matrix,
           float geometric_residual_scale,
           float photometric_residual_scale,
           RobustWeightType geometric_weight_type,
           RobustWeightType photometric_weight_type,
           Sophus::SE3f* updated_motion_estimate,
           float* error,
           DeviceImage* debug_image0,
           DeviceImage* debug_image1,
           DeviceImage* debug_image2,
           DeviceImage* debug_image3,
           DeviceImage* debug_image4,
           DeviceImage* debug_image5);

 private:
  int32_t num_workgroups_;
  cl::Buffer accumulators_buffer_;
  std::vector<AlignmentAccumulator> accumulators_;
};

} // End of namespace p0se
