typedef struct {
  float4 row0, row1, row2;
} AffineTransform3f;

inline float4 MultiplyAffineTransform3fAndVector(const AffineTransform3f trans,
                                                 const float4 vec) {
  return (float4)(dot(trans.row0, vec),
                  dot(trans.row1, vec),
                  dot(trans.row2, vec),
                  vec.s3);
}
