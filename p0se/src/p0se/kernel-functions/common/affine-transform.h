#pragma once

#include <CL/cl2.hpp>
#include <Eigen/Core>
#include <sophus/se3.hpp>

namespace p0se {

struct AffineTransform3f {
  cl_float4 row0, row1, row2;

  AffineTransform3f() {}
  AffineTransform3f(const Eigen::Matrix4f& mat) {
    row0 = { mat(0, 0), mat(0, 1), mat(0, 2), mat(0, 3) };
    row1 = { mat(1, 0), mat(1, 1), mat(1, 2), mat(1, 3) };
    row2 = { mat(2, 0), mat(2, 1), mat(2, 2), mat(2, 3) };
  }

  /**
  Does backprojection, tranformation and partly the reprojection. Only the
  depth devision is not included, because that part of the formula is
  non-linear and cannot be represented by a matrix.
  */
  static AffineTransform3f CreateForReprojection(
      const Sophus::SE3f& motion,
      const Eigen::Matrix3f& camera_matrix) {
    const Eigen::Matrix3f& K = camera_matrix;
    const Eigen::Matrix3f& R  = motion.rotationMatrix();
    const Eigen::Vector3f& t  = motion.translation();

    Eigen::Matrix4f mat;
    mat << K * R * K.inverse(), K * t,
           0,       0,       0,     1;

    return AffineTransform3f(mat);
  }
};

} // End of namespace p0se
