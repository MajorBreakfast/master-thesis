#include "p0se/kernel-functions/common/alignment-accumulator.h"

#include <algorithm>

namespace p0se {

AlignmentAccumulator& AlignmentAccumulator::operator+=(
    const AlignmentAccumulator& acc2) {
  a00 += acc2.a00;
  a10 += acc2.a10;
  a11 += acc2.a11;
  a20 += acc2.a20;
  a21 += acc2.a21;
  a22 += acc2.a22;
  a30 += acc2.a30;
  a31 += acc2.a31;
  a32 += acc2.a32;
  a33 += acc2.a33;
  a40 += acc2.a40;
  a41 += acc2.a41;
  a42 += acc2.a42;
  a43 += acc2.a43;
  a44 += acc2.a44;
  a50 += acc2.a50;
  a51 += acc2.a51;
  a52 += acc2.a52;
  a53 += acc2.a53;
  a54 += acc2.a54;
  a55 += acc2.a55;
  b0 += acc2.b0;
  b1 += acc2.b1;
  b2 += acc2.b2;
  b3 += acc2.b3;
  b4 += acc2.b4;
  b5 += acc2.b5;
  error += acc2.error;
  weight += acc2.weight;
  num_valid_entries += acc2.num_valid_entries;
  return *this;
}


Eigen::Matrix<float, 6, 6> AlignmentAccumulator::a() {
  Eigen::Matrix<float, 6, 6> a;
  a << a00, a10, a20, a30, a40, a50,
       a10, a11, a21, a31, a41, a51,
       a20, a21, a22, a32, a42, a52,
       a30, a31, a32, a33, a43, a53,
       a40, a41, a42, a43, a44, a54,
       a50, a51, a52, a53, a54, a55;
  return a;
}

Eigen::Matrix<float, 6, 1> AlignmentAccumulator::b() {
  Eigen::Matrix<float, 6, 1> b;
  b << b0, b1, b2, b3, b4, b5;
  return b;
}

} // End of namespace p0se
