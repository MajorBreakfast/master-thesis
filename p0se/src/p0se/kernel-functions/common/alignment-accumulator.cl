typedef struct __attribute__((packed)) {
  // J' * J (symmetric matrix)
  float a00;
  float a10, a11;
  float a20, a21, a22;
  float a30, a31, a32, a33;
  float a40, a41, a42, a43, a44;
  float a50, a51, a52, a53, a54, a55;

  // -J' * r
  float b0;
  float b1;
  float b2;
  float b3;
  float b4;
  float b5;

  float error;
  float weight;
  int num_valid_entries;
} AlignmentAccumulator;

void ResetAlignmentAccumulator(local AlignmentAccumulator* pAcc) {
  pAcc->a00 = 0.0f;
  pAcc->a10 = 0.0f;
  pAcc->a11 = 0.0f;
  pAcc->a20 = 0.0f;
  pAcc->a21 = 0.0f;
  pAcc->a22 = 0.0f;
  pAcc->a30 = 0.0f;
  pAcc->a31 = 0.0f;
  pAcc->a32 = 0.0f;
  pAcc->a33 = 0.0f;
  pAcc->a40 = 0.0f;
  pAcc->a41 = 0.0f;
  pAcc->a42 = 0.0f;
  pAcc->a43 = 0.0f;
  pAcc->a44 = 0.0f;
  pAcc->a50 = 0.0f;
  pAcc->a51 = 0.0f;
  pAcc->a52 = 0.0f;
  pAcc->a53 = 0.0f;
  pAcc->a54 = 0.0f;
  pAcc->a55 = 0.0f;
  pAcc->b0 = 0.0f;
  pAcc->b1 = 0.0f;
  pAcc->b2 = 0.0f;
  pAcc->b3 = 0.0f;
  pAcc->b4 = 0.0f;
  pAcc->b5 = 0.0f;
  pAcc->error = 0.0f;
  pAcc->weight = 0.0f;
  pAcc->num_valid_entries = 0;
}

void AddRowToAlignmentAccumulator(const float residual,
                         const float jacobianRow[],
                         const float weight,
                         local AlignmentAccumulator* pAcc) {
  float weighted_residual = residual * weight;
  pAcc->a00 += jacobianRow[0] * jacobianRow[0] * weight;
  pAcc->a10 += jacobianRow[1] * jacobianRow[0] * weight;
  pAcc->a11 += jacobianRow[1] * jacobianRow[1] * weight;
  pAcc->a20 += jacobianRow[2] * jacobianRow[0] * weight;
  pAcc->a21 += jacobianRow[2] * jacobianRow[1] * weight;
  pAcc->a22 += jacobianRow[2] * jacobianRow[2] * weight;
  pAcc->a30 += jacobianRow[3] * jacobianRow[0] * weight;
  pAcc->a31 += jacobianRow[3] * jacobianRow[1] * weight;
  pAcc->a32 += jacobianRow[3] * jacobianRow[2] * weight;
  pAcc->a33 += jacobianRow[3] * jacobianRow[3] * weight;
  pAcc->a40 += jacobianRow[4] * jacobianRow[0] * weight;
  pAcc->a41 += jacobianRow[4] * jacobianRow[1] * weight;
  pAcc->a42 += jacobianRow[4] * jacobianRow[2] * weight;
  pAcc->a43 += jacobianRow[4] * jacobianRow[3] * weight;
  pAcc->a44 += jacobianRow[4] * jacobianRow[4] * weight;
  pAcc->a50 += jacobianRow[5] * jacobianRow[0] * weight;
  pAcc->a51 += jacobianRow[5] * jacobianRow[1] * weight;
  pAcc->a52 += jacobianRow[5] * jacobianRow[2] * weight;
  pAcc->a53 += jacobianRow[5] * jacobianRow[3] * weight;
  pAcc->a54 += jacobianRow[5] * jacobianRow[4] * weight;
  pAcc->a55 += jacobianRow[5] * jacobianRow[5] * weight;
  pAcc->b0 -= jacobianRow[0] * weighted_residual;
  pAcc->b1 -= jacobianRow[1] * weighted_residual;
  pAcc->b2 -= jacobianRow[2] * weighted_residual;
  pAcc->b3 -= jacobianRow[3] * weighted_residual;
  pAcc->b4 -= jacobianRow[4] * weighted_residual;
  pAcc->b5 -= jacobianRow[5] * weighted_residual;
  pAcc->error += residual * weighted_residual;
  pAcc->weight += weight;
  pAcc->num_valid_entries += 1;
}

void AddAlignmentAccumulators(const local AlignmentAccumulator* first,
                              local AlignmentAccumulator* second) {
  second->a00 += first->a00;
  second->a10 += first->a10;
  second->a11 += first->a11;
  second->a20 += first->a20;
  second->a21 += first->a21;
  second->a22 += first->a22;
  second->a30 += first->a30;
  second->a31 += first->a31;
  second->a32 += first->a32;
  second->a33 += first->a33;
  second->a40 += first->a40;
  second->a41 += first->a41;
  second->a42 += first->a42;
  second->a43 += first->a43;
  second->a44 += first->a44;
  second->a50 += first->a50;
  second->a51 += first->a51;
  second->a52 += first->a52;
  second->a53 += first->a53;
  second->a54 += first->a54;
  second->a55 += first->a55;
  second->b0 += first->b0;
  second->b1 += first->b1;
  second->b2 += first->b2;
  second->b3 += first->b3;
  second->b4 += first->b4;
  second->b5 += first->b5;
  second->error += first->error;
  second->weight += first->weight;
  second->num_valid_entries += first->num_valid_entries;
}

void StoreSumOfWorkgroupAlignmentAccumulators(
    local AlignmentAccumulator* local_accumulator_buf,
    global AlignmentAccumulator* global_accumulator_buf) {
  barrier(CLK_LOCAL_MEM_FENCE);
  for(int offset = get_local_size(0) / 2; offset > 0; offset = offset / 2) {
    if (get_local_id(0) < offset) {
      AddAlignmentAccumulators(
          local_accumulator_buf + get_local_id(0) + offset,
          local_accumulator_buf + get_local_id(0));
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  if (get_local_id(0) == 0) {
    global_accumulator_buf[get_group_id(0)] = local_accumulator_buf[0];
  }
}
