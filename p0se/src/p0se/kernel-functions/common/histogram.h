#pragma once

#include <vector>
#include <cinttypes>

namespace p0se {

class Histogram {
 private:
  float min_value_;
  float bucket_width_;
  std::vector<int32_t> buckets_;

public:
  Histogram() {}
  Histogram(float min_value, int32_t size, float bucket_width)
    : min_value_(min_value),
      bucket_width_(bucket_width),
      buckets_(size) {}

  float GetMinValue() const { return min_value_; }
  int32_t GetSize() const { return static_cast<int32_t>(buckets_.size());  }
  float GetBucketWidth() const { return bucket_width_; }
  int32_t operator[](size_t index) const { return buckets_[index]; }
  int32_t* GetData() { return buckets_.data();  }
  const int32_t* GetData() const { return buckets_.data(); }
  int32_t GetNumBytes () const { return GetSize() * sizeof(int32_t); }

  float GetMedian () const {
    int32_t num_entries = 0;
    for (size_t i = 0; i < buckets_.size(); ++i) {
      num_entries += buckets_[i];
    }
    int32_t counter = 0;
    int32_t half = num_entries / 2;
    for (size_t i = 0; i < buckets_.size(); ++i) {
      counter += buckets_[i];
      if (counter >= half) {
        return min_value_ + (i + 0.5f) * bucket_width_;
      }
    }
    return NAN; // Should not happen
  }
};

} // End of namespace p0se
