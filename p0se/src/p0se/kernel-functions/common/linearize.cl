inline int Linearize(int2 pos, int ld) { return pos.x + pos.y * ld; }
inline int2 Delinearize(int i, int ld) { return (int2)(i % ld, i / ld); }
