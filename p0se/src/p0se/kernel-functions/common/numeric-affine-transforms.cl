// Requires affine-transform.cl

/* Transforms required to calculate the numeric jacobian */
typedef struct __attribute__((packed)) {
  AffineTransform3f orig;
  AffineTransform3f perm[6];
  float inv_eps;
} NumericAffineTransforms;
