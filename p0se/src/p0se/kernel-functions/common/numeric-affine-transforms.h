#pragma once

#include <CL/cl2.hpp>
#include <Eigen/Core>
#include <sophus/se3.hpp>
#include <p0se/kernel-functions/common/affine-transform.h>
#include <p0se/utils/packed-struct.h>

namespace p0se {

P0SE_PACKED_STRUCT NumericAffineTransforms {
  AffineTransform3f orig;
  AffineTransform3f perm[6];
  cl_float inv_eps;

  static NumericAffineTransforms CreateForReprojection(
      const Sophus::SE3f& motion,
      const Eigen::Matrix3f& camera_matrix,
      const float eps) {
    NumericAffineTransforms ret;
    ret.orig = AffineTransform3f::CreateForReprojection(motion, camera_matrix);
    for (int i = 0; i < 6; ++i) { // Loop over 6 degrees of freedom
      Sophus::Vector6f eps_vec = Sophus::Vector6f::Zero();
      eps_vec(i) = eps;
      ret.perm[i] = AffineTransform3f::CreateForReprojection(
          motion * Sophus::SE3f::exp(eps_vec), camera_matrix);
    } // End of loop over 6 degrees of freedom
    ret.inv_eps = 1.0f / eps;
    return ret;
  }
} P0SE_END_PACKED_STRUCT;

} // End of namespace p0se
