// Requires math.cl

/** Returns depth in meters */
inline float ReadDepth(const int2 pos,
                       const int2 size,
                       const float depth_scale_factor,
                       global const ushort* depth_buf) {
  float depth = depth_buf[Linearize(pos, size.x)] * depth_scale_factor;
  return depth == 0.0f ? NAN : depth;
}

/** Returns interpolated depth depth in meters */
inline float ReadDepthInterp(float2 point2d,
                             int2 size,
                             const float depth_scale_factor,
                             const global ushort* depth_buf) {
  if (point2d.x >= 0 && point2d.x < size.x - 1 &&
      point2d.y >= 0 && point2d.y < size.y - 1) { // Inside?
    int x0 = (int)point2d.x;
    int y0 = (int)point2d.y;

    float lx = (x0 + 1) - point2d.x; // Influence (lambda) of x0
    float ly = (y0 + 1) - point2d.y; // Influence (lambda) of y0

    float v00 = depth_buf[Linearize((int2)(x0    , y0    ), size.x)];
    float v10 = depth_buf[Linearize((int2)(x0 + 1, y0    ), size.x)];
    float v01 = depth_buf[Linearize((int2)(x0    , y0 + 1), size.x)];
    float v11 = depth_buf[Linearize((int2)(x0 + 1, y0 + 1), size.x)];

    if (v00 == 0.0f || v10 == 0.0f || v01 == 0.0f || v11 == 0.0f) {
      return NAN;
    }

    return (v00 * (lx       ) * (ly       ) +
            v10 * (1.0f - lx) * (ly       ) +
            v01 * (lx       ) * (1.0f - ly) +
            v11 * (1.0f - lx) * (1.0f - ly)) * depth_scale_factor;
  } else {
    return NAN;
  }
}
