// Requires math.cl

inline float3 ReadRgb(int2 pos, int2 size, const global uchar* rgb_buf) {
  uchar3 rgb = vload3(Linearize(pos, size.x), rgb_buf);
  return convert_float3(rgb);
}

inline float3 ReadRgbInterp(float2 point2d,
                            int2 size,
                            const global uchar* rgb_buf) {
  if (point2d.x >= 0 && point2d.x < size.x - 1 &&
      point2d.y >= 0 && point2d.y < size.y - 1) { // Inside?
    int x0 = (int)point2d.x;
    int y0 = (int)point2d.y;

    float lx = (x0 + 1) - point2d.x; // Influence (lambda) of x0
    float ly = (y0 + 1) - point2d.y; // Influence (lambda) of y0

    float3 v00 = convert_float3(
        vload3(Linearize((int2)(x0    , y0    ), size.x), rgb_buf));
    float3 v10 = convert_float3(
        vload3(Linearize((int2)(x0 + 1, y0    ), size.x), rgb_buf));
    float3 v01 = convert_float3(
        vload3(Linearize((int2)(x0    , y0 + 1), size.x), rgb_buf));
    float3 v11 = convert_float3(
        vload3(Linearize((int2)(x0 + 1, y0 + 1), size.x), rgb_buf));

    return (v00 * (lx       ) * (ly       ) +
            v10 * (1.0f - lx) * (ly       ) +
            v01 * (lx       ) * (1.0f - ly) +
            v11 * (1.0f - lx) * (1.0f - ly));
  } else {
    return (float3)(NAN, NAN, NAN);
  }
}
