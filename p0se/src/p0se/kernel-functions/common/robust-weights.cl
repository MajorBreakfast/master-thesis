typedef enum {
  UNIT,
  HUBER,
  TUKEY,
} RobustWeightType;

constant float HUBER_K = 1.345f;
inline float ComputeHuberWeight (const float residual) {
  float residual_abs = fabs(residual);
  return (residual_abs <= HUBER_K) ? 1.0f : HUBER_K / residual_abs;
}

constant float TUKEY_INV_B_SQUARED = 1.0f / (4.6851f * 4.6851f);
constant float TUKEY_B_SQUARED = 4.6851f * 4.6851f;
inline float ComputeTukeyWeight (const float residual) {
  float residual_squared = residual * residual;
  if (residual_squared <= TUKEY_B_SQUARED) {
    float tmp = 1.0f - residual_squared * TUKEY_INV_B_SQUARED;
    return tmp * tmp;
  } else {
    return 0.0f;
  }
}
