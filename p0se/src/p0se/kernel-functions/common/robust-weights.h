#pragma once

namespace p0se {

enum RobustWeightType {
  UNIT,
  HUBER,
  TUKEY
};

} // End of namespace p0se
