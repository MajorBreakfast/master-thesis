// Requires common/linearize.cl
// Requires common/affine-transform.cl
// Requires common/read-depth.cl
// Requires common/read-rgb.cl

kernel void ComputeResidualHistogram(
    const global ushort* pre_depth_buf,
    const global uchar* pre_rgb_buf,
    const global ushort* cur_depth_buf,
    const global uchar* cur_rgb_buf,
    const int2 size,
    const float depth_scale_factor,
    const AffineTransform3f transform, // cur <-- K T K^-1 <-- pre

    const float geometric_histogram_min,
    const int geometric_histogram_size,
    const float geometric_histogram_bucket_inv_width,
    local int* local_geometric_histogram,
    global int* global_geometric_histogram,

    const float photometric_histogram_min,
    const int photometric_histogram_size,
    const float photometric_histogram_bucket_inv_width,
    local int* local_photometric_histogram,
    global int* global_photometric_histogram) {
  // Initialize local histograms with zeros
  for (int i = get_local_id(0);
       i < geometric_histogram_size;
       i += get_local_size(0)) {
    local_geometric_histogram[i] = 0;
  }
  for (int i = get_local_id(0);
       i < photometric_histogram_size;
       i += get_local_size(0)) {
    local_photometric_histogram[i] = 0;
  }

  // Iterate over data array and fill local histogram
  int num_pixels = size.x * size.y;
  for (int i = get_global_id(0); i < num_pixels; i += get_global_size(0)) {
    int2 pos = Delinearize(i, size.x);

    float pre_depth = ReadDepth(pos, size, depth_scale_factor, pre_depth_buf);
    float3 pre_rgb = ReadRgb(pos, size, pre_rgb_buf);
    float2 pre_point2d = convert_float2(pos);
    float4 pre_point3dh = (float4)(pre_point2d * pre_depth, pre_depth, 1);
    float4 pre_warped_point3dh =
        MultiplyAffineTransform3fAndVector(transform, pre_point3dh);
    float2 cur_point2d = pre_warped_point3dh.xy / pre_warped_point3dh.z;
    float cur_depth = ReadDepthInterp(cur_point2d,
                                      size,
                                      depth_scale_factor,
                                      cur_depth_buf);
    float3 cur_rgb = ReadRgbInterp(cur_point2d,
                                   size,
                                   cur_rgb_buf);

    float photometric_resiudal = length(pre_rgb) - length(cur_rgb);
    float geometric_resiudal = pre_warped_point3dh.z - cur_depth;

    int geometric_histogram_bucket_id =
        floor((geometric_resiudal - geometric_histogram_min) *
              geometric_histogram_bucket_inv_width);
    if (isfinite(geometric_resiudal) &&
        geometric_histogram_bucket_id >= 0 &&
        geometric_histogram_bucket_id < geometric_histogram_size) {
      atomic_inc(local_geometric_histogram + geometric_histogram_bucket_id);
    }

    int photometric_histogram_bucket_id =
        floor((photometric_resiudal - photometric_histogram_min) *
              photometric_histogram_bucket_inv_width);
    if (isfinite(photometric_resiudal) &&
        photometric_histogram_bucket_id >= 0 &&
        photometric_histogram_bucket_id < photometric_histogram_size) {
      atomic_inc(local_photometric_histogram + photometric_histogram_bucket_id);
    }
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  // Write to global histograms
  for (int i = get_local_id(0);
       i < geometric_histogram_size;
       i += get_local_size(0)) {
    atomic_add(global_geometric_histogram + i, local_geometric_histogram[i]);
  }
  for (int i = get_local_id(0);
       i < photometric_histogram_size;
       i += get_local_size(0)) {
    atomic_add(global_photometric_histogram + i, local_photometric_histogram[i]);
  }
}
