#include "p0se/kernel-functions/compute-residual-scale-mad/compute-residual-scale-mad.h"
#include "p0se/kernel-functions/common/numeric-affine-transforms.h"

namespace p0se {

ComputeResidualScaleMad::ComputeResidualScaleMad(cl::Context context)
  : KernelFunction(context) {
  kernel_ = CreateAndBuildKernel("ComputeResidualScaleMad", {
    "p0se/kernel-functions/common/linearize.cl",
    "p0se/kernel-functions/common/affine-transform.cl",
    "p0se/kernel-functions/common/read-depth.cl",
    "p0se/kernel-functions/common/read-rgb.cl",
    "p0se/kernel-functions/compute-residual-scale-mad/compute-residual-scale-mad.cl"
  });

  cl::Device device = context.getInfo<CL_CONTEXT_DEVICES>()[0];
  int32_t num_compute_units = device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();

  num_workgroups_ = num_compute_units * 8;

  // Create histograms
  geometric_histogram_ = Histogram(0.0f, 2048, 1.0f / 2048.0f); // 0.0 - 1.0
  photometric_histogram_ = Histogram(0.0f, 2048, 256.0f / 2048.0f); // 0 - 2048

  // Allocate memory
  geometric_histogram_buffer_ = cl::Buffer(
      context,
      CL_MEM_READ_WRITE,
      geometric_histogram_.GetSize() * sizeof(int));
  photometric_histogram_buffer_ = cl::Buffer(
      context,
      CL_MEM_READ_WRITE,
      photometric_histogram_.GetSize() * sizeof(int));
}

void ComputeResidualScaleMad::Run(
    cl::CommandQueue command_queue,
    const DeviceImage& pre_depth_image,
    const DeviceImage& pre_rgb_image,
    const DeviceImage& cur_depth_image,
    const DeviceImage& cur_rgb_image,
    const float depth_scale_factor,
    const Sophus::SE3f motion,
    const Eigen::Matrix3f camera_matrix,
    float* geometric_residual_scale,
    float* photometric_residual_scale) {
  int32_t workgroup_size = 128;

  cl_int zero = 0;
  clEnqueueFillBuffer(command_queue(),
                      geometric_histogram_buffer_(),
                      &zero, sizeof(zero),
                      0, geometric_histogram_.GetNumBytes(),
                      0, nullptr, nullptr);
  clEnqueueFillBuffer(command_queue(),
                      photometric_histogram_buffer_(),
                      &zero, sizeof(zero),
                      0, photometric_histogram_.GetNumBytes(),
                      0, nullptr, nullptr);

  SetKernelArgs(
      pre_depth_image.GetBuffer(),
      pre_rgb_image.GetBuffer(),
      cur_depth_image.GetBuffer(),
      cur_rgb_image.GetBuffer(),
      pre_depth_image.GetSize(),
      depth_scale_factor,
      AffineTransform3f::CreateForReprojection(motion, camera_matrix),

      LocalBuffer(geometric_histogram_.GetSize() * sizeof(int)),
      geometric_histogram_buffer_,

      LocalBuffer(photometric_histogram_.GetSize() * sizeof(int)),
      photometric_histogram_buffer_);

  command_queue.enqueueNDRangeKernel(
      kernel_,
      cl::NullRange,
      cl::NDRange(num_workgroups_ * workgroup_size),
      cl::NDRange(workgroup_size));

  command_queue.enqueueReadBuffer(
      geometric_histogram_buffer_, true,
      0, geometric_histogram_.GetNumBytes(),
      geometric_histogram_.GetData());
  command_queue.enqueueReadBuffer(
      photometric_histogram_buffer_, true,
      0, photometric_histogram_.GetNumBytes(),
      photometric_histogram_.GetData());

  *geometric_residual_scale =  geometric_histogram_.GetMedian() * 1.48f;
  *photometric_residual_scale =  photometric_histogram_.GetMedian() * 1.48f;
}

} // End of namespace p0se
