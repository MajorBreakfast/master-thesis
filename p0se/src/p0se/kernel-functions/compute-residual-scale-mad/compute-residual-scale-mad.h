#pragma once

#include "p0se/kernel-functions/kernel-function.h"
#include <cinttypes>
#include <Eigen/Core>
#include <sophus/se3.hpp>
#include "p0se/images/device-image.h"
#include "p0se/kernel-functions/common/histogram.h"

namespace p0se {

class ComputeResidualScaleMad : public KernelFunction {
 public:
  ComputeResidualScaleMad() {};
  ComputeResidualScaleMad(cl::Context context);

  void Run(cl::CommandQueue command_queue,
           const DeviceImage& pre_depth_image,
           const DeviceImage& pre_rgb_image,
           const DeviceImage& cur_depth_image,
           const DeviceImage& cur_rgb_image,
           const float depth_scale_factor,
           const Sophus::SE3f motion,
           const Eigen::Matrix3f camera_matrix,
           float* geometric_residual_scale,
           float* photometric_residual_scale);
 private:
  int32_t num_workgroups_;
  cl::Buffer geometric_histogram_buffer_;
  cl::Buffer photometric_histogram_buffer_;
  Histogram geometric_histogram_;
  Histogram photometric_histogram_;
};

} // End of namespace p0se
