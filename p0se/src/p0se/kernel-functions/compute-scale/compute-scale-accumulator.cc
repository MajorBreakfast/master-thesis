#include "p0se/kernel-functions/compute-scale/compute-scale-accumulator.h"

#include <algorithm>

namespace p0se {

ComputeScaleAccumulator& ComputeScaleAccumulator::operator+=(
    const ComputeScaleAccumulator& acc2) {
  covariance += acc2.covariance;
  return *this;
}

float ComputeScaleAccumulator::GetCovariance() {
  return covariance;
}

} // End of namespace p0se
