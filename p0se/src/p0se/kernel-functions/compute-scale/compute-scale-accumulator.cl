typedef struct __attribute__((packed)) {
  float covariance;
} ComputeScaleAccumulator;

inline void ResetComputeScaleAccumulator(local ComputeScaleAccumulator* acc) {
  acc->covariance = 0.0f;
}

inline void AddRowToComputeScaleAccumulator(
    const float residual,
    const float weight,
    const float mean,
    local ComputeScaleAccumulator* acc) {
  float diff = residual - mean;
  float covariance = diff * diff * weight;
  acc->covariance += covariance;
}

inline void AddComputeScaleAccumulators(
    const local ComputeScaleAccumulator* first,
    local ComputeScaleAccumulator* second) {
  second->covariance += first->covariance;
}

inline void StoreSumOfWorkgroupComputeScaleAccumulators(
    local ComputeScaleAccumulator* local_accumulator_buf,
    global ComputeScaleAccumulator* global_accumulator_buf) {
  barrier(CLK_LOCAL_MEM_FENCE);
  for(int offset = get_local_size(0) / 2; offset > 0; offset = offset / 2) {
    if (get_local_id(0) < offset) {
      AddComputeScaleAccumulators(
          local_accumulator_buf + get_local_id(0) + offset,
          local_accumulator_buf + get_local_id(0));
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  if (get_local_id(0) == 0) {
    global_accumulator_buf[get_group_id(0)] = local_accumulator_buf[0];
  }
}
