#pragma once

#include <CL/cl2.hpp>
#include <Eigen/Core>
#include <p0se/utils/packed-struct.h>

namespace p0se {

P0SE_PACKED_STRUCT ComputeScaleAccumulator {
  cl_float covariance;

  ComputeScaleAccumulator& operator+=(const ComputeScaleAccumulator& acc2);

  float GetCovariance();
} P0SE_END_PACKED_STRUCT;

} // End of namespace p0se
