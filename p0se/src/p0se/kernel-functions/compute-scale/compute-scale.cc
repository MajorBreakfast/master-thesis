#include "p0se/kernel-functions/compute-scale/compute-scale.h"

namespace p0se {
ComputeScale::ComputeScale(cl::Context context)
  : KernelFunction(context) {
  kernel_ = CreateAndBuildKernel("ComputeScale", {
    "p0se/kernel-functions/compute-scale/compute-scale-accumulator.cl",
    "p0se/kernel-functions/compute-scale/compute-scale.cl"
  });

  cl::Device device = context.getInfo<CL_CONTEXT_DEVICES>()[0];
  int32_t num_compute_units = device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();

  num_workgroups_ = num_compute_units * 8;

  // Allocate memory
  accumulators_.resize(num_workgroups_);
  accumulators_buffer_ = cl::Buffer(
      context,
      CL_MEM_READ_WRITE,
      accumulators_.size() * sizeof(ComputeScaleAccumulator));
}

void ComputeScale::Run(
    cl::CommandQueue command_queue,
    const DeviceImage& resiudals_device_image,
    const DeviceImage& weights_device_image,
    const float mean,
    const int32_t num_valid_residuals,
    float *covariance) {
  int32_t workgroup_size = 128;

  SetKernelArgs(resiudals_device_image.GetBuffer(),
                weights_device_image.GetBuffer(),
                resiudals_device_image.GetNumPixels(),
                mean,
                LocalBuffer(workgroup_size * sizeof(ComputeScaleAccumulator)),
                accumulators_buffer_);

  command_queue.enqueueNDRangeKernel(
      kernel_,
      cl::NullRange,
      cl::NDRange(num_workgroups_ * workgroup_size),
      cl::NDRange(workgroup_size));

  command_queue.enqueueReadBuffer(
      accumulators_buffer_, true,
      0, accumulators_.size() * sizeof(ComputeScaleAccumulator),
      accumulators_.data());

  ComputeScaleAccumulator acc = {};
  for (auto& acc2 : accumulators_) { acc += acc2; }

  *covariance = acc.GetCovariance() / num_valid_residuals;
}

} // End of namespace p0se
