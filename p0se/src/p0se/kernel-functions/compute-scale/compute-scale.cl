// Requires compute-scale-accumulator.cl

kernel void ComputeScale(
    global float* residuals_buf,
    const global float* weights_buf,
    const int num_residuals,
    const float mean,
    local ComputeScaleAccumulator* local_accumulators_buf,
    global ComputeScaleAccumulator* global_accumulators_buf) {
  local ComputeScaleAccumulator* acc = local_accumulators_buf + get_local_id(0);
  ResetComputeScaleAccumulator(acc);
  // Loop over residuals
  for (int i = get_global_id(0); i < num_residuals; i += get_global_size(0)) {
    float residual = residuals_buf[i];
    float weight = weights_buf[i];
    if (isfinite(residual)) {
      AddRowToComputeScaleAccumulator(residual, weight, mean, acc);
    }
  } // End of loop over residuals

  // Perform reduction and store accumulator in global buffer
  StoreSumOfWorkgroupComputeScaleAccumulators(local_accumulators_buf,
                                              global_accumulators_buf);
}
