#pragma once

#include "p0se/kernel-functions/kernel-function.h"
#include <cinttypes>
#include <Eigen/Core>
#include "p0se/images/device-image.h"
#include "p0se/kernel-functions/compute-scale/compute-scale-accumulator.h"

namespace p0se {

class ComputeScale : public KernelFunction {
 public:
  ComputeScale() = default;
  ComputeScale(cl::Context context);

  void Run(cl::CommandQueue command_queue,
           const DeviceImage& resiudals_image,
           const DeviceImage& weights_image,
           const float mean,
           const int32_t num_valid_residuals,
           float *covariance);

 private:
  int32_t num_workgroups_;
  cl::Buffer accumulators_buffer_;
  std::vector<ComputeScaleAccumulator> accumulators_;
};

} // End of namespace p0se
