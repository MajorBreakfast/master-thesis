#include "p0se/kernel-functions/downsampler-depth/downsampler-depth.h"

namespace p0se {

DownsamplerDepth::DownsamplerDepth(cl::Context context)
  : KernelFunction(context) {
  kernel_ = CreateAndBuildKernel("DownsamplerDepth", {
    "p0se/kernel-functions/common/linearize.cl",
    "p0se/kernel-functions/downsampler-depth/downsampler-depth.cl"
  });
}

void DownsamplerDepth::Enqueue(cl::CommandQueue command_queue,
                               const DeviceImage& src_depth_device_image,
                               DeviceImage* dst_depth_device_image) {
  SetKernelArgs(src_depth_device_image.GetBuffer(),
                dst_depth_device_image->GetSize(),
                dst_depth_device_image->GetBuffer());

  command_queue.enqueueNDRangeKernel(
      kernel_,
      cl::NullRange,
      MakeNextMultiple2DRange(dst_depth_device_image->GetSize(), { 32, 32 }),
      cl::NDRange(32, 32));
}

} // End of namespace p0se
