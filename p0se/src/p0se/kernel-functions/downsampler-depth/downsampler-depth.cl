// Requires common/linearize.cl

kernel void DownsamplerDepth(
    const global ushort* src_depth_buf,
    const int2 dst_size,
    global ushort* dst_depth_buf) {
  int2 dst_pos = (int2)(get_global_id(0), get_global_id(1));

  if (dst_pos.x >= dst_size.x || dst_pos.y >= dst_size.y) { return; }

  int2 src_pos = 2 * dst_pos;
  int src_ld = 2 * dst_size.x; // Leading dimension (width)

  int v00 = src_depth_buf[Linearize(src_pos               , src_ld)];
  int v10 = src_depth_buf[Linearize(src_pos + (int2)(1, 0), src_ld)];
  int v01 = src_depth_buf[Linearize(src_pos + (int2)(0, 1), src_ld)];
  int v11 = src_depth_buf[Linearize(src_pos + (int2)(1, 1), src_ld)];

  int acc = v00 + v10 + v01 + v11;
  int num_valid = (v00 > 0) + (v10 > 0) + (v01 > 0) + (v11 > 0);

  dst_depth_buf[Linearize(dst_pos, dst_size.x)] =
      num_valid > 0 ? acc / num_valid : 0;
}
