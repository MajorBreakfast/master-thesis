#include "p0se/kernel-functions/downsampler-rgb/downsampler-rgb.h"

namespace p0se {

DownsamplerRgb::DownsamplerRgb(cl::Context context)
  : KernelFunction(context) {
  kernel_ = CreateAndBuildKernel("DownsamplerRgb", {
    "p0se/kernel-functions/common/linearize.cl",
    "p0se/kernel-functions/downsampler-rgb/downsampler-rgb.cl"
  });
}

void DownsamplerRgb::Enqueue(cl::CommandQueue command_queue,
                             const DeviceImage& src_rgb_device_image,
                             DeviceImage* dst_rgb_device_image) {
  SetKernelArgs(src_rgb_device_image.GetBuffer(),
                dst_rgb_device_image->GetSize(),
                dst_rgb_device_image->GetBuffer());

  command_queue.enqueueNDRangeKernel(
      kernel_,
      cl::NullRange,
      MakeNextMultiple2DRange(dst_rgb_device_image->GetSize(), { 32, 32 }),
      cl::NDRange(32, 32));
}

} // End of namespace p0se
