#pragma once

#include "p0se/kernel-functions/kernel-function.h"
#include "p0se/images/device-image.h"

namespace p0se {

class DownsamplerRgb : public KernelFunction {
 public:
  DownsamplerRgb(cl::Context context);

  void Enqueue(cl::CommandQueue command_queue,
               const DeviceImage& src_rgb_device_image,
               DeviceImage* dst_rgb_device_image);
};

} // End of namespace p0se
