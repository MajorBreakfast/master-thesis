#include "p0se/kernel-functions/kernel-function.h"
#include <stdexcept>
#include <map>
#include <iostream>
#include "p0se/utils/concat.h"
#include "p0se/utils/ends-with.h"


namespace p0se {

std::map<std::string, std::string> kKernelMap = {
  #include "p0se/kernels-inc.h"
};

KernelFunction::KernelFunction()  {}

KernelFunction::KernelFunction(cl::Context context)
 : context_(context) {}

cl::Kernel KernelFunction::CreateAndBuildKernel(const std::string& name,
                                                std::vector<std::string> file_names) {
  auto devices = context_.getInfo<CL_CONTEXT_DEVICES>();

  // Create program
  std::vector<std::string> sources;
  for (auto file_name : file_names) {
    if (EndsWith(file_name, ".cl")) {
      if (!kKernelMap.count(file_name)) {
        throw std::runtime_error(Concat("Cannot find \"", file_name, "\""));
      }
      sources.push_back(kKernelMap[file_name]);
    } else {
      sources.push_back(file_name);
    }
  }
  cl::Program program = cl::Program(context_, sources);

  // Build program for all devices in context
  program.build(devices);

  // Build log
  for (auto& device : devices) {
    cl_build_status status = program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device);
    if (status != CL_BUILD_SUCCESS) {
      throw std::runtime_error(
        Concat("Build log for \"", name, "\":\n",
          program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device)));
    }

    build_log_ += "Build log for \"" + name + "\":\n";
    build_log_ += program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device);
  }

  return cl::Kernel(program, name.c_str());
}

} // End of namespace p0se
