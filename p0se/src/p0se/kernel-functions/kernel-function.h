#pragma once

#include <cinttypes>
#include <vector>
#include <string>
#include <CL/cl2.hpp>

namespace p0se {

class KernelFunction {
 public:
  KernelFunction();
  KernelFunction(cl::Context context);
  const std::string& GetBuildLog() const { return build_log_; }

 protected:
  cl::Context context_;
  std::string build_log_;
  cl::Kernel kernel_;

  cl::Kernel CreateAndBuildKernel(const std::string& name,
                                  std::vector<std::string> file_names);

  static cl::NDRange MakeNextMultiple2DRange (cl_int2 size,
                                              cl_int2 tile_size);

  struct LocalBuffer {
    size_t size;
    LocalBuffer(size_t size) : size(size) {}
  };

  template<typename... Args>
  void SetKernelArgs(Args&&... args);

 private:
  void SetKernelArgsImpl(int32_t i);

  template<typename T, typename... Args>
  void SetKernelArgsImpl(int32_t i, T&& value, Args&&... args);

  template<typename... Args>
  void SetKernelArgsImpl(int32_t i, LocalBuffer&& value, Args&&... args);
};

inline cl::NDRange KernelFunction::MakeNextMultiple2DRange (cl_int2 size,
                                                            cl_int2 tile_size) {
  return cl::NDRange(
    ((size.x + tile_size.x - 1) / tile_size.x) * tile_size.x,
    ((size.y + tile_size.y - 1) / tile_size.y) * tile_size.y
  );
}

inline void KernelFunction::SetKernelArgsImpl(int32_t i) {}

template<typename T, typename... Args>
inline void KernelFunction::SetKernelArgsImpl(int32_t i,
                                              T&& value,
                                              Args&&... args) {
  kernel_.setArg(i++, value);
  SetKernelArgsImpl(i, std::forward<Args>(args)...);
}

template<typename... Args>
inline void KernelFunction::SetKernelArgsImpl(int32_t i,
                                              LocalBuffer&& value,
                                              Args&&... args) {
  kernel_.setArg(i++, value.size, nullptr);
  SetKernelArgsImpl(i, std::forward<Args>(args)...);
}

template<typename... Args>
inline void KernelFunction::SetKernelArgs(Args&&... args) {
  SetKernelArgsImpl(0, std::forward<Args>(args)...);
}

} // End of namespace p0se
