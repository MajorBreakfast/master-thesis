#pragma once

#include <string>
#include "p0se/logger/log-message.h"
#include "p0se/kernel-functions/common/histogram.h"

namespace p0se {

class HistogramLogMessage : public LogMessage {
 public:
  HistogramLogMessage(std::string string, Histogram histogram)
    : title_(string),
      histogram_(histogram) {}

  const std::string& GetTitle() const { return title_; }
  const Histogram& GetHistogram() const { return histogram_; }
 private:
  std::string title_;
  Histogram histogram_;
};

} // End of namespace p0se
