#include "p0se/logger/image-log-message.h"
#include <algorithm>
#include <cinttypes>
#include <vector>

namespace p0se {

HostImage ImageLogMessage::CreateHostImage(const cl::CommandQueue command_queue,
                                           const DeviceImage& device_image,
                                           const ImageType image_type) {
  std::vector<char> buffer(device_image.GetNumBytes());
  command_queue.enqueueReadBuffer(device_image.GetBuffer(),
                                  true,
                                  0,
                                  device_image.GetNumBytes(),
                                  buffer.data());
  command_queue.flush();

  HostImage host_image(device_image.GetWidth(),
                       device_image.GetHeight(),
                       HostImage::UCHAR4);
  uint8_t* output = reinterpret_cast<uint8_t*>(host_image.GetBuffer());
  const int32_t num_pixels = device_image.GetNumPixels();

  switch (image_type) {
    case RGB: {
      const uint8_t* input = reinterpret_cast<uint8_t*>(buffer.data());
      for (int32_t pos = 0; pos < num_pixels; ++pos) {
        output[4 * pos    ] = input[3 * pos    ];
        output[4 * pos + 1] = input[3 * pos + 1];
        output[4 * pos + 2] = input[3 * pos + 2];
        output[4 * pos + 3] = 255;
      }
      break;
    }

    case DEPTH: {
      const uint16_t* input = reinterpret_cast<uint16_t*>(buffer.data());
      const float factor = 1 / 150.0f;
      for (int32_t pos = 0; pos < num_pixels; ++pos) {
        float v = input[pos] * factor;
        v = 255.0f - std::max(0.0f, std::min(v, 255.0f));
        uint8_t value = static_cast<uint8_t>(v);
        if (value < 255) {
          output[4 * pos    ] = value;
          output[4 * pos + 1] = value;
          output[4 * pos + 2] = value;
          output[4 * pos + 3] = 255;
        } else {
          output[4 * pos    ] = 147; // Light blue
          output[4 * pos + 1] = 188;
          output[4 * pos + 2] = 255;
          output[4 * pos + 3] = 255;
        }
      }
      break;
    }

    case RESIDUAL: {
      const float* input = reinterpret_cast<float*>(buffer.data());
      for (int32_t pos = 0; pos < num_pixels; ++pos) {
        float v = input[pos] * 30.0f;
        v = std::max(0.0f, std::min(v, 255.0f));
        uint8_t value = static_cast<uint8_t>(v);
        if (!std::isnan(input[pos])) {
          output[4 * pos    ] = value;
          output[4 * pos + 1] = value;
          output[4 * pos + 2] = value;
          output[4 * pos + 3] = 255;
        } else {
          output[4 * pos    ] = 158; // Light green
          output[4 * pos + 1] = 249;
          output[4 * pos + 2] = 102;
          output[4 * pos + 3] = 255;
        }
      }
      break;
    }

    case JACOBIAN: {
      const float* input = reinterpret_cast<float*>(buffer.data());
      for (int32_t pos = 0; pos < num_pixels; ++pos) {
        float v = input[pos] * 10.0f + 128.0f;
        v = std::max(0.0f, std::min(v, 255.0f));
        uint8_t value = static_cast<uint8_t>(v);
        if (!std::isnan(input[pos])) {
          output[4 * pos    ] = value;
          output[4 * pos + 1] = value;
          output[4 * pos + 2] = value;
          output[4 * pos + 3] = 255;
        } else {
          output[4 * pos    ] = 244,  // Red with orange tint
          output[4 * pos + 1] = 119;
          output[4 * pos + 2] = 66;
          output[4 * pos + 3] = 255;
        }
      }
      break;
    }

    case FLOAT_DEPTH: {
      const float* input = reinterpret_cast<float*>(buffer.data());
      const float factor = 1 / 0.0002f / 150.0f;
      for (int32_t pos = 0; pos < num_pixels; ++pos) {
        float v = input[pos] * factor;
        v = 255.0f - std::max(0.0f, std::min(v, 255.0f));
        uint8_t value = static_cast<uint8_t>(v);
        if (value < 255) {
          output[4 * pos    ] = value;
          output[4 * pos + 1] = value;
          output[4 * pos + 2] = value;
          output[4 * pos + 3] = 255;
        } else {
          output[4 * pos    ] = 147; // Light blue
          output[4 * pos + 1] = 188;
          output[4 * pos + 2] = 255;
          output[4 * pos + 3] = 255;
        }
      }
      break;
    }

    default: break;
  }

  return host_image;
}

} // End of namespace p0se
