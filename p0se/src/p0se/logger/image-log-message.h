#pragma once

#include <string>
#include <CL/cl2.hpp>
#include "p0se/logger/log-message.h"
#include "p0se/images/host-image.h"
#include "p0se/images/device-image.h"

namespace p0se {

class ImageLogMessage : public LogMessage {
 public:
  enum ImageType { RGB, DEPTH, RESIDUAL, JACOBIAN, FLOAT_DEPTH };

  ImageLogMessage(const std::string& title,
                  cl::CommandQueue command_queue,
                  const DeviceImage& device_image,
                  const ImageType image_type)
  : title_(title),
    host_image_(CreateHostImage(command_queue, device_image, image_type)) {}

  const std::string& GetTitle() const { return title_; }
  const HostImage& GetHostImage() const { return host_image_; }
 private:
  std::string title_;
  HostImage host_image_;

  static HostImage CreateHostImage(const cl::CommandQueue command_queue,
                                   const DeviceImage& device_image,
                                   const ImageType image_type);
};

} // End of namespace p0se
