#pragma once

#include <cinttypes>
#include <vector>
#include <memory>
#include <CL/cl2.hpp>
#include "p0se/images/device-image.h"
#include "p0se/logger/string-log-message.h"
#include "p0se/logger/image-log-message.h"
#include "p0se/logger/histogram-log-message.h"

namespace p0se {

class Logger {
 public:
  void LogString(std::string string) {
    LogMessage* msg = new StringLogMessage(string);
    log_messages_.push_back(std::unique_ptr<LogMessage>(msg));
  }

  void LogDeviceImage(const std::string& title,
                      const cl::CommandQueue command_queue,
                      const DeviceImage& device_image,
                      const ImageLogMessage::ImageType image_type) {
    LogMessage* msg = new ImageLogMessage(
        title, command_queue, device_image, image_type);
    log_messages_.push_back(std::unique_ptr<LogMessage>(msg));
  }

  void LogHistogram(const std::string& title,
                    const Histogram& histogram) {
    LogMessage* msg = new HistogramLogMessage(title, histogram);
    log_messages_.push_back(std::unique_ptr<LogMessage>(msg));
  }

  const std::vector<std::unique_ptr<LogMessage>>& GetLogMessages() const {
    return log_messages_;
  }
 private:
  std::vector<std::unique_ptr<LogMessage>> log_messages_;
};

} // End of namespace p0se
