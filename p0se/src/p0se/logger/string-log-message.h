#pragma once

#include <string>
#include "p0se/logger/log-message.h"

namespace p0se {

class StringLogMessage : public LogMessage {
 public:
  StringLogMessage(std::string string) : string_(string) {}

  const std::string& GetString() const { return string_; }
 private:
  std::string string_;
};

} // End of namespace p0se
