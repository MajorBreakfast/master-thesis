#pragma once

#include <sstream>

namespace p0se {

// By SebastianK
// http://stackoverflow.com/questions/662918

inline void AddToStringStream(std::ostringstream&) {}

template<typename T, typename... Args>
inline void AddToStringStream(std::ostringstream& a_stream,
                       T&& a_value, Args&&... a_args) {
  a_stream << std::forward<T>(a_value);
  AddToStringStream(a_stream, std::forward<Args>(a_args)...);
}

template<typename... Args>
inline std::string Concat(Args&&... a_args) {
  std::ostringstream s;
  AddToStringStream(s, std::forward<Args>(a_args)...);
  return s.str();
}

} // End of namespace p0se
