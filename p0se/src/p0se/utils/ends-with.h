#pragma once

#include <sstream>

namespace p0se {

// By Dario, http://stackoverflow.com/questions/874134/find-if-string-ends-with-another-string-in-c

bool EndsWith(const std::string& a, const std::string& b) {
  if (b.size() > a.size()) return false;
  return std::equal(a.begin() + a.size() - b.size(), a.end(), b.begin());
}

} // End of namespace p0se
