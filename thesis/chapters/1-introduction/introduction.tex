\chapter{Introduction}

The estimation of camera poses over time, the camera trajectory, has been the subject of continuing research for multiple decades now. Various capturing technologies have been invented for multiple application areas with different objectives in mind.

One application area is 3D reconstruction. By estimating the camera pose over time, multiple camera images captured from different angles can be fused together into a 3D model. Various industries can make use of reconstructed objects. One example is the game industry where game designers employ ``photogrammetry'' to create realistic looking game assets. Photogrammetry relies on 3D object reconstruction to construct game assets from real world objects. With the increasing demand for more and more realistic games this approach provides a way to create detailed game assets without the usage of 3d modeling software. Objects constructed using photogrammetry can be less time consuming while providing greater realism and its use does not necessarily require personnel trained with the usage of 3D modeling software.

\begin{figure}[h]
\centering
\includegraphics[width=0.9\linewidth]{photogrammetry.jpg}
\caption{
	Photo-realistic game asset created using photogrammetry rendered in Unreal Engine 4 by artist Rense de Boer \cite{petaPixelPhotogrammetry}.
}
\end{figure}

Another application area is robotics. Many mobile robots need to be aware where they are with respect to their surroundings in order to navigate autonomously. To achieve this they can reconstruct an internal representation of the world around them, keep track of where there are currently located and remember where they were previously. This scenario is referred to as ``simultaneous location and mapping'' (SLAM).

\begin{figure}[H]
\centering
\includegraphics[width=0.7\linewidth]{vr200.jpg}
\caption{
	The Vorwerk VR200: A robotic vacuum cleaner that avoids cleaning the same area twice.
}
\end{figure}

This thesis concerns itself with how the camera trajectory can be estimated using RGB and depth image data. Cameras that caputure such data are called RGB-D cameras. In recent years RBG-D cameras have become widely available because image sensors have become small and cheap to manufacture.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{lenovo-phab-2-pro.jpg}
	\caption{
		Lenovo Phab 2 Pro: A phone with an RGB-D camera.
	}
\end{figure}

Since the title of this thesis ``Evaluation of Robust Dense Visual Odometry for RGB-D Cameras in OpenCL'' is very likely to sound rather intimidating or non-descriptive to people not familiar with the subject, let's break the title down into its constituent parts and clarify the meaning of each term.

``Odometry'' is the art of estimating the position and orientation of an object over time through sensor data. The word stems from the ancient Greek words ``hodós'' which means ``route'' and ``métron'' which means ``measurement'' \cite{wikiOdometry}. The combination of position and orientation will be called ``pose'' throught this thesis. The object whose pose is of interest can be some kind of autonomous system like a mobile robot, e.g. a flying drone or robotic vacuum cleaner. Robots on wheels or tracks can estimate their pose using sensors like rotary encoders which measure the wheel rotations. However, many types of sensors exist for different use cases and it depends on the kind of robot which combination of one or more sensors is most suitable for the application.

``Visual odometry'' is the art of determining the pose of an object over time through associated camera images. For robots with non-standard locomotion like drones or legged robots this kind of odometry can be the most suitable. An advantage of visual odometry over odometry that relies on rotary encoders to obtain position measurements is that it can be implemented in a way so that it accumulates less drift (errors accumulated over time). There exist different camera types that can be employed. These camera types include standard RGB (red, green, blue) cameras, infrared cameras and RGB-D cameras which capture depth measurements in addition to RGB image data.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{mavic-pro.jpg}
	\caption{
		DJI Mavic Pro: A drone that uses visual odometry for indoor flight stabilization.
	}
\end{figure}

``Dense visual odometry'' means that the algorithm employed to estimate the position and orientation works directly on the image data as opposed to feature points extracted from the images. This approach gained popularity with the rise of more powerful processors and the availability of affordable RGB-D cameras (More details about RGB-D cameras and their history will be explained in a later section). The advantage of algorithms working directly on the whole image data is that they don't discard image information with the feature extraction process and instead consider the complete available image data. Chapter 4 of this thesis will discuss in detail how a dense visual odometry algorithm works.

``Robust dense visual odometry'' additionally considers the fact that not every pixel of the captured images is equally reliable. For example it is common that depth components associated to RGB pixels around depth discontinuities are incorrect due to the way of how RGB-D cameras capture their sensor data: An RGB pixel on an object in the foreground might get associated with a depth value corresponding to the background or vice versa. Such and other types of incorrect sensor measurements, for example due to moving objects, usually affect the accuracy of the estimated pose in a negative way. Instead of letting each sensor measurement affect the pose estimate equally, robust weighting functions can be used to assign to each measurement a weight which controls its influence on the final output. Thus the term ``\textit{Robust} dense visual odometry''.

``OpenCL'' is a framework for writing highly parallel programs that can run on CPUs and GPUs. This makes it possible to utilize the parallel processing power of graphics cards for algorithms that can take advantage of high parallelism. The robust dense visual odometry algorithm implemented for this thesis suits itself very well for this kind of processing model.

\section{Problem statement}

The objective of this thesis is to investigate which alignment techniques work best for the estimation of the camera motion between two consecutive RGB-D camera frames.

\begin{itemize}
\item An implementation of a robust dense visual odometry algorithm is developed with OpenCL.
\item Different error terms are being analyzed: The geometric error and the photometric error
\item Different robust weighting functions are being compared to each other: Huber weights and Tukey weights
\end{itemize}

\section{Outline}
The outline of this thesis is structured as follows:

First, \textbf{chapter 2} gives an overview about related work. It presents existing approaches, algorithms and tools.

\textbf{Chapter 3} explains relevant background information. It goes into the working principle of RGB-D cameras, outlines terminology and programming techniques for OpenCL and explains the mathematical notations, tools and techniques employed in this thesis.

\textbf{Chapter 4} describes the details of the implemented odometry algorithm.

\textbf{Chapter 5} evaluates the algorithm on various datasets and presents an analysis on the performance and accuracy.

Finally, \textbf{chapter 6} concludes with a summary about the developed approach.
