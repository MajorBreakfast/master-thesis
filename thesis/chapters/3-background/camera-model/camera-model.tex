\section{Camera model}
\subsection{Physical Pinhole Camera}

The pinhole camera model is a simple but effective camera model for computer vision. The following picture illustrates the working principle of a real physical pinhole camera on which the camera model is based.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{pinhole-camera-2.jpg}
	\caption{Pinhole camera}
\end{figure}

The pinhole camera is comprised of a dark chamber with a screen and a tiny hole on opposite sides. Since light travels in a straight line and the only light that can enter has to travel through the hole, the light that falls on a certain coordinate on the screen can only have come from certain direction from the outside. This causes an upside-down projection of the outside to form on the screen.

\subsection{Simple Projection Formula}

When using the principle of a pinhole camera as a mathematical model for projection, it can be further simplified by considering a virtual screen mounted in front of the hole/origin at unit distance outside the chamber. That way the projected image does not appear flipped upside-down on the screen.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{projection-2.jpg}
	\caption{Projection}
\end{figure}

Using the intersect theorem this results in the following projection formula.
\begin{align}
\bm{\pi}\left(
\begin{pmatrix}
x \\
y \\
z \\
1
\end{pmatrix}
\right)
=
\begin{pmatrix}
x / z \\
y / z \\
1
\end{pmatrix}
=
\begin{pmatrix}
a \\
b \\
1
\end{pmatrix}
\end{align}

$(x, y, z, 1)^{\mathrm{T}}$ is a 3D-point in the camera frame.

$(a, b, 1)^{\mathrm{T}}$ is the 2D-point in sensor-space coordinates.

Note: The formula above uses 3D and 2D homogeneous coordinates because they are required by the formulas in the following sections.

\subsection{Projection with an Intrinsic Camera Matrix}
Real camera capture images with non-negative coordinates measured in pixels. A  2D affine transform can be employed to convert from sensor space coordinates produced by the projection formula above to image space coordinates.

\begin{align}
\bm{\pi_K}\left(
\begin{pmatrix}
x \\
y \\
z \\
1
\end{pmatrix}
\right)
=
\bm{K}\bm{\pi}\left(
\begin{pmatrix}
x \\
y \\
z \\
1
\end{pmatrix}
\right)
=
\bm{K}
\begin{pmatrix}
x / z \\
y / z \\
1
\end{pmatrix}
=
\begin{pmatrix}
u \\
v \\
1
\end{pmatrix}
\end{align}

with $
\bm{K} =
\begin{pmatrix}
f_x && s_x && o_x \\
s_y && f_y && o_y \\
0 && 0 && 1
\end{pmatrix}
$

$\bm{K}$ is different for each camera. Its values can be estimated for a particular camera by capturing images of a known test pattern, commonly a checkerboard. This process is called camera calibration. We call $\bm{K}$ the "intrinsic" matrix, because its values depend on the camera itself, not the camera's "extrinsic" position in the world.

It should be noted that 2D affine transforms can model translation, rotation, scaling and skewing. What they cannot model is lens distortion. If the used camera has non-negligible lens distortion, like for example a camera with a fisheye lens, additional work has to be done to take the distortion into account. One possible way to do this is to undistort the images as a preprocessing step.

The camera matrix can also be applied before the projection formula. The proof for this (not shown) builds on the fact that $\bm{K}$ is an affine matrix.

\begin{align}
\bm{\pi_K}\left(
\begin{pmatrix}
x \\
y \\
z \\
1
\end{pmatrix}
\right)
=
\bm{\pi}\left(
\begin{pmatrix}
&&        && && 0 \\
&& \bm{K} && && 0 \\
&&        && && 0 \\
0 && 0 && 0 && 1
\end{pmatrix}
\begin{pmatrix}
x \\
y \\
z \\
1
\end{pmatrix}
\right)
=
\begin{pmatrix}
u \\
v \\
1
\end{pmatrix}
\end{align}

\subsection{Backprojection formula}

It is possible to backproject a 2D-point in sensor-space to a 3D-point in the camera frame when its associated depth value $z$ is known.

\begin{align}
\bm{\pi^{-1}_K}\left(
\begin{pmatrix}
u \\
v \\
1
\end{pmatrix}
,
z
\right)
=
\begin{pmatrix}
  &&             &&   && 0 \\
  && \bm{K^{-1}} &&   && 0 \\
  &&             &&   && 0 \\
0 && 0           && 0 && 1
\end{pmatrix}
\begin{pmatrix}
uz \\
vz \\
z \\
1
\end{pmatrix}
=
\begin{pmatrix}
x \\
y \\
z \\
1
\end{pmatrix}
\end{align}

\subsection{Reprojection}

The algorithms used in this thesis require pixels situated in the image captured by a camera in one pose to be reprojected into the image of a camera in another pose. To achieve this the 2D point is first backprojected using $\bm{\pi^{-1}_K}$. Then, the resulting 3D point gets transformed using the rigid-body motion $\bm{T}$ between the two camera frames. And finally the 3D point is projected using $\bm{\pi_K}$ into the image of the other camera.

\begin{align}
\begin{pmatrix}
u_2 \\
v_2 \\
1
\end{pmatrix}
=
\bm{\pi_K}\left(
  \bm{T}
  \bm{\pi^{-1}_K}\left(
  \begin{pmatrix}
  u_1 \\
  v_1 \\
  1
  \end{pmatrix}
  ,
  z_1
  \right)
\right)
\end{align}

with $\bm{T}
=
\begin{pmatrix}
  &&        &&   &&        \\
  && \bm{R} &&   && \bm{t} \\
  &&        &&   &&        \\
0 && 0      && 0 && 1
\end{pmatrix}
$

Using the formulas from above this can be simplified to the following:

\begin{align}
\begin{pmatrix}
u_2 \\
v_2 \\
1
\end{pmatrix}
=
\bm{\pi}\left(
\underbrace{
  \begin{pmatrix}
    &&                           &&   &&        \\
    && \bm{K} \bm{R} \bm{K^{-1}} &&   && \bm{K} \bm{t} \\
    &&                           &&   &&        \\
  0 && 0                         && 0 && 1
  \end{pmatrix}
}_{=\bm{M}}
\begin{pmatrix}
u_1 z_1 \\
v_1 z_1 \\
z_1 \\
1
\end{pmatrix}
\right)
\end{align}

Note: $\bm{M}$ can be precomputed and then used for each pixel.
