\section{Robust Non-Linear Least Squares}

\subsection{Least Squares}

The least squares method is a common technique in regression analysis for determining an approximate solution to an overdetermined equation system, i.e. the system has more equations than parameters.

The least squares approach can be employed to fit a model $\bm{f}(\bm{x}, \bm{\beta})$ with $n$  parameters $\bm{\beta} = (\beta_1, \beta_2, ..., \beta_n)^{\mathrm{T}}$ to a set of $m$ measurements $\bm{y} = (y_1, y_2, ..., y_m)^{\mathrm{T}}$ at measurement locations $\bm{x} = (x_1, x_2, ..., x_m)^{\mathrm{T}}$. It finds model parameters $\bm{\beta}$, so that the sum of squared errors $E_{LS}(\bm{\beta})$ between the observed value $y_i$ and the fitted model $f(\bm{x_i}, \bm{\beta})$ is minimized.

\begin{align} 
\bm{\beta_{LS}}
&=
\mathop{\mathrm{arg\,min}}_{\bm{\beta}} E_{LS}(\bm{\beta}) \\
&=
\mathop{\mathrm{arg\,min}}_{\bm{\beta}} \label{eq:leastSquares}
\lVert \bm{y} - \bm{f}(\bm{x}, \bm{\beta}) \rVert ^2 \\
&= 
\mathop{\mathrm{arg\,min}}_{\bm{\beta}}
\sum_{i=1}^{m}{\left(
	\underbrace{
		y_i- f(\bm{x_i}, \bm{\beta})
	}_{r_i})
\right)^2}
\end{align}

The difference between an observation and the fitted value provided by the model is called residual $r_i$.

\subsection{Linear Least Squares}

For linear models, which can be expressed as a matrix vector product $\bm{f}(\bm{x}, \bm{\beta}) = \bm{A}(\bm{x}) \bm{\beta}$, equation \ref{eq:leastSquares} can be rewritten as:

\begin{align}
\bm{\beta_{LS}}
=
\mathop{\mathrm{arg\,min}}_{\bm{\beta}}
\lVert \bm{y} - \bm{A}(\bm{x}) \bm{\beta} \rVert ^2
\end{align}

If the columns of the matrix $\bm{A}(\bm{x})$ are linearly independent, there exists a unique solution $\bm{\beta_{LS}}$. It can be computed by solving the normal equations:

\begin{align}
\bm{A}(\bm{x})^{\mathrm{T}} \bm{A}(\bm{x}) \bm{\beta_{LS}}
=
\bm{A}(\bm{x})^{\mathrm{T}} \bm{y}
\end{align}


\subsection{Non-Linear Least Squares}

The least squares method can also be used for models that are non-linear in their parameters. One approach to solve the problem is using the Gauss-Newton method which iteratively finds the value of the parameters $\bm{\beta}$ that minimize the sum of squares.

The minimum value for $E_{LS}(\bm{\beta})$ can be found by setting the gradient to zero. Each of the $n$ parameters $\beta_j$ yields a gradient equation.

\begin{align} 
{\frac  {\partial E_{LS}(\bm{\beta})}{\partial \beta _{j}}}
&=
2\sum _{i=1}^{m}{r_{i}(\bm{\beta})}
{\frac  {\partial r_{i}(\bm{\beta})}{\partial \beta _{j}}} = 0
\quad (j=1,\ldots ,n)
\end{align}

\begin{align}
\sum _{i=1}^{m}{
	r_{i}(\bm{\beta})
	J_{ij}(\bm{\beta})
} = 0
\quad (j=1,\ldots ,n)
\label{eq:nonLinearLeastSquaresGradient}
\end{align}

$J_{ij}(\bm{\beta})$ denotes an entry of the Jacobian of $\bm{r}(\bm{\beta})$.

The Gauss-Newton method refines the $\bm{\beta}$ by iteratively computing an update $\bm{\Delta \beta}$ for the previous estimate $\bm{\beta^k}$ to get a better estimate $\bm{\beta^{k+1}}$. For this an initial estimate $\bm{\beta^0}$ is needed.

\begin{align}
\bm{\beta} \approx \bm{\beta^{k+1}} = \bm{\beta^k} + \bm{\Delta \beta}
\end{align}

The Taylor expansion is used to linearize the residual about the previous estimate $\bm{\beta^k}$:

\begin{align}
r_i(\bm{\beta})
&\approx
r_i(\bm{\beta^k}) +
\sum_{s=1}^{n}{
	{\frac  {\partial r_{i}(\bm{\beta^k})}{\partial \beta _s}}
	\underbrace{
		(\beta_s - \beta^k_s)
	}_{\Delta \beta_s}
} \\
&=
r_i(\bm{\beta^k}) +
\sum_{s=1}^{n}{
	J_{is}(\bm{\beta^k})
	\Delta \beta_s
}
\end{align}

The derivative can also be approximated:
\begin{align}
J_{ij}(\bm{\beta})
\approx
J_{ij}(\bm{\beta^k})
\end{align}

These approximations plugged into equation \ref{eq:nonLinearLeastSquaresGradient} yields:

\begin{align}
\sum _{i=1}^{m}{
	\left(
		r_i(\bm{\beta^k}) +
		\sum_{s=1}^{n}{
			J_{is}(\bm{\beta^k})
			\Delta \beta_s
		}
	\right)
   	J_{ij}(\bm{\beta^k})
} &= 0
\\
\sum_{i=1}^{m}{
	\sum_{s=1}^{n}{
		J_{ij}(\bm{\beta^k}) J_{is}(\bm{\beta^k}) \Delta \beta_s
	}
}
&=
-
\sum_{i=1}^{m}{
	J_{ij}(\bm{\beta^k}) r_i(\bm{\beta^k})
}
\end{align}
With $j=1,\dots ,n$

This simplifies to:

\begin{align}
\bm{J}(\bm{\beta^k}) ^{\mathrm{T}} \bm{J}(\bm{\beta^k}) \bm{\Delta \beta}
=
- \bm{J}(\bm{\beta^k}) ^{\mathrm{T}} \bm{r}(\bm{\beta^k})
\end{align}

The equations of this equation system are called the ``normal equations''.

The equation system can be rewritten using the short notations $\bm{J}$ for $\bm{J}(\bm{\beta^k})$ and $\bm{r}$ for $\bm{r}(\bm{\beta^k})$:

\begin{align}
\bm{J}^{\mathrm{T}} \bm{J}\bm{\Delta \beta}
=
- \bm{J}^{\mathrm{T}} \bm{r}
\end{align}

This equation is then solved for $\bm{\Delta \beta}$ to compute the new estimate $\bm{\beta^{k+1}}$.

If the Gauss-Newton algorithm converges, it is guaranteed to converge to a stationary point. However, convergence is not guaranteed. Therefore it is important to choose the initial parameters $\bm{\beta_0}$ close to the true solution. There exist extensions to the Gauss-Newton algorithm, like the Levenberg-Marquardt algorithm, which deal with divergence. This thesis, however, employs only the Gauss-Newton method because it produces good results for the particular problem of this thesis.

\subsection{Robust Non-Linear Least Squares}

In practice not all residuals are equally reliable. Some have an unusually large value due to discrepancies between model and measurements. These residuals which cannot be explained by the model are called outliers. However, the solution produced by the least squares approach as seen so far is heavily influenced by such residuals due to the quadratic term $r_i^2$. To alleviate the problem, a robust enrror function $\rho(x)$ (Greek letter ``rho'') which is not as vulnerable to unusual data can be used in place of the quadratic term.

\begin{align} 
\bm{\beta_{RLS}}
&=
\mathop{\mathrm{arg\,min}}_{\bm{\beta}} E_{RLS}(\bm{\beta}) \\
&= 
\mathop{\mathrm{arg\,min}}_{\bm{\beta}}
\sum_{i=1}^{m}{\rho(r_i(\bm{\beta}))} \label{eq:RLS}
\end{align}


Similar to before, the minimum can be found by setting the gradient to zero:

\begin{align} 
{\frac  {\partial E_{RLS}(\bm{\beta})}{\partial \beta _{j}}}
&=
\sum _{i=1}^{m}{
	\psi(r_{i}(\bm{\beta}))
	{\frac  {\partial r_{i}(\bm{\beta})}{\partial \beta _{j}}} = 0
}
\quad (j=1,\ldots ,n) 
\end{align}

Where $\psi(x)$ (Greek letter ``psi'') is the derivative of $\rho(x)$. $\psi(x)$ and called the ``influence function''.

Now $\psi(x)$ is substituted with $w(x)x$ where $w(x) = \frac{\psi(x)}{x}$ and called ``weight function''.

\begin{align} 
{\frac  {\partial E_{RLS}(\bm{\beta})}{\partial \beta _{j}}}
&=
\sum _{i=1}^{m}{
	w(r_{i}(\bm{\beta}))
	r_{i}(\bm{\beta})
	{\frac  {\partial r_{i}(\bm{\beta})}{\partial \beta _{j}}} = 0
}
\quad (j=1,\ldots ,n) \label{eq:RLSGradient} 
\end{align}

It can be observed that finding the stationary point in \ref{eq:RLS} using \ref{eq:RLSGradient} is equivalent to minimizing the iteratively reweighted least squares (IRLS) problem of the form \cite{szeliski2010}:

\begin{align} 
E_{IRLS}(\bm{\beta})
&= 
\sum_{i=1}^{m}{w(r_{i}(\bm{\beta})) (r_i(\bm{\beta}))^2}
\end{align}

The approach that can be employed to solve equation \ref{eq:RLSGradient} for the parameters $\bm{\beta}$ is called iteratively reweighted least squares approach. 

The algorithm alternates between computing the weight functions $w_i = w(r_{i})$ and solving the resulting weighted least squares problem with fixed weights $w_i$):

\begin{align}
\bm{J}^{\mathrm{T}} \bm{W} \bm{J}\bm{\Delta \beta}
=
- \bm{J}^{\mathrm{T}} \bm{W} \bm{r}
\end{align}

Where $\bm{W}$ is a diagonal matrix with $W_{ii} = w_i = w(r_{i}(\bm{\beta^k}))$. 

This means that iteration $k + 1$ uses for the computation of its parameter update $\bm{\Delta \beta}$ weights $w_i$ which it computes using the residuals $r_{i}(\bm{\beta^k})$ calculated with the parameters $\bm{b^k}$ that were estimated in the previous iteration $k$.

\subsubsection{Scale estimation}

The following two robust weight functions require their input of a certain scale to work properly. 

The scale parameter $\sigma$ can be estimated in a robust way using the median absolute deviation:

\begin{align}
\sigma_{MAD}(\bm{r}) =
c \cdot \mathop{\mathrm{median}}(
	|\bm{r} - \mathop{\mathrm{median}}(
		\bm{r}
	)|
)
\end{align}

Where $c = 1.482$

The residual can then be rescaled accordingly before it is plugged into a weight function: $\frac{r}{\sigma}$

\subsubsection{Huber Weights}

The Huber weight function has the following formula:

\begin{align}
w_{\text{Huber}}(r) =
\begin{cases}
1               & \text{for } r \leq k\\
\frac{k}{|r|}   & \text{otherwise}
\end{cases}
\end{align}

With $k = 1.345$ for a Gaussian distribution.

It's use has the effect that large residuals only affect the solution linearly.

\subsubsection{Tukey Weights}

The Tukey weight function has the following formula:

\begin{align}
w_{\text{Tukey}}(r) =
\begin{cases}
(1 - \frac{r^2}{b^2})^2   & \text{for } r \leq b\\
0                        & \text{otherwise}
\end{cases}
\end{align}

Usage of the Tukey weight function has the effect that large residuals don't affect the soltion at all.
