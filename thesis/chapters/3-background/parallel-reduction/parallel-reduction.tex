\section{Parallel Reduction}

Reductions are operations that reduce a data vector into a single value. For example the computation of the minimum, maximum or the sum of all values in the vector. They can be implemented efficiently if the underlying operation is associative and commutative. \cite{catanzaro2010}

The common way of making things fast on GPUs is to parallelize the problem as much as possible. So, the naive strategy would be to let each work item load one element of the vector, let the kernel perform operations using local memory and workgroup synchronization  and then write out one result value per workgroup into a buffer. This approach can then be repeated until a single value is left.

Unfortunately the naive approach with multiple stages is slow because its performance is limited by the memory bandwidth of the graphics card.

Better performance can be achieved by creating only as many work items as can be run in parallel by the underlying hardware. The values are then reduced in 3 steps:
\begin{itemize}
	\item Each work item reduces multiple vector elements into one value using a loop
	\item Then, the reduction is performed inside the workgroup using synchronization. Afterwards one value per workgroup is written into a result buffer
	\\ Finally a reduction on the host side reduces the result buffer values into a single value
\end{itemize}

For the workgroup size it is best to pick a large power of 2 (as large as the hardware allows) and for the number of workgroups it is best to pick a multiple of the number of the graphic card's compute units.

\begin{lstlisting}[
language=C,
caption={Parallel reduction kernel in OpenCL},
emph={kernel, global, local},
emphstyle={\color{blue}}
]
kernel void Reduce(
		const global float* input,
		const global int size,
		local float* local_buf,
		global float* global_buf) {
	// Step 1: Per work item reduction loop
	for (int i = get_global_id(0);
			i < size; i += get_global_size(0)) {
		local_buf[get_local_id(0)] += input[i];
	}
	
	// Step 2: Reduction inside the workgroup
	barrier(CLK_LOCAL_MEM_FENCE);
	for(int offset = get_local_size(0) / 2;
			offset > 0;
			offset = offset / 2) {
		if (get_local_id(0) < offset) {
			local_buf[get_local_id(0)] +=
				local_buf[get_local_id(0) + offset];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	if (get_local_id(0) == 0) {
		global_buf[get_group_id(0)] = local_buf[0];
	}
}
\end{lstlisting}