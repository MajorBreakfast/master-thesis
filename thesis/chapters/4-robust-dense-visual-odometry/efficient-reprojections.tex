\section{Efficient Reprojections}

In order to compare depth or RGB values between different camera frames it is necessary to reproject pixels from one camera frame into another. Therefore the reprojection formula which was introduced in the background chapter is used.

\begin{align}
\begin{pmatrix}
u_2 \\
v_2 \\
1
\end{pmatrix}
=
\bm{\pi}\left(
\underbrace{
	\begin{pmatrix}
	&&                           &&   &&        \\
	&& \bm{K} \bm{R} \bm{K^{-1}} &&   && \bm{K} \bm{t} \\
	&&                           &&   &&        \\
	0 && 0                       && 0 && 1
	\end{pmatrix}
}_{=\bm{M}}
\begin{pmatrix}
u_1 z_1 \\
v_1 z_1 \\
z_1 \\
1
\end{pmatrix}
\right)
\end{align}

The affine transform $M$ can be precomputed on the CPU. The reprojection then translates to this code:

\begin{lstlisting}[
language=C,
caption={Reprojection in OpenCL},
emph={float2, float4, AffineTransform3f},
emphstyle={\color{blue}}
]
float4 pre_point3dh = // Backprojection
	(float4)(pre_point2d.xy * pre_depth, pre_depth, 1);
float4 pre_warped_point3dh = // Apply transform
	MultiplyAffineTransform3fAndVector(transform, pre_point3dh);
float2 cur_point2d = // Projection
	pre_warped_point3dh.xy / pre_warped_point3dh.z;
\end{lstlisting}

Unlike OpenGL or DirectX there exists no matrix datatype in OpenCL. This means that there is also no build-in matrix multiplication operation. Row-major data storage makes it possible, however, to execute the matrix-vector multiplication with three dot products. Therefore, the affine transform is stored as a struct with three \lstinline[columns=fixed]{float4}s, each representing a row.

\begin{lstlisting}[
	language=C,
	caption={Matrix-vector product with three dot products},
	emph={float4, AffineTransform3f},
	emphstyle={\color{blue}}
]
typedef struct {
	float4 row0, row1, row2;
} AffineTransform3f;

inline float4 MultiplyAffineTransform3fAndVector(
		const AffineTransform3f trans,
		const float4 vec) {
	return (float4)(
		dot(trans.row0, vec),
		dot(trans.row1, vec),
		dot(trans.row2, vec),
		vec.s3
	);
}
\end{lstlisting}
