\chapter{Evaluation}

This thesis uses the TUM RGB-D Benchmark released in 2012 by Sturm et al \cite{sturm12iros} for the evaluation of the visual odometry pose estimation quality. It provides datasets with RGB-D images captured using a Microsoft Kinect sensor, a groundtruth trajectory of camera poses to compare against and evaluation scripts.

\section{Measurement Methodology}

\subsection{Error measurement}
In order to provide evaluation results that can be easily compared to other implementations of visual odometry algorithms this thesis uses the recommended TUM RGB-D Benchmark evaluation settings for visual odometry methods that the project website recommends.

The TUM RGB-D Benchmark includes a python script called \lstinline[columns=fixed]{evaluate_rpe.py} for the evaluation of the relative pose error (rpe). This script was used with the following command line options (as recommended on the website):

\begin{lstlisting}[language=bash, caption={Command line options for evaluate\_rpe.py}]
python evaluate_rpe.py
	groundtruth.txt
	trajectory.txt
	--max_pairs 10000
	--fixed_delta --delta 1 --delta_unit s
	--offset 0
	--scale 1
	--plot plot.png
	--verbose
}
\end{lstlisting}

This configuration evaluates the relative pose difference between the ground truth and and the estimated trajectory every second. Since the camera images from the benchmark datasets were captured at 30fps this means that a comparison is calculated every 30 frames.

The script distills the difference between two poses down into two error terms: Translational error and rotational error. It calculates these values using the following formulas:

\begin{align}
e^\text{trans}_i &= \lVert \bm{t}^\text{diff}_i \rVert \\
e^\text{rot}_i &= \cos^{-1} \left( {\frac{ \mathop{\mathrm{trace}}(\bm{R}^\text{diff}_i) - 1}{2}} \right)
\end{align}

Where $\bm{R}^\text{diff}_i$ and $\bm{t}^\text{diff}_i$ are the rotational and translational components of the difference between the ground truth relative pose and the estimated relative pose (rotational error formula from \cite{ma2001}).

The individual pose difference errors are then combine into one error metric for the whole dataset by calculating the ``root mean square error'' (RMSE).

\begin{align}
e^\text{trans}_\text{RMSE} &= \sqrt{\sum_i{(e^\text{trans}_i)^2}} \\
e^\text{rot}_\text{RMSE} &= \sqrt{\sum_i{(e^\text{rot}_i)^2}}
\end{align}

These two error terms are then representative for the estimation quality within the whole dataset. The higher the values are, the more drift happens during the pose estimation.

\subsection{Time measurement}

The runtime measurements in the following sections consider only the runtime of the algorithm itself. The loading times of the png image files and the time for the upload to the graphics card are not included in the runtime measurements.

\section{Evaluation of the Different Error Terms and Robust Weights}

This section evaluates the performance of the geometric and photometric error terms combined with different robust weighting functions.

\subsection{Geometric error}

First, the geometric error was evaluated with the early break mechanism enabled. This means that the algorithm stops the iterations within each pyramid level as soon as an iteration happens that has a higher overall error compared to the previous iteration within the same level.

\begin{table}[H]
	\caption{fr3/office: Geometric error with early break}
	\begin{center}
		\begin{tabular}{ p{6cm} | l | l | l }
			\hline
			Method   &	$e^\text{trans}_\text{RMSE}$ & $e^\text{rot}_\text{RMSE}$ & $\varnothing$ Runtime \\ \hline \hline
			Geometric error & 0.4646 m & 11.2623 deg & 43 ms \\ \hline 
			Geometric error with Huber weights & 0.1434 m & 4.5989 deg & 58 ms \\ \hline 
			Geometric error with Tukey weights & 0.1071 m & 4.8125 deg & 86 ms \\ \hline 
			\hline
		\end{tabular}
	\end{center}
\end{table}

The RMSE numers show that the geometric error function without robust weights does not produce a meaningful trajectory. This is due to how the least squares approach reacts to outliers. The histogram shows that there are many large residuals which have a strong influence on the solution due to the quadratic term of the least squares approach.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{geometric-histogram.jpg}
	\caption{
		Histogram of the geometric residuals: Many outliers are present. The x-axis is the distance in meters, y-axis the number of pixels
	}
\end{figure}

Next, it was tested whether the result improves with the early break mechanism disabled. The algorithm then stops after 20 iterations on each pyramid level.

\begin{table}[H]
	\caption{fr3/office: Geometric error without early break}
	\begin{center}
		\begin{tabular}{ p{6cm} | l | l | l }
			\hline
			Method   &	$e^\text{trans}_\text{RMSE}$ & $e^\text{rot}_\text{RMSE}$ & $\varnothing$ Runtime \\ \hline \hline
			Geometric error & 0.4755 m & 11.4453 deg & 122 ms \\ \hline 
			Geometric error with Huber weights & 0.1559 m & 4.8425 deg & 167 ms \\ \hline 
			Geometric error with Tukey weights & 0.0951 m & 4.1801 deg & 178 ms \\ \hline 
		\end{tabular}
	\end{center}
\end{table}

Only the Tukey weighted setting shows a minor improvement with disabled early break. The 3D view shows that the estimated trajectory using the geometric error function is recognizeable, but the overall tracking quality is not satisfactory.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{3d-plot-geometric-tukey.png}
	\caption{
		fr3/office: 3D plot of the geometric error with Tukey weights without early break. (red: estimated trajectory, green: ground truth)
	}
\end{figure}

\subsection{Photometric error}

The evaluation of the photometric error minimization approach shows very promising results:

\begin{table}[H]
	\caption{fr3/office: Photometric error with early break enabled}
	\begin{center}
		\begin{tabular}{ p{6cm} | l | l | l }
			\hline
			Method   &	$e^\text{trans}_\text{RMSE}$ & $e^\text{rot}_\text{RMSE}$ & $\varnothing$ Runtime \\ \hline \hline
			
			Photometric error & 0.0209 m & 0.8838 deg & 47 ms \\ \hline 
			Photometric error with Huber weights & 0.0170 m & 0.7319 deg & 75 ms \\ \hline
			Photometric error with Tukey weights & 0.0427 m & 2.2902 deg & 70 ms \\ \hline
			\hline
		\end{tabular}
	\end{center}
\end{table}

Especially the Huber weighted variant shows a very satisfactory $e^\text{trans}_\text{RMSE}$ value of 1.7cm/s. The longer runtime compared to the scenario without robust weighting can be explained with the slower converge induced by the Huber weights. Therefore, the algorithm needs more iterations.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{photometric-average-iterations-per-level.png}
	\caption{
		fr3/office: Average iterations per level for the photometric error
	}
\end{figure}

It was found that the time spent on iterations on the finest level (level 0) is the longest:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{photometric-average-time-per-level.png}
	\caption{
		fr3/office: Average time per level (in milliseconds) for the photometric error
	}
\end{figure}

The 3D view shows that good tracking quality has been achieved.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{3d-plot-photometric-huber.png}
	\caption{
		fr3/office: 3D plot for the photometric error with Huber weights
	}
\end{figure}

Compared to the geometric error term, the photometric error term produces far better pose estimations. This can be explained by the quality difference between the depth and RGB data captured by the Kinect sensor. The higher quality of the RGB image data gives the photometric error term the advantage.

\section{Photometric error with combined weights}

Since the photometric error term produces good results, it was evaluated whether the quality can be further improved by multiplying the weight of the geometric residual with the weight of the photometric residual. The idea is that the combination of both weights filters the good measurements and the outliers more comprehensively. The results show that only slight improvements were possible.

\begin{center}
	\begin{tabular}{ p{6cm} | l | l | l }
		\hline
		Method   &	$e^\text{trans}_\text{RMSE}$ & $e^\text{rot}_\text{RMSE}$ & $\varnothing$ Runtime \\ \hline \hline
		
		Photometric error with geometric Huber weights & 0.0174 m & 0.7477 deg & 39 ms \\ \hline
		Photometric error with geometric Tukey weights & 0.0176 m & 0.7648 deg & 38 ms \\ \hline 
		
		Photometric error with Huber weights and geometric Huber weights & 0.0165 m & 0.6986 deg & 78 ms \\ \hline 
		Photometric error with Huber weights and geometric Tukey weights & 0.0170 m & 0.7166 deg & 91 ms \\ \hline 
		\hline
	\end{tabular}
\end{center}

\section{GPU vs CPU}

OpenCL applications can run on most modern hardware. Unlike its competitor CUDA, OpenCL code can also be executed on CPUs. This section compares the execution speed on an Intel Core i5-6300U dual-core CPU with simultaneous multiprocessing of a Surface Book Laptop to its Nvidia GPU with three compute units.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{cpu-vs-gpu-avarage-frame-time.png}
	\caption{
		fr3/office: Average frame time (in milliseconds) for the photometric error with Huber weights
	}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\linewidth]{full-cpu.png}
	\caption{
		Intel Core i5-6300U under load
	}
\end{figure}

The speed difference was found to be very noticeable: The execution on the GPU was found to be 10 times faster.