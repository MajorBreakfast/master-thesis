\chapter{Mathematical Concepts and Notations}

This chapter introduces various mathematical concepts and notations used in this thesis.

\section{Text style}

For better readability variables and functions are written in a different style unique to their type throughout this thesis.

\begin{description}[noitemsep]
\item[Scalars:] Lowercase and italic, e.g. $n$
\item[Vectors:] Lowercase and bold, e.g. $\mathbf{v}$
\item[Matrices:] Uppercase and bold, e.g. $\mathbf{M}$
\end{description}

\section{Sets}

The following is a list with sets that are important for this thesis.

\begin{description}
  \item[Rotation matrices]
  $
  \mathrm{SO}(3) = \{
    \mathbf{R} \in \mathbb{R}^{3 \times 3}
    \mid
    \mathbf{R}^{\mathrm{T}} \mathbf{R} = \mathbf{I} \wedge \det(\mathbf{R}) = 1
  \}
  $
  
  \item[Skew-symmetric matrices]
  $
  \mathrm{so}(3) = \{
    \mathbf{M} \in \mathbb{R}^{3 \times 3}
    \mid
    \mathbf{M} = -\mathbf{M}^{\mathrm{T}}
  \}
  $
\end{description}

\section{Operations}

This section introduces various operations which are used throughout this thesis.

\subsection{Scalar product}
This thesis uses the angle bracket notation $\langle u, v \rangle$. The $u \cdot v$ notation is not used.

\subsection{Hat Operator}
The hat operator transforms a vector $\mathbf{u}$ into a skew-symmetric matrix $\widehat{\mathbf{u}}$ which behaves like the cross product.


\begin{align}
  \widehat{\mathbf{u}} &= \begin{pmatrix} 
    0    & -u_3 & u_2 \\
    u_3  & 0    & -u_1 \\
    -u_2 & u_1  & 0
  \end{pmatrix} \in \mathrm{so}(3)
  \\
  \widehat{\mathbf{u}}\mathbf{v} &= \mathbf{u} \times \mathbf{v}
\end{align}

where $\mathbf{u} = (u_1, u_2, u_3)^{\mathrm{T}} , \mathbf{v} \in \mathbb{R}^3$

In other literature the equivalent $[\mathbf{u}]_{\times}$ notation is often used. This thesis uses the more compact $\widehat{\mathbf{u}}$ notation.

\subsection{Jacobian}
The Jacobian of a function $\mathbf{f}: \mathbb{R}^n \to \mathbb{R}^m$ is a function $\mathbf{J_f}$ which returns a matrix whose columns are the partial derivatives of $\mathbf{f}$.

\begin{align}
\mathbf{J_f}(\mathbf{x}) &=
\begin{pmatrix}
  \frac{\partial \mathbf{f}}{\partial \textcolor{violet}{x_1}}(\mathbf{x}) &
  \frac{\partial \mathbf{f}}{\partial \textcolor{teal}{x_2}}(\mathbf{x}) &
  \cdots &
  \frac{\partial \mathbf{f}}{\partial \textcolor{blue}{x_n}}(\mathbf{x})
\end{pmatrix}
\end{align}

with $
  \mathbf{x} = (\textcolor{violet}{x_1},
                \textcolor{teal}{x_2},
                \dots,
                \textcolor{blue}{x_n})^\mathrm{T}
$

The main drawback of this notation is that it doesn't work with functions which return something else than a scalar or column vector, e.g. a matrix. However, in most cases the value can be made into a column vector, e.g. a matrix can be stacked, i.e. its column vectors can be stacked into a single vector.

\section{Matrixexponential and Matrixlogarithmn}

Let $\mathbf{R}: \mathbb{R} \to \mathrm{SO}(3)$ be a family of rotation matrices with $\mathbf{R}(0) = \mathbf{I}$ (used later).

\begin{align}
  \frac{\mathrm{d}}{\mathrm{dt}}
    \underbrace{
      \left(
        \mathbf{R}\mathbf{R}^\mathrm{T}
      \right)
    }_{=\mathbf{I}}
  =
  \dot{\mathbf{R}} \mathbf{R}^\mathrm{T} +
  \mathbf{R} \dot{\mathbf{R}}^\mathrm{T}
  = \mathbf{0}
\end{align}

It follows that
$
  \dot{\mathbf{R}} \mathbf{R}^\mathrm{T}
  =
  -(\dot{\mathbf{R}} \mathbf{R}^\mathrm{T})^\mathrm{T}
$ holds and consequently $\dot{\mathbf{R}} \mathbf{R}^\mathrm{T}$ must be skew-symmetric.

Let's define $\widehat{\mathbf{v}}(t) = \dot{\mathbf{R}} \mathbf{R}^\mathrm{T}$. Consequently $\dot{\mathbf{R}}(t) = \widehat{\mathbf{v}}(t)  \mathbf{R}(t)$ then holds ($\mathbf{R}$ was multiplied to the right).

Now, let's make $\mathbf{v}(t) = \mathrm{const}$, i.e. time-independent. This leads to the following:
\begin{align}
  \dot{\mathbf{R}}(t) &= \widehat{\mathbf{v}} \mathbf{R}(t)
\end{align}

Using this we can do a Taylor expansion at $0$. Note that we defined $\mathbf{R}(0) = \mathbf{I}$:
\begin{align}
  \mathbf{R}(t) &=
    \mathbf{R}(0) +
    \dot{\mathbf{R}}(0)t +
    \frac{\ddot{\mathbf{R}}(0)}{2!}t^2 + \dots
  \\ &=
    \mathbf{I} +
    \widehat{\mathbf{v}}t +
    \frac{\widehat{\mathbf{v}}^2}{2!}t^2 +
    \dots
  \\ &=
  \sum_{n=0}^{\infty}{
    \frac{(\widehat{\mathbf{v}}t)^n}{n!}
  }
  \\ &=
  e^{\widehat{\mathbf{v}}t}
  \\ &=
  e^{\widehat{\mathbf{w}}} \text{ with $\widehat{\mathbf{w}} = \widehat{\mathbf{v}}t$}
\end{align}

The series that we get matches the exponential series. The main difference is that the usual exponential series uses a scalar instead of a matrix as its parameter.

So, we now see that a vector $\mathbf{w}$ can be used to describe a rotation. This is great because unlike a rotation matrix this form has just three values and this coincides with the three degrees of freedom rotations have. Consequently this way is perfectly suited for optimization algorithms because.

Unfortunately an infinite sum cannot be computed efficiently by a processor. Luckily there is more that can be done: The terms can be rearranged so that the $\sin()$ and $\cos()$ functions can be used to get rid of the infinite sum.

\begin{align}
  \intertext{
    With $t = \left| \mathbf{w} \right|$ and
    $\mathbf{v} = \frac{1}{t}\mathbf{w}$
  }
  e^{\mathbf{\widehat{w}}} &=
    e^{\mathbf{\widehat{v}}t}
  \\ &=
    \sum_{n=0}^{\infty}{
      \frac{(\widehat{\mathbf{v}}t)^n}{n!}
    }
  \\ &=
    \mathbf{I} +
    \widehat{\mathbf{v}}t +
    \frac{\widehat{\mathbf{v}}^2}{2!}t^2 +
    \frac{\widehat{\mathbf{v}}^3}{3!}t^3 +
    \dots
  \\ &
    \text{and using } \widehat{\mathbf{v}}^3
    =
    \widehat{\mathbf{v}}
      \underbrace{
        (\mathbf{v}\mathbf{v}^{\mathrm{T}} - \mathbf{I})
      }_{=\widehat{\mathbf{v}}^2 \text{ (*)}}
    =
    \underbrace{
      \widehat{\mathbf{v}}\mathbf{v}
    }_{=\mathbf{v} \times \mathbf{v} = \mathbf{0}}
    \mathbf{v}^{\mathrm{T}} -\widehat{\mathbf{v}}
    =
    -\widehat{\mathbf{v}}
  \\ &=
    \mathbf{I}
    +
    \underbrace{\left(
      t - \frac{t^3}{3!} + \frac{t^5}{5!} - \dots
    \right)}_{=\sin(t)}
    \widehat{\mathbf{v}}
    +
    \underbrace{\left(
      \frac{t^2}{2!} - \frac{t^4}{4!} + \frac{t^6}{6!} - \dots
    \right)}_{=1 -\cos(t)}
    \widehat{\mathbf{v}}^2
  \\ &=
    \mathbf{I} +
      \sin(t) \mathbf{\widehat{v}} +
      (1 -\cos(t)) \mathbf{\widehat{v}}^2
  \\
\end{align}

(*) Can be verified by calculating both sides with $\mathbf{v}=(a, b, c)^\mathrm{T}$ and seeing that they match considering that $|\mathbf{v}| = 1$.

The resulting formula is called the "Rodrigues' rotation formula"\cite{wikiRod} and can be computed efficiently by a processor.

