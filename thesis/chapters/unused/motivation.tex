\chapter{Motivation}

\section{Different cost functions}

This section lists the different cost functions that were analyzed in this thesis. Each function has strengths and weaknesses and because of that it is beneficial to use a combination of multiple functions in practice.

\subsection{Photometric error}
This cost function penalizes the difference in the RGB channels.

\subsubsection{How it works}
Each pixel of the previous image is backprojected into 3d space using its z-value. This point is then transformed using the estimated rigid-body motion of the camera and then projected into the current frame. The RGB values at that pixel in the current frame is then compared to the RGB values of the original pixel in the previous frame. The error (also called residual) is the difference between the RBG value pairs. The closer the estimated rigid-body motion of the camera is to the actual motion, the smaller the error gets.

\begin{align}
\bm{\xi^*} = \operatornamewithlimits{arg~min}\limits_{\bm\xi} \left(
  \int_{\bm{x}}{
    \Big|
      \bm{I_2} \big( \mathbf{w}(\bm{\xi}, \bm{x}) \big)
      -
      \bm{I_1} \big( \bm{x} \big)
    \Big|^2
    dx
  }
\right)
\end{align}

\subsubsection{Strengths and Weaknesses}
This error function relies on the color and brightness of the pixels. This means that it works best if the captured scene has objects with different colors and brightnesses. This cost function cannot produce good results if the depicted objects are very homogeneous, like for example a blackboard. Furthermore it doesn't work well with non-Lambertian (shiny, not matte) materials that change with the position of the viewer.

\begin{lstlisting}[language=C++, caption={C++ code using listings}]
#include <iostream>
int main() {
  // Print hello to the console
  std::cout << "Hello, world!" << std::endl;
  return 0;
}
\end{lstlisting}



How to Parallelize a Reduction

Reductions are operations that reduce a data vector into a single value. For example the computation of the minimum, maximum or the sum of all values in the vector. Luckily reductions can be computed very efficiently on GPUs if the operation gives use some leeway by being associative and commutative. The following section describes how to implement reductions efficiently. It is based on a very interesting article about reductions by Bryan Catanzaro on AMD Developer Central \cite{amdReductions}.

The common way of making things fast on GPUs is to parallelize the problem as much as possible. So, the naive strategy is to create a work item for each element of the vector. Then each work item loads one element, the kernel performs the reduction using local memory and synchronization inside the workgroup and finally writes out the result of each workgroup into a buffer. After that the result buffer contains as many values as workgroups where started. Then it's just a matter of applying this strategy on the result buffer over and over again until there is just one value left. With each iteration the number of result values gets divided by the number of workgroups. The number of iterations is logarithmic with respect to the number of elements of the initial vector. For example if the vector has a billion elements and the workgroup size is 1024 work items, 3 iterations are required ($3 > log_1024(10^9)$).

picture of naive approach

Unfortunately the naive approach with multiple stages is slow because its performance is limited by the memory bandwidth of the graphics card. This means the performance could be better if the approach used less memory accesses.

Luckily there is an easy way to implement a reduction with less accesses to global memory. The trick is to parallelize less, but in a different and smarter way and thus avoid the big temporary result buffers of the naive approach altogether. Instead of creating one work item per vector element, it is better to only create as many work items as can be run in parallel by the hardware and handle multiple vector elements per work item using a loop. After all vector element values were handle the reduction is performed inside the workgroup. And finally the result of each workgroup is written into a result buffer. Note that the number of elements in the result buffer is not dependent on the number of elements like in the naive approach, instead it matches the number of workgroups that where started. Finally the reduction on the result buffer can be done on the host side. The article by AMD calls this strategy the "two-stage reduction". Stage 1 is the loop, stage 2 the reduction inside the workgroup, the rest is done on the host side.

For good performance it is of course necessary to pick an optimal number of workgroups and optimal workgroup size. For the workgroup size it is best to choose a power of 2 because that plays nice with stage 2, the local reduction, and also to make it as big as possible. For the number of workgroups it is best to consider the number of compute units of the graphics card (e.g. the AMD RX 480 has 36 compute units) and to make it a multiple of it for latency compensation.
