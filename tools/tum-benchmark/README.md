This directory contains the tools from the TUM benchmark.

- **asociate.py** Generates depth+color image pairs according to their
  timestamps
- **evaluate_ate.py** Evaluates the absolute trajectory error
- **evaluate_rte.py** Evaluates the relative trajectory error

# Installation
- Python 2
  - Installation using chocolatey (windows) `choco install python2`
  - Add `C:\tools\python2` and `C:\tools\python2\Scripts` to the `Path` system
    environment variable
  - Installation can be verified using `python --version`.
    It should be python 2.x
- NumPy
  - Installation using pip `pip install numpy`

# Usage
Usage instructions can be displayed using the `-h` option. For example
`python associate.py -h` displays the usage instructions for the association
script.

More details can be found on the
[website](http://vision.in.tum.de/data/datasets/rgbd-dataset/tools).
